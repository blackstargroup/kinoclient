import { Platform } from 'react-native';
import FCM from 'react-native-fcm';

import store from '../store';
import deviceInfoService from '../utils/device-info-service';
import { alertOk } from '../utils/helpers/alerts';

export default async function onFetchFCMToken(fcmToken) {
  console.log('!!Refresh FCM token ', fcmToken);

  if (!fcmToken) {
    console.log('!!Invalid FCM token');
    return;
  }
  const { me, common } = store.getState();
  if (!me._id) {
    return;
  }

  if (common.multiLoginToken) {
    !__DEV__ &&
      alertOk(
        `Устройство`,
        `Не запоминаем устройство, вы вошли испльзуя мультилогин токен`,
      );
    return;
  }

  const apnsToken = await getAPNSTokenOrDefault();
  console.log('!!APNS token ', apnsToken);

  try {
    await deviceInfoService.sendToServer({ fcmToken });
    console.log('!!Device info updated');
  } catch (error) {
    console.log('!!Cant update device info ', error);
  }
}

async function getAPNSTokenOrDefault() {
  // don't know where we use APNS token. May be we use it with APNSKey
  if (Platform.OS !== 'ios') {
    return '';
  }

  let token;
  try {
    token = await FCM.getAPNSToken();
  } catch (apnsTokenError) {
    console.log(`!!Can't get APNS token`, apnsTokenError);
  }
  return token || '';
}
