import accountManager from '../utils/account-manager';
import { initFCM } from '../utils/fcm';

import FCM from 'react-native-fcm';
import devInfo from 'react-native-device-info';
import deviceStorage from '../utils/device-storage';
import store from '../store';
import {
  commonSetMultiLoginToken,
  commonSetLoginPhones,
} from '../actions/common';

export default async () => {
  deviceStorage.getMultiLoginToken().then(multiLoginToken => {
    store.dispatch(commonSetMultiLoginToken({ multiLoginToken }));
  });

  deviceStorage.getLoginPhones().then(loginPhones => {
    store.dispatch(commonSetLoginPhones({ loginPhones }));
  });

  try {
    await initFCM();
    console.log('!! FCM successfully  initialized !!');
  } catch (error) {
    console.log('!! FCM init error !!');
  }

  await accountManager.tryAutoLogin();
};
