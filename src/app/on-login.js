import FCM from 'react-native-fcm';

import store from '../store';
import { setMe } from '../actions/me';
import dataService from '../utils/data-service';
import { intervals } from '../utils/constants';
import tasksService from '../utils/tasks-service';
import navService from '../utils/nav-service';
import { requestNotificationPermission } from '../utils/permissions';
import { refreshFCMToken } from '../utils/fcm';

const DEFAULT_SCREEN = ''; //'ProjDraftEdit';
const DEFAULT_SCREEN_PARAMS = {}; // { projDraftId: '5bd08fb219bb7ed22d9396ec' };

async function requestPermissions() {
  try {
    await requestNotificationPermission();
  } catch (error) {}
}

export default async ({ me, isAutoLogin }) => {
  if (!me) return;
  tasksService.restart();

  __DEV__ &&
    DEFAULT_SCREEN &&
    navService.navigate(DEFAULT_SCREEN, DEFAULT_SCREEN_PARAMS);

  requestPermissions();

  setTimeout(refreshFCMToken, 2000);
};
