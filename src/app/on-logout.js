import store, { client } from '../store';
import { setMe } from '../actions/me';
import tasksService from '../utils/tasks-service';

export default async () => {
  tasksService.stop();
  store.dispatch(setMe(null));
  await client.resetStore();
};
