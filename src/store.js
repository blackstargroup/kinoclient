import { ApolloClient } from 'apollo-client';
import { InMemoryCache } from 'apollo-cache-inmemory';
import { ApolloLink } from 'apollo-link';
import { createHttpLink } from 'apollo-link-http';
import { RetryLink } from 'apollo-link-retry';
import { onError } from 'apollo-link-error';
import { setContext } from 'apollo-link-context';
import { createStore } from 'redux';

import reducers from './reducers';
import deviceStorage from './utils/device-storage';
import settings from './utils/settings';

const errorLink = onError(({ graphQLErrors, networkError }) => {
  graphQLErrors && console.log(graphQLErrors);
  networkError && console.log(networkError);
});

const authLink = setContext(async (operation, previousContext) => {
  const { headers } = previousContext;
  const token = await deviceStorage.getAuthToken();

  return {
    ...previousContext,
    headers: {
      ...headers,
      authorization: token || '',
    },
  };
});

const uri = settings.graphqlUrl;

const link = ApolloLink.from([
  //new RetryLink(),
  authLink,
  errorLink,
  createHttpLink({ uri }),
]);

export const client = new ApolloClient({
  link,
  cache: new InMemoryCache(),
  defaultOptions: {
    watchQuery: {
      fetchPolicy: 'network-only',
      errorPolicy: 'ignore',
    },
    query: {
      fetchPolicy: 'network-only',
      errorPolicy: 'all',
    },
  },
});

const store = createStore(reducers);
export default store;
