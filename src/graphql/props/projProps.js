export default `
_id
title
desc
thumb {
  path
}
country
city
test
roles {
  _id
  projId
  title
  shortDesc
  desc
  homework
}
closed
closedAt
createdAt
updatedAt
`;
