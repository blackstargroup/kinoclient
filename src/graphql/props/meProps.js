export default `
_id
loginPhone
firstName
lastName
avatar {
  path
}
isDir
isModer
isTester
dirAbout
dirContacts
`;
