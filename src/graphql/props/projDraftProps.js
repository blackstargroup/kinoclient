export default `
_id
ownerId
projId
title
desc
thumb {
  path
}
country
city
roles {
  _id
  projDraftId
  roleId
  title
  shortDesc
  desc
  homework
  createdAt
  updatedAt
}
status
dirComment
moderComment
test
deleted
createdAt
updatedAt
`;
