export default `
_id
title
thumb {
  path
}
country
city
test
closed
closedAt
createdAt
updatedAt
`;
