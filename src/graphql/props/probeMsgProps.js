export default `
_id
userId
text
attachment {
  path
  thumbPath
  mime
  height
  width
  size
}
createdAt
updatedAt
`;
