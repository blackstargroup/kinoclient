export default `
_id
ownerId
projId
title
desc
thumb {
  path
}
country
city
status
test
deleted
createdAt
updatedAt
`;
