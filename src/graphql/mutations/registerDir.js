import gql from 'graphql-tag';
import meProps from '../props/meProps';

export default gql`
  mutation registerDir(
    $firstName: String
    $lastName: String
    $dirAbout: String
    $dirContacts: String
  ) {
    registerDir(
      firstName: $firstName
      lastName: $lastName
      dirAbout: $dirAbout
      dirContacts: $dirContacts
    ) {
${meProps}
    }
  }
`;
