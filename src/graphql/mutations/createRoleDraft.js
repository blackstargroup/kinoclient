import gql from 'graphql-tag';
import roleDraftProps from '../props/roleDraftProps';

export default gql`
  mutation createRoleDraft($projDraftId: ID!, $title: String) {
    createRoleDraft(projDraftId: $projDraftId, title: $title) {
${roleDraftProps}
    }
  }
`;
