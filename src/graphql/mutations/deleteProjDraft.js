import gql from 'graphql-tag';
import projDraftProps from '../props/projDraftProps';

export default gql`
  mutation createProjDraft($_id: ID!) {
    deleteProjDraft(_id: $_id) {
${projDraftProps}
    }
  }
`;
