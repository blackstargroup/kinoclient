import gql from 'graphql-tag';
import probeProps from '../props/probeProps';

export default gql`
  mutation createProbe($roleId: ID!, $text: String, $attachmentJson: String) {
    createProbe(roleId: $roleId, text: $text, attachmentJson: $attachmentJson) {
${probeProps}
    }
  }
`;
