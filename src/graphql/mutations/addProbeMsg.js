import gql from 'graphql-tag';
import probeMsgProps from '../props/probeMsgProps';

export default gql`
  mutation addProbeMsg($probeId: ID!, $text: String, $attachmentJson: String) {
    addProbeMsg(probeId: $probeId, text: $text, attachmentJson: $attachmentJson) {
${probeMsgProps}
    }
  }
`;
