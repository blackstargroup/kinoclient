import gql from 'graphql-tag';
import meProps from '../props/meProps';

export default gql`
  mutation updateMe($firstName: String, $lastName: String) {
    updateMe(firstName: $firstName, lastName: $lastName) {
${meProps}
    }
  }
`;
