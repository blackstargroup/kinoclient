import gql from 'graphql-tag';
import statusProps from '../props/statusProps';

export default gql`
  mutation deleteProbe($_id: ID!) {
    deleteProbe(_id: $_id) {
${statusProps}
    }
  }
`;
