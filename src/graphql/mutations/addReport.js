import gql from 'graphql-tag';

export default gql`
  mutation addReport($kind: String, $title: String, $desc: String) {
    addReport(kind: $kind, title: $title, desc: $desc) {
      success
    }
  }
`;
