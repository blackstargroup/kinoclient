import gql from 'graphql-tag';
import projDraftProps from '../props/projDraftProps';

export default gql`
  mutation promoteProjDraft($_id: ID!, $status: String!) {
    promoteProjDraft(_id: $_id, status: $status) {
${projDraftProps}
    }
  }
`;
