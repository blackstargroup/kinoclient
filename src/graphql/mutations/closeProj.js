import gql from 'graphql-tag';
import projProps from '../props/projProps';

export default gql`
  mutation closeProj($_id: ID!) {
    closeProj(_id: $_id) {
${projProps}
    }
  }
`;
