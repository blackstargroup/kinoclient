import gql from 'graphql-tag';
import projDraftProps from '../props/projDraftProps';

export default gql`
  mutation updateProjDraft($_id: ID!, $title: String, $desc: String, $country: String, $city: String,
    $dirComment: String, $moderComment: String) {
    updateProjDraft(_id: $_id, title: $title, desc: $desc, country: $country, city: $city,
      dirComment: $dirComment, moderComment: $moderComment) {
${projDraftProps}
    }
  }
`;
