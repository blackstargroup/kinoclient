import gql from 'graphql-tag';
import statusProps from '../props/statusProps';

export default gql`
  mutation visitProbe($_id: ID!) {
    visitProbe(_id: $_id) {
${statusProps}
    }
  }
`;
