import gql from 'graphql-tag';
import statusProps from '../props/statusProps';

export default gql`
  mutation setTester($loginPhone: String!, $value: Boolean) {
    setTester(loginPhone: $loginPhone, value: $value) {
${statusProps}
    }
  }
`;
