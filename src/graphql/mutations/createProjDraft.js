import gql from 'graphql-tag';
import projDraftProps from '../props/projDraftProps';

export default gql`
  mutation createProjDraft($title: String!, $test: Boolean) {
    createProjDraft(title: $title, test: $test) {
${projDraftProps}
    }
  }
`;
