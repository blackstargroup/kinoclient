import gql from 'graphql-tag';

export default gql`
  mutation setDeviceInfo(
    $deviceId: String!
    $os: String
    $systemVersion: String
    $fcmToken: String
    $locale: String
    $country: String
    $timezone: String
  ) {
    setDeviceInfo(
      deviceId: $deviceId
      os: $os
      systemVersion: $systemVersion
      fcmToken: $fcmToken
      locale: $locale
      country: $country
      timezone: $timezone
    ) {
      deviceId
      os
      systemVersion
      fcmToken
      locale
      country
      timezone
    }
  }
`;
