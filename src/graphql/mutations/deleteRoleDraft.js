import gql from 'graphql-tag';
import statusProps from '../props/statusProps';

export default gql`
  mutation deleteRoleDraft($_id: ID!) {
    deleteRoleDraft(_id: $_id) {
${statusProps}
    }
  }
`;
