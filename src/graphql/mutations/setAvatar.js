import gql from 'graphql-tag';
import meProps from '../props/meProps';

export default gql`
  mutation setAvatar($serverName: String!, $fileId: String!) {
    setAvatar(serverName: $serverName, fileId: $fileId) {
${meProps}
    }
  }
`;
