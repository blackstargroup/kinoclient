import gql from 'graphql-tag';
import roleDraftProps from '../props/roleDraftProps';

export default gql`
  mutation updateRoleDraft($_id: ID!, $title: String, $shortDesc: String, $desc: String, $homework: String) {
    updateRoleDraft(_id: $_id, title: $title, shortDesc: $shortDesc, desc: $desc, homework: $homework) {
${roleDraftProps}
    }
  }
`;
