import gql from 'graphql-tag';
import userProps from '../props/userProps';

export default gql`
  query getUser($_id: ID!) {
    getUser(_id: $_id) {
${userProps}
    }
  }
`;
