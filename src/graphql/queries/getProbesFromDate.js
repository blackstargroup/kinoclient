import gql from 'graphql-tag';
import probeProps from '../props/probeProps';

export default gql`
  query getProbesFromDate($from: Date) {
    getProbesFromDate(from: $from) {
${probeProps}
    }
  }
`;
