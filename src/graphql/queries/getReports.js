import gql from 'graphql-tag';

export default gql`
  query getReports($count: Int!, $kind: String) {
    getReports(count: $count, kind: $kind) {
      _id
      userId
      userTitle
      kind
      title
      desc
      full
      createdAt
    }
  }
`;
