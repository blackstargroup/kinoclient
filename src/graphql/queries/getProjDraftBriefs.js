import gql from 'graphql-tag';
import projDraftBriefProps from '../props/projDraftBriefProps';

export default gql`
  {
    getProjDrafts {
${projDraftBriefProps}
    }
  }
`;
