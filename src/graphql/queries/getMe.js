import gql from 'graphql-tag';
import meProps from '../props/meProps';

export default gql`
  query getMe {
    getMe {
${meProps}
    }
  }
`;
