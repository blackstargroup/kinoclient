import gql from 'graphql-tag';
import projProps from '../props/projProps';
import roleProps from '../props/roleProps';

export default gql`
  query getProj($_id: ID!) {
    getProj(_id: $_id) {
${projProps}
    }
  }
`;
