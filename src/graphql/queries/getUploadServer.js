import gql from 'graphql-tag';

export default gql`
  query getUploadServer {
    getUploadServer {
      serverName
      uploadUrl
    }
  }
`;
