import gql from 'graphql-tag';
import probeMsgProps from '../props/probeMsgProps';

export default gql`
  query getProbeMsgsFromDate($probeId: ID!, $from: Date) {
    getProbeMsgsFromDate(probeId: $probeId, from: $from) {
${probeMsgProps}
    }
  }
`;
