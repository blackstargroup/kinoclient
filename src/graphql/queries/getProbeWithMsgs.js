import gql from 'graphql-tag';
import probeProps from '../props/probeProps';
import probeMsgProps from '../props/probeMsgProps';

export default gql`
  query getProbe($probeId: ID!) {
    getProbe(_id: $probeId) {
${probeProps}
    }

    getProbeMsgs(probeId: $probeId) {
${probeMsgProps}
    }
  }
`;
