import gql from 'graphql-tag';
import probeInfoProps from '../props/probeInfoProps';

export default gql`
  query getProbeInfosFromDate($from: Date) {
    getProbeInfosFromDate(from: $from) {
${probeInfoProps}
    }
  }
`;
