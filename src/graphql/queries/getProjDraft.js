import gql from 'graphql-tag';
import projDraftProps from '../props/projDraftProps';

export default gql`
  query getProjDraft($_id: ID!) {
    getProjDraft(_id: $_id) {
${projDraftProps}
    }
  }
`;
