import gql from 'graphql-tag';
import projBriefProps from '../props/projBriefProps';

export default gql`
  query getProjBriefsFromDate($from: Date) {
    getProjsFromDate(from: $from) {
${projBriefProps}
    }
  }
`;
