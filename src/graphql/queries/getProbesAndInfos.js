import gql from 'graphql-tag';
import probeProps from '../props/probeProps';
import probeInfoProps from '../props/probeInfoProps';

export default gql`
  query getProbesAndInfos {
    getProbes {
${probeProps}
    }

    getProbeInfos {
 ${probeInfoProps}
    }
  }
`;
