import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  Alert,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import store from '../store';
import { newProjAcceptRules, newProjAcceptAgreement } from '../actions/newProj';

import { AvatarUploadingView } from '../components/AvatarUploadingView';

import { BtnDefault, BtnTransparent, Hint } from '../components/Buttons';
import { BackBtnNavHeader } from '../components/NavHeaders';
import { sizes, colors, shadow, shadowInner, baseStyles } from '../utils/theme';
import navService from '../utils/nav-service';

import dataService from '../utils/data-service';
import { AvatarWithText } from '../components/Thumbs';
import accountManager from '../utils/account-manager';
import { alertOk } from '../utils/helpers/alerts';

const screenTitle = 'Новый кастинг';
const hintDirAbout =
  'Мы не хотим добавлять фейковые кастинги. Поэтому должны убедиться, что вы действительно ищете актеров. ' +
  'Представившись, Вы дадите нам знать с кем мы имеем дело.';

let initialState = {
  _id: '',
  firstName: '',
  lastName: '',
  dirAbout: '',
  dirContacts: '',
};

let stateMemory = {
  ...initialState,
};

function goBack() {
  navService.navigate('General');
}

function RulesContent({ acceptPress, backPress }) {
  return (
    <View style={baseStyles.container}>
      <BackBtnNavHeader
        title={screenTitle}
        subTitle={''}
        onBackPress={goBack}
      />
      <View style={[baseStyles.content, {}]}>
        <Text style={baseStyles.textH1}>Условия добавления</Text>
        <Text style={baseStyles.textParagraph}>
          ВАЖНО! Проект должен быть настоящим. Если вы ивестны в кругах,
          достаточно подтвердить свою личность. Мы так же принимаем кастинги от
          мало известных людей, начинающих режисеров, и другие интересные
          проекты. Однако в таком случае нам необходимо убедиться, что кастинг
          настоящий, вы действительно набираете людей на свой проект, и вы не
          занимаетесь нелегальной деятельность, такой как проституция и
          порнография.
        </Text>
      </View>
      <View style={baseStyles.btnsContainer}>
        <BtnDefault title={'Продолжить'} onPress={acceptPress} />
        <BtnTransparent title={'Вернуться на главную'} onPress={backPress} />
      </View>
    </View>
  );
}

function AgreementContent({ acceptPress, backPress }) {
  return (
    <View style={baseStyles.container}>
      <BackBtnNavHeader
        title={screenTitle}
        subTitle={''}
        onBackPress={goBack}
      />
      <View style={[baseStyles.content, {}]}>
        <Text style={baseStyles.textH1}>Соглашение</Text>
        <Text style={baseStyles.textParagraph}>
          Вы гарантируете что вам более 18 лет.
        </Text>
        <Text style={baseStyles.textParagraph}>
          Вы полностью берете на себя отвественность за информацию, которую
          размещаете, а так же за переписку.
        </Text>
        <Text style={baseStyles.textParagraph}>
          Вы должны понимать, что другими пользователями приложения могут быть
          несовершеннолетние лица
        </Text>
        <Text style={baseStyles.textParagraph}>
          Запрещается размещать или запрашивать у других пользователей фото и
          видео порнографического характера
        </Text>
        <Text style={baseStyles.textParagraph}>
          Запрещается заниматься проституцией, и прочей незаконной деятельность,
          которая запрещена в РБ РФ Укранине ...
        </Text>
      </View>
      <View style={baseStyles.btnsContainer}>
        <BtnTransparent
          title={'Я прочитал(а) и согласен(на)'}
          onPress={acceptPress}
        />
        <BtnTransparent title={'Вернуться к условиям'} onPress={backPress} />
      </View>
    </View>
  );
}

async function trySubmit(self) {
  let me = await dataService.getMe();
  let { _id, firstName, lastName, dirAbout, title, dirContacts } = self.state;
  if (me._id !== _id) throw new Error('Invalid user id');

  firstName = firstName ? firstName.trim() : '';
  lastName = lastName ? lastName.trim() : '';
  dirAbout = dirAbout ? dirAbout.trim() : '';
  title = title ? title.trim() : '';
  dirContacts = dirContacts ? dirContacts.trim() : '';

  if (!firstName || !lastName || !dirAbout || !title || !dirContacts) {
    await alertOk('Добавление кастинга', 'Необходимо заполнить все поля формы');
    return;
  }

  let test = me.isTester;
  if (test) {
    await alertOk(
      'Тестовый кастинг',
      `Так как Вы тестировщик, проект будет создан в тестовом режиме, Его сможете видеть только Вы и другие тестировщики`,
    );
  }

  try {
    await dataService.registerDir({
      firstName,
      lastName,
      dirAbout,
      dirContacts,
    });
    await dataService.createProjDraft({ title, test });
  } catch (error) {
    await alertOk(
      'Добавление кастинга',
      `Не удалось создать новый проект. 
      Проверьте соединение или попробуйте позже, возможно требуется
      обновить приложение`,
    );
    console.log(error);
    return;
  }

  self.setState({ ...initialState });
  navService.navigate('General');
  navService.navigate('Projs');
}

class NewProjScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  componentDidMount() {
    let { _id, firstName, lastName, dirAbout, dirContacts } = this.props.me;

    if (stateMemory._id === _id) {
      firstName = firstName || stateMemory.firstName || '';
      lastName = lastName || stateMemory.lastName || '';
      this.setState({ ...stateMemory, firstName, lastName });
    } else {
      this.setState({
        _id,
        firstName,
        lastName,
        dirAbout,
        dirContacts,
      });
    }
  }

  componentWillUnmount() {
    let { _id, firstName, lastName, dirAbout, dirContacts } = this.state;
    stateMemory = { _id, firstName, lastName, dirAbout, dirContacts };
  }

  render() {
    const { avatarUploadStatus } = this.props;

    if (avatarUploadStatus.visible || avatarUploadStatus.error) {
      return (
        <AvatarUploadingView
          avatarUploadStatus={avatarUploadStatus}
          title={title}
          onBackPress={goBack}
        />
      );
    }

    if (!this.props.rulesOK) {
      return (
        <RulesContent
          acceptPress={() =>
            store.dispatch(newProjAcceptRules({ accept: true }))
          }
          backPress={goBack}
        />
      );
    }

    if (!this.props.agreementOK) {
      return (
        <AgreementContent
          acceptPress={() =>
            store.dispatch(newProjAcceptAgreement({ accept: true }))
          }
          backPress={() =>
            store.dispatch(newProjAcceptRules({ accept: false }))
          }
        />
      );
    }

    return (
      <KeyboardAvoidingView
        style={baseStyles.container}
        enabled
        behavior="padding"
      >
        <BackBtnNavHeader
          title={screenTitle}
          subTitle={''}
          onBackPress={goBack}
        />
        <View style={{ flex: 1 }}>
          <ScrollView
            style={{ flex: 1 }}
            contentContainerStyle={{ flexGrow: 1 }}
          >
            <View style={baseStyles.scrollViewContent}>
              <View
                style={{
                  backgroundColor: 'transparent',
                  padding: 16,
                  flexDirection: 'row',
                }}
              >
                <TouchableOpacity
                  style={{ flex: 2 }}
                  onPress={accountManager.changeAvatar}
                >
                  <AvatarWithText
                    path={this.props.me.avatar.path}
                    size={80}
                    text={'Загрузите\nсвое фото'}
                  />
                </TouchableOpacity>

                <View style={{ flex: 4 }}>
                  <TextInput
                    style={[baseStyles.textbox]}
                    underlineColorAndroid="transparent"
                    value={this.state.firstName}
                    placeholder="Имя"
                    onChangeText={firstName => this.setState({ firstName })}
                  />

                  <TextInput
                    style={[baseStyles.textbox]}
                    underlineColorAndroid="transparent"
                    value={this.state.lastName}
                    placeholder="Фамилия"
                    onChangeText={lastName => this.setState({ lastName })}
                  />
                </View>
              </View>

              <View style={{ flexDirection: 'row' }}>
                <Text style={baseStyles.textboxHint}>Представьтесь:</Text>
                <Hint text={hintDirAbout} />
              </View>

              <TextInput
                style={[baseStyles.textbox]}
                underlineColorAndroid="transparent"
                minHeight={80}
                multiline={true}
                placeholder={
                  'В произвольной форме укажите кем вы являетесь, должность, ' +
                  'прочую информацию, чтобы мы могли Вас идентифицировать. ' +
                  'Можете назвать свой самый значимый проект'
                }
                value={this.state.dirAbout}
                onChangeText={dirAbout => this.setState({ dirAbout })}
              />

              <Text style={baseStyles.textboxHint}>Название проекта:</Text>

              <TextInput
                style={[baseStyles.textbox]}
                underlineColorAndroid="transparent"
                placeholder={'Название кастинга'}
                value={this.state.title}
                onChangeText={title => this.setState({ title })}
              />

              <Text style={baseStyles.textboxHint}>
                Как с Вами связаться модератору?
              </Text>

              <TextInput
                style={[baseStyles.textbox]}
                underlineColorAndroid="transparent"
                multiline={true}
                minHeight={60}
                placeholder={
                  'Telegram, Viber, Facebook, WhatsUp, или номер телефона'
                }
                value={this.state.dirContacts}
                onChangeText={dirContacts => this.setState({ dirContacts })}
              />

              <View style={[baseStyles.content, {}]} />
              <View style={baseStyles.btnsContainer}>
                <BtnDefault title={'Создать'} onPress={() => trySubmit(this)} />
                <BtnTransparent
                  title={'Вернуться к соглашению'}
                  onPress={() =>
                    store.dispatch(newProjAcceptAgreement({ accept: false }))
                  }
                />
              </View>
            </View>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  propSection: { flexDirection: 'row', backgroundColor: 'white' },
  propName: {
    flex: 2,
    justifyContent: 'center',
    paddingLeft: 10,
    paddingVertical: 10,
  },
});

function mapStateToProps(state, { navigation }) {
  const { newProj, me, avatarUploadStatus } = state;
  const { rulesOK, agreementOK } = newProj;
  return {
    rulesOK,
    agreementOK,
    me,
    avatarUploadStatus,
    navigation,
  };
}

export default connect(mapStateToProps)(NewProjScreen);
