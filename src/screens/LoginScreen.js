import React from 'react';
import { StyleSheet, Text, View, Button, TouchableOpacity } from 'react-native';
import { LoginButton } from 'react-native-facebook-account-kit';
import { connect } from 'react-redux';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import accountManager from '../utils/account-manager';
import { colors } from '../utils/theme';

import MultiLoginButtons from '../components/MultiLoginBtns';

function onLoginError(e) {
  console.log('Failed to login', e);
}

async function tryLoginFacebook() {
  try {
    await accountManager.loginUsingFacebookKit();
  } catch (error) {
    console.log(error);
  }
}

class Login extends React.Component {
  render() {
    let { multiLoginToken, loginPhones } = this.props.common;

    return (
      <View style={styles.container}>
        <View style={styles.welcomeView}>
          <Text
            style={{
              fontSize: 30,
              fontWeight: 'bold',
              color: 'white',
              marginBottom: 20,
            }}
          >
            Самопробы в Кино
          </Text>
          <LoginButton
            style={{ backgroundColor: colors.BTN_DEFAULT_BG }}
            type="phone"
            onLogin={tryLoginFacebook}
            onError={onLoginError}
          >
            <Text
              style={{
                paddingVertical: 12,
                paddingHorizontal: 24,
                fontSize: 16,
                color: colors.BTN_DEFAULT_TEXT_COLOR,
              }}
            >
              Войти
            </Text>
          </LoginButton>

          {multiLoginToken && <MultiLoginButtons loginPhones={loginPhones} />}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  welcomeView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

function mapStateToProps(state) {
  let { common } = state;
  return { common };
}

export default connect(mapStateToProps)(Login);
