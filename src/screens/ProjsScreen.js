import React from 'react';
import {
  View,
  Text,
  Button,
  ActivityIndicator,
  FlatList,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { projDraftStatuses } from '../utils/constants';
import { colors, sizes } from '../utils/theme';
import { ErrorView } from '../components/ErrorView';
import { LoadingView } from '../components/LoadingView';
import { ProjThumb } from '../components/Thumbs';
import { NavHeader } from '../components/NavHeaders';
import navService from '../utils/nav-service';

function ProjItemStatus({ item }) {
  return (
    <View style={{ flexDirection: 'row' }}>
      {item.projDraftId && item.projId && (
        <MaterialCommunityIcons
          style={{ marginRight: 4 }}
          name="earth"
          color={colors.GREEN}
          size={16}
        />
      )}

      {item.test && <Text style={{ color: 'blue', marginRight: 4 }}>Тест</Text>}

      {item.closed && (
        <Text style={{ color: colors.RED, marginRight: 4 }}>Завершён</Text>
      )}

      {item.projDraftId && item.statusTitle && !item.closed && (
        <Text style={{ color: item.statusColor }}>{item.statusTitle}</Text>
      )}
    </View>
  );
}

function ProjItem({ item }) {
  const {
    _id,
    projId,
    projDraftId,
    statusTitle,
    statusColor,
    isOwner,
    title,
    country,
    city,
  } = item;
  return (
    <View style={styles.item}>
      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          projId && navService.navigate('ProjCard', { projId });
        }}
      >
        <ProjThumb size={70} />

        <View
          style={{
            marginLeft: 10,
            marginRight: 40,
            // flex: 1,
            flexDirection: 'column',
            flexWrap: 'wrap',
          }}
        >
          <Text>{title}</Text>

          <Text style={{ color: colors.GREY_TEXT_COLOR }}>
            {country && city ? `${country}, ${city}` : country || city || ''}
          </Text>

          <ProjItemStatus item={item} />
        </View>
      </TouchableOpacity>

      {projDraftId && (
        <TouchableOpacity
          style={styles.editBtn}
          onPress={() => {
            navService.navigate('ProjDraftCard', { projDraftId });
          }}
        >
          <FontAwesome
            name="gear"
            color={colors.CONTENT_ICON_COLOR}
            size={18}
          />
        </TouchableOpacity>
      )}
    </View>
  );
}

/* 
  CROWN ICON
    <MaterialCommunityIcons
      name="crown"
      color={colors.GOLD}
      size={18}
    />
*/

class ProjsScreen extends React.Component {
  render() {
    const title = 'Кастинги';

    function renderItem({ item }) {
      return <ProjItem item={item} />;
    }

    if (this.props.loading) {
      return <LoadingView title={title} />;
    }

    if (this.props.error) {
      return <ErrorView title={title} />;
    }

    return (
      <View style={styles.container}>
        <NavHeader title={title} />
        <View style={styles.content}>
          <FlatList
            ListHeaderComponent={<View style={{ flex: 1, height: 16 }} />}
            contentContainerStyle={styles.listContainer}
            data={this.props.items}
            keyExtractor={item => `${item.projId}|${item.projDraftId}`}
            renderItem={renderItem}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
    paddingHorizontal: sizes.paddingHorizontal,
    justifyContent: 'center',
  },
  item: {
    marginBottom: 5,
    borderBottomWidth: sizes.BORDER_SEPARATOR_WIDTH,
    borderBottomColor: colors.BORDER_SEPARATOR_COLOR,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    flex: 1,
    alignItems: 'center',
    padding: 10,
    flexDirection: 'row',
  },
  editBtn: {
    padding: 10,
    // borderWidth: 1,
  },
  container: {
    flex: 1,
  },
  listContainer: {
    alignSelf: 'stretch',
  },
});

function mapStateToProps(state, { navigation }) {
  const { projs } = state;
  return {
    loading: false,
    error: false,
    items: projs.items,
  };
}

export default connect(mapStateToProps)(ProjsScreen);
