import React from 'react';
import {
  StyleSheet,
  TextInput,
  Text,
  View,
  Alert,
  Button,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';

import accountManager from '../utils/account-manager';
import { Avatar } from '../components/Thumbs';
import { BtnTransparentRed } from '../components/Buttons';
import { BackBtnNavHeader } from '../components/NavHeaders';
import { sizes, colors, baseStyles } from '../utils/theme';
import navService from '../utils/nav-service';

function goBack() {
  navService.navigate('General');
}

const initialState = {
  success: false,
  loading: false,
  error: false,
};

function deleteAccount() {
  console.warn('Remove me');
}

function deleteAccountConfirm() {
  Alert.alert('Подтвердите', `Профиль будет удален безвозвратно`, [
    {
      text: 'Я хочу удалить профиль',
      onPress: deleteAccount,
    },
    {
      text: 'Отменить',
      onPress: () => {},
    },
  ]);
}

function tryDeleteAccount() {
  throw new Error('Not implemented');
  Alert.alert(
    'Точно удалить?',
    `В случае удаления профиля также будут удалены ваши пробы, автар,
      диалоги с другими пользователями, прикрепленные материалы, и
      прочее.`,
    [
      {
        text: 'Нет',
        onPress: () => {},
      },
      {
        text: 'Да',
        onPress: deleteAccountConfirm,
      },
    ],
  );
}

function UserPermissions({ me, common }) {
  let lockStatus = me.locked ? 'Заблокирован' : 'Не заблокирван';
  let { loginPhones, multiLoginToken } = common;

  return (
    <View style={{ paddingBottom: 8 }}>
      <Text>Тип профиля: {me.isDir ? 'Режисер' : 'Актер'}</Text>
      {me.isModer && <Text>Права: Модератор</Text>}
      {me.isTester && <Text>Права: Тестировщик</Text>}
      <Text>Блок: {lockStatus}</Text>
      {multiLoginToken && <Text>Мульти логин токен: {multiLoginToken}</Text>}
      {loginPhones && (
        <FlatList
          contentContainerStyle={{}}
          data={loginPhones}
          keyExtractor={item => item.phone}
          renderItem={({ item }) => (
            <Text>
              Быстрый вход {item.title}: {item.phone}
            </Text>
          )}
        />
      )}
    </View>
  );
}

class AccountScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  componentDidMount() {
    this.setState(initialState);
  }

  render() {
    const title = 'Учетная запись';
    const { me, common } = this.props;

    return (
      <View style={baseStyles.container}>
        <BackBtnNavHeader title={title} subTitle={''} onBackPress={goBack} />
        <View style={[baseStyles.content, { flex: 1 }]}>
          <View style={{ flex: 1 }}>
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                paddingBottom: 20,
              }}
            >
              <Avatar path={me.avatar.path} size={80} />
            </View>

            <Text style={[baseStyles.textParagraph, { paddingBottom: 8 }]}>
              Номер: {me.loginPhone}
            </Text>

            <Text
              style={[
                baseStyles.textParagraph,
                {
                  color: colors.GREY_TEXT_COLOR,
                  paddingBottom: 8,
                },
              ]}
            >
              Этот номер не виден другим пользователям и режисерам, а
              используется только для входа
            </Text>

            <UserPermissions me={me} common={common} />
          </View>

          <View style={baseStyles.btnsContainer}>
            <BtnTransparentRed
              title={'Выйти'}
              onPress={() => accountManager.logout()}
            />
          </View>
        </View>
      </View>
    );
  }
}

function mapStateToProps(state) {
  const { me, common } = state;
  return {
    me,
    common,
  };
}

export default connect(mapStateToProps)(AccountScreen);
