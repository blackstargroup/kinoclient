import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  Image,
  FlatList,
  KeyboardAvoidingView,
  TouchableOpacity,
  ScrollView,
  TextInput,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';

import { ErrorView } from '../components/ErrorView';
import { SuccessView } from '../components/SuccessView';
import { LoadingView } from '../components/LoadingView';
import { AvatarUploadingView } from '../components/AvatarUploadingView';
import { BackBtnNavHeader } from '../components/NavHeaders';
import { Thumb, ThumbAddVideo } from '../components/Thumbs';
import { BtnDefault } from '../components/Buttons';
import { sizes, colors, baseStyles } from '../utils/theme';
import probeDraftUiHelper from '../utils/ui-helpers/probeDraft-ui-helper';
import navService from '../utils/nav-service';
import { AvatarWithText } from '../components/Thumbs';
import accountManager from '../utils/account-manager';

class ProbeDraftScreen extends React.Component {
  constructor(props) {
    super(props);
    probeDraftUiHelper.componentInit(this);
  }

  componentWillUnmount() {
    probeDraftUiHelper.componentWillUnmount();
  }

  static getDerivedStateFromProps(props, state) {
    return probeDraftUiHelper.getDerivedStateFromProps(props, state);
  }

  render() {
    let { me, probeDraft, avatarUploadStatus } = this.props;
    let {
      error,
      loading,
      success,
      projId,
      roleId,
      role,
      proj,
      attachments,
      progress,
      status,
    } = probeDraft;

    let screenTitle = 'Проба на роль';

    function goBack() {
      navService.navigate('RoleCard', { projId, roleId });
    }

    if (avatarUploadStatus.visible || avatarUploadStatus.error) {
      return (
        <AvatarUploadingView
          avatarUploadStatus={avatarUploadStatus}
          title={screenTitle}
          onBackPress={goBack}
        />
      );
    }

    if (error) {
      return (
        <ErrorView
          title={screenTitle}
          onBackPress={goBack}
          errorText={'Произошла ошибка'}
          errorSubText={''}
          contentBtns={[
            {
              text: 'OK',
              onPress: goBack,
            },
          ]}
        />
      );
    }

    if (loading) {
      return (
        <LoadingView
          loadingText={status}
          progress={progress}
          title={screenTitle}
        />
      );
    }

    if (success) {
      return (
        <SuccessView
          title={screenTitle}
          onBackPress={goBack}
          onOkPress={goBack}
          status={'Заявка успешно отправлена! 😃'}
        />
      );
    }

    if (!me.firstName || !me.lastName) {
      return (
        <KeyboardAvoidingView
          style={baseStyles.container}
          enabled
          behavior="padding"
        >
          <BackBtnNavHeader
            title={screenTitle}
            subTitle={''}
            onBackPress={goBack}
          />
          <View style={{ flex: 1 }}>
            <ScrollView
              style={{ flex: 1 }}
              contentContainerStyle={{ flexGrow: 1 }}
            >
              <View style={baseStyles.scrollViewContent}>
                <View style={{ flex: 1 }}>
                  <Text style={baseStyles.textH1}>Ваши данные</Text>

                  <View
                    style={{
                      backgroundColor: 'transparent',
                      padding: 16,
                      flexDirection: 'row',
                    }}
                  >
                    <TouchableOpacity
                      style={{ flex: 2 }}
                      onPress={accountManager.changeAvatar}
                    >
                      <AvatarWithText
                        path={me.avatar.path}
                        size={80}
                        text={'Загрузите\nсвое фото'}
                      />
                    </TouchableOpacity>

                    <View style={{ flex: 4 }}>
                      <TextInput
                        style={[baseStyles.textbox]}
                        underlineColorAndroid="transparent"
                        value={this.state.firstName}
                        placeholder="Имя"
                        onChangeText={firstName => this.setState({ firstName })}
                      />

                      <TextInput
                        style={[baseStyles.textbox]}
                        underlineColorAndroid="transparent"
                        value={this.state.lastName}
                        placeholder="Фамилия"
                        onChangeText={lastName => this.setState({ lastName })}
                      />
                    </View>
                  </View>
                </View>

                <View style={baseStyles.btnsContainer}>
                  <BtnDefault
                    title={'Продолжить'}
                    onPress={() => {
                      let { firstName, lastName } = this.state;
                      probeDraftUiHelper.trySubmitAboutMeForm({
                        firstName,
                        lastName,
                      });
                    }}
                  />
                </View>
              </View>
            </ScrollView>
          </View>
        </KeyboardAvoidingView>
      );
    }

    return (
      <View style={baseStyles.container}>
        <BackBtnNavHeader title={screenTitle} onBackPress={goBack} />

        <View style={{ flex: 1 }}>
          <ScrollView style={baseStyles.scrollViewContent}>
            <Text style={baseStyles.textH1}>{role.title}</Text>
            <Text style={[baseStyles.textParagraph, { paddingBottom: 16 }]}>
              {role.homework}
            </Text>

            <Text style={baseStyles.textboxHint}>Ваш комментарий</Text>
            <TextInput
              style={[baseStyles.textbox]}
              underlineColorAndroid="transparent"
              multiline={true}
              numberOfLines={5}
              minHeight={80}
              placeholder="Текст сообщения..."
              borderRadius={5}
              value={this.state.text}
              onChangeText={text => this.setState({ text })}
            />

            <Text style={baseStyles.textboxHint}>Прикрепите файлы</Text>

            <FlatList
              style={{ flex: 1, flexDirection: 'row' }}
              data={this.state.attachments}
              horizontal={true}
              keyExtractor={item => item.file.path}
              renderItem={({ item }) => {
                let { file, thumbFile } = item;
                let thumbPath =
                  thumbFile && thumbFile.path ? thumbFile.path : file.path;

                return (
                  <Thumb
                    source={{ isStatic: true, uri: thumbPath }}
                    action={() =>
                      probeDraftUiHelper.removeFile({ path: item.file.path })
                    }
                    actionIcon={'remove'}
                    thumbSize={100}
                  />
                );
              }}
              ListFooterComponent={
                <ThumbAddVideo
                  source={{ isStatic: true, uri: '' }}
                  action={probeDraftUiHelper.addFile}
                  actionIcon={'plus'}
                  thumbSize={100}
                />
              }
            />
            <View style={baseStyles.btnsContainer}>
              <BtnDefault
                title="Отправить"
                onPress={probeDraftUiHelper.trySend}
              />
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  successContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  thumb: { width: 50, height: 50, borderWidth: 1 },
});

function mapStateToProps(state) {
  let { me, probeDraft, avatarUploadStatus } = state;
  return {
    me,
    probeDraft,
    avatarUploadStatus,
  };
}

export default connect(mapStateToProps)(ProbeDraftScreen);
