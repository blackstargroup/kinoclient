import React from 'react';
import {
  StyleSheet,
  TextInput,
  Text,
  KeyboardAvoidingView,
  ScrollView,
  View,
  Alert,
} from 'react-native';

import { BtnDefault, BtnTransparent } from '../components/Buttons';
import { BackBtnNavHeader } from '../components/NavHeaders';
import { sizes, colors, baseStyles } from '../utils/theme';
import navService from '../utils/nav-service';
import reportService from '../utils/report-service';
import { ErrorView } from '../components/ErrorView';
import { SuccessView } from '../components/SuccessView';
import { LoadingView } from '../components/LoadingView';
import { sleep } from '../utils/helpers/sleep';
import { DEV_SLEEP } from '../utils/constants';
import { handleTesterCommand } from '../utils/helpers/tester-cmds-handler';
import store from '../store';

const title = 'Написать разработчикам';

function goBack() {
  navService.navigate('General');
}

async function sendReport(self) {
  let text = self.state.text ? self.state.text.trim() : '';

  if (!text) {
    Alert.alert('Ой...', `Вы, забыли написать текст`, [
      {
        text: 'OK',
        onPress: () => {},
      },
    ]);
    return;
  }

  let { me } = store.getState();
  if (text.startsWith('$') && (me.isTester || me.isModer)) {
    try {
      await handleTesterCommand(text);
    } catch (error) {
      console.warn(error);
    }
    return;
  }

  try {
    self.setState({ ...initialState, loading: true });
    __DEV__ && (await sleep(DEV_SLEEP));

    await reportService.sendCustomReport({
      kind: 'msg',
      title: 'user report',
      desc: text,
    });

    self.setState({ ...initialState, success: true });
  } catch (error) {
    self.setState({ ...initialState, text, error: true });
  }
}

const initialState = {
  text: '',
  success: false,
  loading: false,
  error: false,
};

class ContactScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  componentDidMount() {
    this.setState(initialState);
  }

  render() {
    let { loading, success, error } = this.state;

    if (loading) {
      return (
        <LoadingView
          title={title}
          onBackPress={goBack}
          loadingText={'Отправка сообщения...'}
        />
      );
    }

    if (success) {
      return (
        <SuccessView
          title={title}
          onBackPress={goBack}
          status={'Сообщение успешно отправлено 😃'}
          onOkPress={goBack}
        />
      );
    }

    if (error) {
      return (
        <ErrorView
          title={title}
          onBackPress={goBack}
          errorText={'Произошла ошибка отправки сообщения'}
          errorSubText={
            'Проверьте соединение и попробуйте отправить сообщение позже..' +
            'Если ошибка повторяется, возможно вы используете старую версию приложения'
          }
          contentBtns={[
            {
              text: 'ОК',
              onPress: () => {
                this.setState({ error: false });
              },
            },
          ]}
        />
      );
    }

    return (
      <KeyboardAvoidingView
        style={baseStyles.container}
        enabled
        behavior="padding"
      >
        <BackBtnNavHeader title={title} subTitle={''} onBackPress={goBack} />
        <View style={{ flex: 1 }}>
          <ScrollView
            style={{ flex: 1 }}
            contentContainerStyle={{ flexGrow: 1 }}
          >
            <View style={[baseStyles.scrollViewContent, {}]}>
              <Text style={[baseStyles.textParagraph, { marginBottom: 16 }]}>
                Если Вы нашли ошибку 🐞, знаете как улучшить проект, Вам чего-то
                не хватает, Вы желаете сотрудничать 🛠 или просто Вам понравилось
                ❤️ приложение, можете написать нам 😀 в произвольной форме
              </Text>

              <View style={{ flex: 1 }}>
                <TextInput
                  multiline={true}
                  style={{
                    flex: 1,
                    backgroundColor: 'white',
                    padding: 8,
                    borderRadius: 4,
                    textAlignVertical: 'top',
                  }}
                  underlineColorAndroid="transparent"
                  value={this.state.text}
                  placeholder="Текст сообщения..."
                  maxLength={2000}
                  onChangeText={text =>
                    this.setState({
                      text,
                    })
                  }
                />
                <View style={{ flex: 1 }} />
              </View>

              <View
                style={{
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingBottom: 40,
                  paddingTop: 20,
                }}
              >
                <BtnDefault
                  title={'Отправить'}
                  onPress={() => sendReport(this)}
                />
              </View>
            </View>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

// PS текст можно сохранять в device storage наслучай если вылезла ошибка
// чтоб пользователь не потерял написанное

export default ContactScreen;
