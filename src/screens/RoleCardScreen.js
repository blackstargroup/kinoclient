import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  ScrollView,
  ActivityIndicator,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';

import { ErrorView } from '../components/ErrorView';
import { BackBtnNavHeader } from '../components/NavHeaders';
import { BtnDefault } from '../components/Buttons';
import { baseStyles } from '../utils/theme';
import navService from '../utils/nav-service';

class RoleCardScreen extends React.Component {
  render() {
    const { proj, role, error, navigation } = this.props;

    const screenTitle = 'Роль';

    function goBack() {
      navigation.pop();
    }

    if (error) {
      return <ErrorView title={screenTitle} onBackPress={goBack} />;
    }

    const { _id, projId, desc, homework } = role;

    return (
      <View style={baseStyles.container}>
        <BackBtnNavHeader title={screenTitle} onBackPress={goBack} />
        <View style={{ flex: 1 }}>
          <ScrollView
            style={{ flex: 1 }}
            contentContainerStyle={{ flexGrow: 1 }}
          >
            <View style={baseStyles.scrollViewContent}>
              <View style={{ flex: 1 }}>
                <Text style={baseStyles.textH1}>{role.title}</Text>
                <Text style={[baseStyles.textParagraph]}>{desc}</Text>
                <Text style={[baseStyles.textParagraph]}>{homework}</Text>
              </View>
              {!proj.closed && (
                <View style={baseStyles.btnsContainer}>
                  <BtnDefault
                    title="Попробывать себя"
                    onPress={() =>
                      navService.navigate('ProbeDraft', { projId, roleId: _id })
                    }
                  />
                </View>
              )}
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({});

function mapStateToProps(state, { navigation }) {
  const { proj, role, error } = state.projCard;
  return {
    proj,
    role,
    error,
    navigation,
  };
}

export default connect(mapStateToProps)(RoleCardScreen);
