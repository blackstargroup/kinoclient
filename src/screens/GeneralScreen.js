import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { Avatar } from '../components/Thumbs';
import { colors, sizes, shadow, rainbow, baseStyles } from '../utils/theme';
import navService from '../utils/nav-service';

function mapStateToProps(state) {
  return { ...state };
}

function Header({ navigation, me }) {
  const { avatar, firstName, lastName } = me;
  return (
    <View
      style={{
        backgroundColor: colors.NAV_BAR_COLOR,
        borderBottomWidth: sizes.BORDER_SEPARATOR_WIDTH,
        borderBottomColor: colors.NAV_BAR_BORDER,
        ...shadow,
      }}
    >
      <TouchableOpacity
        style={{ flexDirection: 'row', padding: 10 }}
        onPress={() => navigation.navigate('ProfileConfig')}
      >
        <View style={styles.avatarContainer}>
          <Avatar size={80} path={avatar.path} />
        </View>
        <View>
          <View style={{ paddingLeft: 10, paddingVertical: 20 }}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                color: colors.NAV_FIRST_TEXT_COLOR,
              }}
            >
              {firstName || 'Имя'} {lastName || 'Фамилия'}
            </Text>
            <Text style={{ color: colors.NAV_FIRST_TEXT_COLOR }}>
              Настройки профиля
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
}

function MenuItem({ text, Font, iconName, iconColor, onPress }) {
  return (
    <TouchableOpacity style={styles.section} onPress={onPress}>
      <View style={styles.sectionInner}>
        <View style={[styles.sectionIcon, { backgroundColor: iconColor }]}>
          <Font
            style={{ marginLeft: 2, marginTop: 2 }}
            name={iconName}
            color={colors.WHITE}
            size={20}
          />
        </View>
        <Text style={styles.sectionText}>{text}</Text>
      </View>
    </TouchableOpacity>
  );
}

class GeneralScreen extends React.Component {
  static navigationOptions = {
    header: null,
  };

  render() {
    const { me, navigation } = this.props;

    return (
      <View style={baseStyles.container}>
        <Header me={me} navigation={navigation} />

        <View style={baseStyles.content}>
          <View style={styles.separator} />
          <View>
            <MenuItem
              text={'Учётная запись'}
              onPress={() => navService.navigate('Account')}
              iconName={'key-variant'}
              iconColor={rainbow.one}
              Font={MaterialCommunityIcons}
            />

            <MenuItem
              text={'Написать разработчикам'}
              onPress={() => navService.navigate('Contact')}
              iconName={'pencil'}
              iconColor={rainbow.two}
              Font={MaterialCommunityIcons}
            />

            <MenuItem
              text={'Добавить свой кастинг'}
              onPress={() => navService.navigate('NewProj')}
              iconName={'rocket'}
              iconColor={rainbow.three}
              Font={FontAwesome}
            />

            {me.isModer && (
              <MenuItem
                text={'Отчеты'}
                onPress={() => navService.navigate('Reports')}
                iconName={'bug'}
                iconColor={rainbow.five}
                Font={FontAwesome}
              />
            )}
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  separator: {
    paddingTop: 40,
  },
  section: {
    backgroundColor: 'transparent',
  },
  sectionInner: {
    flexDirection: 'row',

    marginTop: 2,
    padding: 5,
    backgroundColor: 'transparent',
    borderBottomWidth: sizes.BORDER_SEPARATOR_WIDTH,
    borderColor: colors.BORDER_SEPARATOR_COLOR,
  },
  sectionText: {
    marginTop: 3,
    marginLeft: 10,
  },
  sectionIcon: {
    backgroundColor: 'skyblue',
    height: 24,
    width: 24,
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  welcomeView: {
    alignItems: 'center',
  },
  avatarContainer: { justifyContent: 'center', alignItems: 'center' },
});

export default connect(mapStateToProps)(GeneralScreen);
