/*******************************
 * Helpfull link for understanding how it works (apollow with react navigaion)
 * http://engineering.khanacademy.org/posts/creating-query-components-with-apollo.htm
 ******************************/

import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  FlatList,
  TouchableOpacity,
  Alert,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
} from 'react-native';
import { connect } from 'react-redux';
import store from '../store';

import { ErrorView } from '../components/ErrorView';
import { LoadingView } from '../components/LoadingView';
import { BtnDefault, Hint } from '../components/Buttons';
import { BackBtnNavHeader } from '../components/NavHeaders';
import { sizes, colors, baseStyles } from '../utils/theme';
import navService from '../utils/nav-service';
import dataService from '../utils/data-service';
import { projDraftStatuses } from '../utils/constants';
import roleDraftEditUiHelper from '../utils/ui-helpers/roleDraft-edit-ui-helper';

const shortDescHint =
  'Короткое описание кандидата отображается прямо под названием роли. ' +
  'Может содержать пол, возраст или другую принципиально значимую информацию.';

const descHint = 'Подробно опишите роль';

const homeworkHint =
  'Напишите задание, которое должен выполнить кандидат. Например ' +
  'записать и прислать видео самопробы, где он играет диалог, либо выслать фотографии, на ваше усмотрение..';

class RoleDraftEditScreen extends React.Component {
  constructor(props) {
    super(props);
    roleDraftEditUiHelper.componentInit(this);
  }

  componentDidMount() {
    let { projDraft, roleDraft, navigation } = this.props;
    roleDraftEditUiHelper.componentDidMount({ projDraft, roleDraft });
  }

  componentDidUpdate(prevProps, prevState) {
    roleDraftEditUiHelper.componentDidUpdate({ prevProps, prevState });
  }

  render() {
    const {
      me,
      projDraftId,
      projDraft,
      roleDraftId,
      roleDraft,
      proj,
      error,
      errorText,
      loading,
      loadingText,
    } = this.props;
    const screenTitle = 'Черновик';
    const { title: subTitle } = projDraft;

    if (loading) {
      return (
        <LoadingView
          title={screenTitle}
          subTitle={subTitle}
          loadingText={loadingText}
          onBackPress={roleDraftEditUiHelper.goBack}
        />
      );
    }

    if (!roleDraft) {
      return (
        <ErrorView
          title={screenTitle}
          subTitle={subTitle}
          errorText={'Роль не найдена'}
          onBackPress={roleDraftEditUiHelper.goBack}
        />
      );
    }

    if (error) {
      return (
        <ErrorView
          title={screenTitle}
          subTitle={subTitle}
          errorText={errorText}
          onBackPress={roleDraftEditUiHelper.goBack}
        />
      );
    }

    return (
      <KeyboardAvoidingView
        style={baseStyles.container}
        enabled
        behavior="padding"
      >
        <BackBtnNavHeader
          title={screenTitle}
          subTitle={subTitle}
          onBackPress={roleDraftEditUiHelper.trySave}
        />

        <View style={{ flex: 1 }}>
          <ScrollView>
            <View style={baseStyles.scrollViewContent}>
              <Text style={baseStyles.textboxHint}>Название роли:</Text>

              <TextInput
                style={[baseStyles.textbox]}
                underlineColorAndroid="transparent"
                placeholder={'Название роли'}
                value={this.state.title}
                onChangeText={title => this.setState({ title })}
              />

              <View style={{ flexDirection: 'row' }}>
                <Text style={baseStyles.textboxHint}>
                  Очень короткое описание кандидата:
                </Text>
                <Hint text={shortDescHint} />
              </View>

              <TextInput
                style={[baseStyles.textbox]}
                underlineColorAndroid="transparent"
                placeholder={'Представительный мужчина, 35-40лет'}
                value={this.state.shortDesc}
                onChangeText={shortDesc => this.setState({ shortDesc })}
              />

              <View style={{ flexDirection: 'row' }}>
                <Text style={baseStyles.textboxHint}>
                  Подробное описание роли:
                </Text>
                <Hint text={descHint} />
              </View>

              <TextInput
                style={[baseStyles.textbox]}
                underlineColorAndroid="transparent"
                multiline={true}
                minHeight={160}
                placeholder={'Описание роли'}
                value={this.state.desc}
                onChangeText={desc => this.setState({ desc })}
              />

              <View style={{ flexDirection: 'row' }}>
                <Text style={baseStyles.textboxHint}>
                  Задание для кандидата:
                </Text>
                <Hint text={homeworkHint} />
              </View>

              <TextInput
                style={[baseStyles.textbox]}
                underlineColorAndroid="transparent"
                multiline={true}
                minHeight={80}
                placeholder={
                  'Требуется прислать видео (или несколько), диалог такой-то..., продемонстровать эмоции... удивление...'
                }
                value={this.state.homework}
                onChangeText={homework => this.setState({ homework })}
              />
            </View>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

function mapStateToProps(state, { navigation }) {
  const { me, projDraftCard } = state;
  const {
    projDraftId,
    projDraft,
    roleDraftId,
    roleDraft,
    proj,
    error,
    errorText,
    loading,
    loadingText,
  } = projDraftCard;

  return {
    me,
    projDraftId,
    projDraft,
    roleDraftId,
    roleDraft,
    proj,
    error,
    errorText,
    loading,
    loadingText,
    navigation,
  };
}

export default connect(mapStateToProps)(RoleDraftEditScreen);
