/*******************************
 * Helpfull link for understanding how it works (apollow with react navigaion)
 * http://engineering.khanacademy.org/posts/creating-query-components-with-apollo.htm
 ******************************/

import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  ScrollView,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { ErrorView } from '../components/ErrorView';
import { BackBtnNavHeader } from '../components/NavHeaders';
import { sizes, colors, baseStyles } from '../utils/theme';
import navService from '../utils/nav-service';

function RoleItem({ _id, projId, title, shortDesc }) {
  return (
    <View style={{ paddingTop: 10 }}>
      <TouchableOpacity
        style={{ padding: 10, backgroundColor: 'white', flexDirection: 'row' }}
        onPress={() => {
          navService.navigate('RoleCard', { projId, roleId: _id });
        }}
      >
        <View style={{ flex: 1 }}>
          <Text style={baseStyles.textH2}>{title}</Text>
          <Text style={baseStyles.textParagraph}>{shortDesc}</Text>
        </View>

        <FontAwesome
          name={'angle-right'}
          size={32}
          color={colors.CONTENT_ICON_COLOR}
        />
      </TouchableOpacity>
    </View>
  );
}

class ProjCardScreen extends React.Component {
  render() {
    const { proj, error } = this.props;
    const { desc, closed } = proj;
    const title = 'Кастинг';
    const subTitle = proj.title;

    function goBack() {
      navService.navigate('Projs');
    }

    if (error) {
      return (
        <ErrorView title={title} subTitle={subTitle} onBackPress={goBack} />
      );
    }

    return (
      <View style={baseStyles.container}>
        <BackBtnNavHeader title={title} onBackPress={goBack} />
        <View style={{ flex: 1 }}>
          <ScrollView style={baseStyles.scrollViewContent}>
            <Text style={baseStyles.textH1}>{proj.title}</Text>
            {closed && (
              <Text style={[baseStyles.textParagraph, { color: colors.RED }]}>
                Кастинг завершен
              </Text>
            )}

            <Text style={baseStyles.textParagraph}>{desc}</Text>

            <FlatList
              contentContainerStyle={styles.listContainer}
              data={this.props.proj.roles}
              keyExtractor={item => item._id}
              renderItem={({ item }) => {
                return <RoleItem {...item} />;
              }}
            />
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listContainer: {
    alignSelf: 'stretch',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  welcomeView: {
    alignItems: 'center',
  },
});

function mapStateToProps(state, { navigation }) {
  const { proj, error } = state.projCard;
  return {
    proj,
    error,
  };
}

export default connect(mapStateToProps)(ProjCardScreen);
