/*******************************
 * Helpfull link for understanding how it works (apollow with react navigaion)
 * http://engineering.khanacademy.org/posts/creating-query-components-with-apollo.htm
 ******************************/

import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Alert,
  ScrollView,
  Button,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';

import { ErrorView } from '../components/ErrorView';
import { LoadingView } from '../components/LoadingView';

import {
  BtnDefault,
  BtnTransparent,
  BtnTransparentRed,
} from '../components/Buttons';
import { BackBtnNavHeader } from '../components/NavHeaders';
import { ProjDraftCardBtns } from '../components/ProjDraftCardComps';
import { sizes, colors, baseStyles } from '../utils/theme';
import navService from '../utils/nav-service';
import dataService from '../utils/data-service';

class ProjDraftCardScreen extends React.Component {
  render() {
    const {
      me,
      projDraftId,
      projDraft,
      proj,
      error,
      errorText,
      loading,
      loadingText,
    } = this.props;
    const screenTitle = 'Управление проектом';
    const subTitle = projDraft.title;
    const { projId, status } = projDraft;

    function goBack() {
      navService.navigate('Projs');
    }

    if (error) {
      return (
        <ErrorView
          title={screenTitle}
          subTitle={subTitle}
          onBackPress={goBack}
          errorText={errorText}
          contentBtn={[
            {
              text: 'Обновить',
              onPress: () => {
                // dont wait result
                dataService.dispatchProjDraft({ projDraftId });
              },
            },
          ]}
        />
      );
    }

    if (loading) {
      return (
        <LoadingView
          title={screenTitle}
          subTitle={subTitle}
          onBackPress={goBack}
          loadingText={loadingText}
        />
      );
    }

    if (!proj._id) {
      projStatusTitle = 'Не опубликован';
      projStatusColor = colors.CONTENT_TEXT_COLOR;
    } else if (proj.closed) {
      projStatusTitle = 'Завершён';
      projStatusColor = colors.CONTENT_TEXT_COLOR;
    } else {
      projStatusTitle = 'Опубликован';
      projStatusColor = colors.GREEN;
    }

    return (
      <View style={baseStyles.container}>
        <BackBtnNavHeader
          title={screenTitle}
          subTitle={subTitle}
          onBackPress={goBack}
        />

        <View style={{ flex: 1 }}>
          <ScrollView style={baseStyles.content}>
            <Text style={baseStyles.textH1}>{projDraft.title}</Text>

            <View style={{ paddingBottom: 16 }}>
              {projDraft.test && (
                <View style={{ flexDirection: 'row' }}>
                  <Text style={{ color: 'blue' }}>Тестовый кастинг</Text>
                </View>
              )}

              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 4 }}>Состояние проекта:</Text>
                <Text style={{ color: projStatusColor }}>
                  {projStatusTitle}
                </Text>
              </View>

              <View style={{ flexDirection: 'row' }}>
                <Text style={{ marginRight: 4 }}>Состояние черновика:</Text>
                <Text style={{ color: projDraft.statusColor }}>
                  {projDraft.statusTitle}
                </Text>
              </View>
            </View>

            <ProjDraftCardBtns
              projDraftId={projDraftId}
              projDraft={projDraft}
              me={me}
              proj={proj}
            />
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  listContainer: {
    alignSelf: 'stretch',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  welcomeView: {
    alignItems: 'center',
  },
});

function mapStateToProps(state, { navigation }) {
  const { me, projDraftCard } = state;
  const {
    projDraftId,
    projDraft,
    proj,
    error,
    errorText,
    loading,
    loadingText,
  } = projDraftCard;
  return {
    me,
    projDraftId,
    projDraft,
    proj,
    error,
    errorText,
    loading,
    loadingText,
  };
}

export default connect(mapStateToProps)(ProjDraftCardScreen);
