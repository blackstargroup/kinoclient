import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  FlatList,
  Platform,
  Dimensions,
  Image,
  TouchableOpacity,
  ScrollView,
  TextInput,
  KeyboardAvoidingView,
} from 'react-native';
import { connect } from 'react-redux';

import store from '../store';
import dataService from '../utils/data-service';
import { ErrorView } from '../components/ErrorView';
import { LoadingView } from '../components/LoadingView';
import { Msg, MsgAlt } from '../components/Msgs';
import MsgTextarea from '../components/MsgTextarea';
import { ProbeAsActorNavHeader } from '../components/NavHeaders';
import { colors, sizes, shadow, rainbow, baseStyles } from '../utils/theme';
import probeUiHelper from '../utils/ui-helpers/probe-ui-helper';
import navService from '../utils/nav-service';

function HeaderComponent() {
  return <View style={{ flex: 1, height: 20 }} />;
}

class ChatScreen extends React.Component {
  componentDidMount() {
    this.scrollToEnd();
  }

  scrollToEnd() {
    probeUiHelper.scrollList(this.msgsList);
  }

  render() {
    let { chat, me, common, cachedUsers, navigation } = this.props;
    let { keyboardOffset } = common;
    let chatMsgsFooterOffset = keyboardOffset
      ? sizes.CHAT_MSGS_WITH_KEYBOARD_FOOTER_OFFSET
      : sizes.CHAT_MSGS_FOOTER_OFFSET;
    let { probe, probeId } = chat;
    const { projThumbPath, projTitle, roleTitle } = probe;

    const screenTitle = projTitle;

    let win = Dimensions.get('window');
    let contentWidth = win.width - sizes.CONTENT_HORIZONTAL_PADDING * 2;

    function goBack() {
      navigation.pop();
    }

    if (chat.loading) {
      return (
        <LoadingView
          title={screenTitle}
          onBackPress={goBack}
          loadingText={chat.loadingText}
          progress={chat.progress}
        />
      );
    }

    if (chat.error) {
      let errorBtns = [
        {
          text: 'OK',
          onPress: () => {
            navService.navigateToNotifications();
          },
        },
      ];
      return (
        <ErrorView
          title={screenTitle}
          onBackPress={goBack}
          contentBtns={errorBtns}
        />
      );
    }

    return (
      <KeyboardAvoidingView
        behavior={Platform.select({ ios: 'padding', android: null })}
        style={[baseStyles.container, {}]}
        enabled
      >
        <ProbeAsActorNavHeader
          projThumbPath={projThumbPath}
          projTitle={screenTitle}
          onPressLeft={goBack}
          onPressRight={() => probeUiHelper.showFlagMenu({ probeId })}
        />

        <View style={styles.content}>
          <FlatList
            contentContainerStyle={styles.msgsList}
            ref={ref => (this.msgsList = ref)}
            onContentSizeChange={() => {
              this.scrollToEnd();
            }}
            onLayout={() => {
              this.scrollToEnd();
            }}
            data={chat.msgs}
            keyExtractor={item => item._id}
            renderItem={({ item }) => {
              const { userId } = item;
              const user = cachedUsers[userId];

              if (me._id == userId) {
                return (
                  <Msg msg={item} user={user} contentWidth={contentWidth} />
                );
              }
              return (
                <MsgAlt msg={item} user={user} contentWidth={contentWidth} />
              );
            }}
            ListHeaderComponent={<HeaderComponent />}
            ListFooterComponent={
              <View style={{ flex: 1, height: chatMsgsFooterOffset }} />
            }
          />
        </View>
        <MsgTextarea
          onSend={probeUiHelper.sendMsg}
          onAttach={probeUiHelper.trySendAttachment}
          keyboardOffset={keyboardOffset}
        />
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  content: {
    flex: 1,
  },
  msgsList: {
    paddingHorizontal: sizes.CONTENT_HORIZONTAL_PADDING,
  },
});

function mapStateToProps(state, { navigation }) {
  const { cachedUsers, chat, me, common } = state;

  return {
    cachedUsers,
    chat,
    me,
    common,
    navigation,
  };
}

export default connect(mapStateToProps)(ChatScreen);
