/*******************************
 * Helpfull link for understanding how it works (apollow with react navigaion)
 * http://engineering.khanacademy.org/posts/creating-query-components-with-apollo.htm
 ******************************/

import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  FlatList,
  TouchableOpacity,
  Alert,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';

import store from '../store';

import { ErrorView } from '../components/ErrorView';
import { LoadingView } from '../components/LoadingView';
import { BtnDefault, Hint } from '../components/Buttons';
import { BackBtnNavHeader } from '../components/NavHeaders';
import { sizes, colors, baseStyles } from '../utils/theme';
import navService from '../utils/nav-service';
import dataService from '../utils/data-service';
import { projDraftStatuses } from '../utils/constants';
import projDraftEditUiHelper from '../utils/ui-helpers/projDraft-edit-ui-helper';

async function tryCreateRoleDraft(projDraftId) {
  await projDraftEditUiHelper.tryCreateRoleDraft(projDraftId);
}

const descHint =
  'Опишите сценарий, требования, где проводятся сьемки, условия, все что необходимо знать актерам';
const countryHint =
  'Страна, где вы ищете актеров, либо где проходят сьемки. Если же их несколько, выберите любую';
const cityHint =
  'Город, где вы ищете актеров, либо где проходят сьемки. Если же их несколько, оставьте любой';

function RoleItem(roleDraft) {
  const { _id: roleDraftId, projDraftId, roleId, title, shortDesc } = roleDraft;
  return (
    <View
      style={{
        paddingVertical: 8,
        marginBottom: 8,
        flexDirection: 'row',
        borderBottomWidth: sizes.BORDER_SEPARATOR_WIDTH,
        borderBottomColor: colors.BORDER_SEPARATOR_COLOR,
        alignItems: 'center',
      }}
    >
      <View style={{ flex: 1 }}>
        <Text style={{ fontWeight: 'bold' }}>{title || 'Новая роль'}</Text>
        {shortDesc && <Text style={{}}>{shortDesc}</Text>}
      </View>

      <TouchableOpacity
        style={[
          styles.btn,
          {
            width: 28,
            height: 28,
            marginRight: 8,
          },
        ]}
        onPress={() =>
          navService.navigate('RoleDraftEdit', { projDraftId, roleDraftId })
        }
      >
        <FontAwesome name={'pencil'} size={20} color={'white'} />
      </TouchableOpacity>

      {!roleId && (
        <TouchableOpacity
          style={[
            styles.btn,
            {
              width: 28,
              height: 28,
            },
          ]}
          onPress={() =>
            projDraftEditUiHelper.tryDeleteRoleDraft({
              projDraftId,
              roleDraftId,
              title,
            })
          }
        >
          <FontAwesome name={'remove'} size={20} color={'white'} />
        </TouchableOpacity>
      )}
    </View>
  );
}

function Roles({ projDraft }) {
  return (
    <View style={{ marginBottom: 12 }}>
      <FlatList
        contentContainerStyle={styles.listContainer}
        data={projDraft.roles}
        keyExtractor={item => item._id}
        renderItem={({ item }) => {
          return <RoleItem {...item} />;
        }}
      />

      <View style={{ flexDirection: 'row-reverse', alignItems: 'flex-start' }}>
        <TouchableOpacity
          style={[
            styles.btn,
            {
              paddingVertical: 8,
              paddingHorizontal: 16,
              flexDirection: 'row',
            },
          ]}
          onPress={() => tryCreateRoleDraft(projDraft._id)}
        >
          <Text style={{ color: 'white', marginRight: 4 }}>Добавить роль</Text>
          <FontAwesome style={{}} name={'plus'} size={20} color={'white'} />
        </TouchableOpacity>
      </View>
    </View>
  );
}

class ProjDraftEditScreen extends React.Component {
  constructor(props) {
    super(props);
    projDraftEditUiHelper.componentInit(this);
  }

  componentDidMount() {
    let { projDraft } = this.props;
    projDraftEditUiHelper.componentDidMount({ projDraft });
  }

  render() {
    const {
      me,
      projDraftId,
      projDraft,
      proj,
      error,
      errorText,
      loading,
      loadingText,
    } = this.props;
    const screenTitle = 'Черновик';
    const { title: subTitle, status } = projDraft;

    if (loading) {
      return (
        <LoadingView
          title={screenTitle}
          subTitle={subTitle}
          loadingText={loadingText}
          onBackPress={projDraftEditUiHelper.goBack}
        />
      );
    }

    if (error) {
      return (
        <ErrorView
          title={screenTitle}
          subTitle={subTitle}
          errorText={errorText}
          onBackPress={projDraftEditUiHelper.goBack}
          contentBtn={[
            {
              text: 'Обновить',
              onPress: () => {
                // dont wait result
                dataService.dispatchProjDraft({ projDraftId });
              },
            },
          ]}
        />
      );
    }

    const moderating = me.isModer && status === projDraftStatuses.MODERATING;

    return (
      <KeyboardAvoidingView
        style={baseStyles.container}
        enabled
        behavior="padding"
      >
        <BackBtnNavHeader
          title={screenTitle}
          subTitle={subTitle}
          onBackPress={projDraftEditUiHelper.trySave}
        />

        <View style={{ flex: 1 }}>
          <ScrollView>
            <View style={baseStyles.scrollViewContent}>
              <Text style={baseStyles.textboxHint}>Название проекта:</Text>

              <TextInput
                style={[baseStyles.textbox]}
                underlineColorAndroid="transparent"
                placeholder={'Название кастинга'}
                value={this.state.title}
                onChangeText={title => this.setState({ title })}
              />

              <View style={{ flexDirection: 'row' }}>
                <Text style={baseStyles.textboxHint}>Описание:</Text>
                <Hint text={descHint} />
              </View>

              <TextInput
                style={[baseStyles.textbox]}
                underlineColorAndroid="transparent"
                multiline={true}
                minHeight={160}
                placeholder={'Описание кастинга'}
                value={this.state.desc}
                onChangeText={desc => this.setState({ desc })}
              />

              <View style={{ flexDirection: 'row' }}>
                <Text style={baseStyles.textboxHint}>Страна:</Text>
                <Hint text={countryHint} />
              </View>

              <TextInput
                style={[baseStyles.textbox]}
                underlineColorAndroid="transparent"
                placeholder={'Страна'}
                value={this.state.country}
                onChangeText={country => this.setState({ country })}
              />

              <View style={{ flexDirection: 'row' }}>
                <Text style={baseStyles.textboxHint}>Город:</Text>
                <Hint text={cityHint} />
              </View>

              <TextInput
                style={[baseStyles.textbox]}
                underlineColorAndroid="transparent"
                placeholder={'Город'}
                value={this.state.city}
                onChangeText={city => this.setState({ city })}
              />

              <Text style={baseStyles.textboxHint}>Роли:</Text>

              <Roles projDraft={projDraft} />

              {!moderating && (
                <View>
                  <Text style={baseStyles.textboxHint}>
                    Сообщение модератору:
                  </Text>
                  <TextInput
                    style={[baseStyles.textbox, {}]}
                    underlineColorAndroid="transparent"
                    minHeight={80}
                    multiline={true}
                    placeholder={
                      'Если что-то не понятно, либо есть просьба, оставьте здесь сообщение.'
                    }
                    value={this.state.dirComment}
                    onChangeText={dirComment => this.setState({ dirComment })}
                  />
                </View>
              )}

              {moderating && (
                <View>
                  <Text style={baseStyles.textboxHint}>
                    Ваше сообщение режисеру (Вы Модератор):
                  </Text>
                  <TextInput
                    style={[baseStyles.textbox, {}]}
                    underlineColorAndroid="transparent"
                    minHeight={80}
                    multiline={true}
                    placeholder={
                      'Если вы не можете принят проект, напишите причину, и что нужно исправить'
                    }
                    value={this.state.moderComment}
                    onChangeText={moderComment =>
                      this.setState({ moderComment })
                    }
                  />
                </View>
              )}
            </View>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  listContainer: {
    alignSelf: 'stretch',
  },
  btn: {
    backgroundColor: colors.BTN_DEFAULT_BG,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

function mapStateToProps(state) {
  let { me, projDraftCard } = state;
  let {
    projDraftId,
    projDraft,
    proj,
    error,
    errorText,
    loading,
    loadingText,
  } = projDraftCard;

  return {
    me,
    projDraftId,
    projDraft,
    proj,
    error,
    errorText,
    loading,
    loadingText,
  };
}

export default connect(mapStateToProps)(ProjDraftEditScreen);
