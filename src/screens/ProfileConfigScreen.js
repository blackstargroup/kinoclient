import React from 'react';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Button,
  Image,
  Alert,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';

import store from '../store';
import dataService from '../utils/data-service';
import { Avatar } from '../components/Thumbs';
import { BtnsNavHeader } from '../components/NavHeaders';
import { AvatarUploadingView } from '../components/AvatarUploadingView';

import { baseStyles } from '../utils/theme';
import accountManager from '../utils/account-manager';
import navService from '../utils/nav-service';
import reportService from '../utils/report-service';

function goBack() {
  navService.navigate('General');
}

function isFormAffected(form) {
  let entries = Object.entries(form);
  let { me } = store.getState();
  let affected = false;
  entries.forEach(([key, value]) => {
    if (me[key] !== value) {
      affected = true;
    }
  });

  return affected;
}

function clearFormStateAndExit(self) {
  let { firstName, lastName } = self.props.me;
  self.setState({ firstName, lastName });
  goBack();
}

async function tryExit(self) {
  let { firstName, lastName } = self.state;
  if (isFormAffected({ firstName, lastName })) {
    Alert.alert(
      'Несохраненные изменения',
      'У вас есть несохраненные изменения. Точно отменить?',
      [
        {
          text: 'Нет',
          onPress: () => {},
        },
        {
          text: 'Да',
          onPress: () => clearFormStateAndExit(self),
        },
      ],
    );
    return;
  }
  clearFormStateAndExit(self);
}

async function trySave(self) {
  let { firstName, lastName } = self.state;
  if (isFormAffected({ firstName, lastName })) {
    try {
      await dataService.updateMe({ firstName, lastName });
    } catch (error) {
      reportService.sendErrorReportSafety({ title: 'update profile', error });
      console.warn(error);
      return;
    }
  }

  goBack();
}

class ProfileEdit extends React.Component {
  constructor(props) {
    super(props);
    let { firstName, lastName } = props.me;
    this.state = { firstName, lastName };
  }

  render() {
    const { me, avatarUploadStatus } = this.props;
    const title = 'Профиль';

    if (avatarUploadStatus.visible || avatarUploadStatus.error) {
      return (
        <AvatarUploadingView
          avatarUploadStatus={avatarUploadStatus}
          title={title}
          onBackPress={goBack}
        />
      );
    }

    let self = this;

    return (
      <KeyboardAvoidingView style={styles.container} enabled behavior="padding">
        <BtnsNavHeader
          title={title}
          leftTitle={'Отмена'}
          onLeftPress={() => tryExit(self)}
          rightTitle={'Готово'}
          onRightPress={() => trySave(self)}
        />

        <View style={{ flex: 1 }}>
          <ScrollView style={baseStyles.content}>
            <View style={styles.avatarContainer}>
              <TouchableOpacity onPress={accountManager.changeAvatar}>
                <Avatar size={120} path={me.avatar.path} />
              </TouchableOpacity>

              <Button
                title="Сменить фото профиля"
                onPress={accountManager.changeAvatar}
              />
            </View>
            <View style={styles.propSection}>
              <View style={styles.propName}>
                <Text>Имя</Text>
              </View>
              <TextInput
                style={styles.propInput}
                underlineColorAndroid="transparent"
                value={this.state.firstName}
                placeholder="Имя"
                onChangeText={firstName => this.setState({ firstName })}
              />
            </View>
            <View style={styles.propSection}>
              <View style={styles.propName}>
                <Text>Фамилия</Text>
              </View>
              <TextInput
                style={styles.propInput}
                underlineColorAndroid="transparent"
                value={this.state.lastName}
                placeholder="Фамилия"
                onChangeText={lastName => this.setState({ lastName })}
              />
            </View>
          </ScrollView>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
  },
  button: {
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  welcomeView: {
    alignItems: 'center',
  },
  propSection: { flexDirection: 'row', backgroundColor: 'white' },
  propName: {
    flex: 2,
    justifyContent: 'center',
    paddingLeft: 10,
    paddingVertical: 10,
  },
  propInput: {
    flex: 3,
    paddingLeft: 10,
  },
  avatarContainer: { flex: 1, justifyContent: 'center', alignItems: 'center' },
});

function mapStateToProps(state) {
  return {
    ...state,
  };
}

export default connect(mapStateToProps)(ProfileEdit);
