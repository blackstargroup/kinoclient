import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  FlatList,
  TouchableOpacity,
  TextInput,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import Swipeout from 'react-native-swipeout';
import { graphql } from 'react-apollo';

import store, { client } from '../store';
import navService from '../utils/nav-service';
import { intervals } from '../utils/constants';
import { colors, sizes } from '../utils/theme';
import { Avatar, ProjThumb } from '../components/Thumbs';
import { NavHeader } from '../components/NavHeaders';
import { ErrorView } from '../components/ErrorView';
import { LoadingView } from '../components/LoadingView';

import probeUiHelper from '../utils/ui-helpers/probe-ui-helper';
/*

Show unread messages on navigation panel:
https://github.com/react-navigation/react-navigation/issues/766

https://wix.github.io/react-native-navigation/#/screen-api

*/

function UnreadPoint({ count }) {
  if (!count) return null;

  return (
    <View style={styles.unreadPoint}>
      <Text style={styles.unreadPointText}>{count}</Text>
    </View>
  );
}

class NotificationsScreen extends React.Component {
  componentDidMount() {}

  renderChat({ item }) {
    const { _id, isActor, title, thumbPath, msgText, unreadCount } = item;
    let probeId = _id;

    if (isActor) {
      const actorBtns = [
        {
          text: 'Удалить',
          backgroundColor: 'red',
          onPress: () => probeUiHelper.tryDeleteProbe({ probeId }),
        },
      ];

      return (
        <View>
          <Swipeout style={styles.swipeout} right={actorBtns}>
            <TouchableOpacity
              style={{
                padding: 10,
                backgroundColor: 'transparent',
                flexDirection: 'row',
              }}
              onPress={() => {
                navService.navigate('ProbeAsActor', { probeId: _id });
              }}
            >
              <View>
                <ProjThumb size={50} path={thumbPath} />
                <UnreadPoint count={unreadCount} />
              </View>

              <View style={styles.itemTextContainer}>
                <Text>{title}</Text>
                <Text numberOfLines={1} style={styles.itemMsgText}>
                  {msgText}
                </Text>
              </View>
            </TouchableOpacity>
          </Swipeout>
        </View>
      );
    }

    const directorBtns = [
      {
        component: (
          <View style={styles.deleteBtn}>
            <Text style={{ color: 'white' }}>{'Удалить'}</Text>
            <Text style={{ color: 'white' }}>{'у меня'}</Text>
          </View>
        ),
        backgroundColor: 'red',
        color: 'white',
        onPress: () => probeUiHelper.tryDeleteProbe({ probeId }),
      },
    ];

    return (
      <View>
        <Swipeout style={styles.swipeout} right={directorBtns}>
          <TouchableOpacity
            style={{ padding: 10, flexDirection: 'row' }}
            onPress={() => {
              navService.navigate('ProbeAsDirector', { probeId: _id });
            }}
          >
            <View>
              <Avatar size={50} path={thumbPath} />
              <UnreadPoint count={unreadCount} />
            </View>
            <View style={styles.itemTextContainer}>
              <Text numberOfLines={1}>{title}</Text>
              <Text numberOfLines={1} style={styles.itemMsgText}>
                {msgText}
              </Text>
            </View>
          </TouchableOpacity>
        </Swipeout>
      </View>
    );
  }

  render() {
    const title = 'Пробы';

    const { loading, error, items } = this.props;

    if (loading) {
      return <LoadingView title={title} />;
    }

    if (error) {
      return <ErrorView title={title} />;
    }

    if (!items.length) {
      return (
        <View style={styles.container}>
          <NavHeader title={title} />

          <View style={styles.noProbes}>
            <Text>Тут будут Ваши пробы</Text>
            <Text style={{ color: colors.GREY_TEXT_COLOR }}>
              {' '}
              Выберите кастинг, выполните задание и ждите ответ от режисера
            </Text>
          </View>
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <NavHeader title={title} />

        <FlatList
          ListHeaderComponent={<View style={{ flex: 1, height: 16 }} />}
          contentContainerStyle={styles.listContainer}
          data={items}
          keyExtractor={item => item._id}
          renderItem={this.renderChat}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  noProbes: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: sizes.CONTENT_HORIZONTAL_PADDING,
  },
  itemTextContainer: {
    marginLeft: 10,
    marginRight: 40,
    flex: 1,
    flexDirection: 'column',
    flexWrap: 'wrap',
  },
  itemMsgText: {
    color: '#999',
  },
  swipeout: {
    marginBottom: 5,
    borderBottomWidth: sizes.BORDER_SEPARATOR_WIDTH,
    borderBottomColor: colors.BORDER_SEPARATOR_COLOR,
    backgroundColor: colors.TRANSPARENT,
  },
  deleteBtn: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  unreadPoint: {
    position: 'absolute',
    width: 20,
    height: 20,
    borderRadius: 10,
    right: -5,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
  },
  unreadPointText: {
    color: 'white',
    fontSize: 12,
    fontWeight: 'bold',
  },
});

function mapStateToProps(state, { navigation }) {
  const { probes, me } = state;
  const { loading, error, items } = probes;
  return {
    loading,
    error,
    items,
    me,
    navigation,
  };
}

export default connect(mapStateToProps)(NotificationsScreen);
