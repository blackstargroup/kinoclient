import React from 'react';
import {
  StyleSheet,
  TextInput,
  Text,
  ScrollView,
  View,
  Alert,
  Button,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import { connect } from 'react-redux';

import { BtnDefault, BtnTransparent } from '../components/Buttons';
import { BackBtnNavHeader } from '../components/NavHeaders';
import navService from '../utils/nav-service';
import reportService from '../utils/report-service';
import { ErrorView } from '../components/ErrorView';
import { LoadingView } from '../components/LoadingView';
import { sleep } from '../utils/helpers/sleep';
import { colors, sizes, baseStyles } from '../utils/theme';
import { DEV_SLEEP } from '../utils/constants';

const screenTitle = 'Отчеты';

function goBack() {
  navService.navigate('General');
}

async function getReports(self) {
  try {
    self.setState({ ...initialState, loading: true });
    __DEV__ && (await sleep(DEV_SLEEP));
    let reports = await reportService.getReports();
    self.setState({ ...initialState, reports, success: true });
  } catch (error) {
    self.setState({ ...initialState, error: true });
  }
}

const initialState = {
  success: false,
  loading: false,
  error: false,
  reports: [],
};

function ReportItem({ item }) {
  const { _id, userTitle, kind, title, desc, createdAt } = item;
  let icon;
  switch (kind) {
    case 'msg':
      icon = '🤓';
      break;
    case 'error':
      icon = '🐞';
      break;
    default:
      icon = '❓';
      break;
  }

  return (
    <View style={styles.item}>
      <TouchableOpacity style={styles.button} onPress={() => {}}>
        <View
          style={{
            marginLeft: 10,
            marginRight: 40,
            // flex: 1,
            flexDirection: 'column',
            flexWrap: 'wrap',
          }}
        >
          <Text style={{ color: colors.CONTENT_TEXT_COLOR }}>
            {icon} {title}
          </Text>
          <Text style={{ color: colors.GREY_TEXT_COLOR }}>{desc}</Text>
          <Text style={{ color: colors.GREY_TEXT_COLOR }}>{userTitle}</Text>
          <Text style={{ color: colors.GREY_TEXT_COLOR }}>{createdAt}</Text>
        </View>
      </TouchableOpacity>
    </View>
  );
}

class ReportsScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  componentDidMount() {
    this.setState(initialState);
  }

  componentDidMount() {
    getReports(this);
  }

  render() {
    let self = this;

    if (this.state.loading) {
      return (
        <LoadingView
          title={screenTitle}
          onBackPress={goBack}
          loadingText={'Стягиваем репорты...'}
        />
      );
    }

    if (this.state.error) {
      return (
        <ErrorView
          title={screenTitle}
          onBackPress={goBack}
          errorText={'Произошла ошибка загрузки репортов'}
          contentBtns={[
            {
              text: 'ОК',
              onPress: () => self.setState(initialState),
            },
          ]}
        />
      );
    }

    let reports = this.state.reports;

    return (
      <View style={baseStyles.container}>
        <BackBtnNavHeader title={screenTitle} onBackPress={goBack} />
        <View style={{ flex: 1 }}>
          <ScrollView
            style={{ flex: 1 }}
            contentContainerStyle={{ flexGrow: 1 }}
          >
            <View style={[baseStyles.scrollViewContent, { flexGrow: 1 }]}>
              <FlatList
                contentContainerStyle={styles.listContainer}
                data={reports}
                keyExtractor={report => report._id}
                renderItem={ReportItem}
              />

              <View
                style={{
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingVertical: 40,
                }}
              >
                <BtnDefault
                  title={'Обновить'}
                  onPress={() => getReports(self)}
                />
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}

// PS текст можно сохранять в device storage наслучай если вылезла ошибка
// чтоб пользователь не потерял написанное

export default ReportsScreen;

const styles = StyleSheet.create({
  item: {
    marginBottom: 5,
    borderBottomWidth: sizes.BORDER_SEPARATOR_WIDTH,
    borderBottomColor: colors.BORDER_SEPARATOR_COLOR,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    flex: 1,
    alignItems: 'center',
    padding: 10,
    flexDirection: 'row',
  },
});
