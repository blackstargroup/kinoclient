import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  FlatList,
  TouchableOpacity,
  Alert,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { sizes, colors, baseStyles } from '../utils/theme';

export function Btn({ title, onPress }) {
  return (
    <TouchableOpacity
      style={{ backgroundColor: colors.BTN_BG }}
      onPress={onPress}
    >
      <Text
        style={{
          paddingVertical: 12,
          paddingHorizontal: 24,
          fontSize: 16,
          color: colors.BTN_DEFAULT_TEXT_COLOR,
        }}
      >
        {title}
      </Text>
    </TouchableOpacity>
  );
}

export function BtnDefault({ title, onPress, style }) {
  return (
    <TouchableOpacity
      style={[{ backgroundColor: colors.BTN_DEFAULT_BG }, style]}
      onPress={onPress}
    >
      <Text
        style={{
          paddingVertical: 12,
          paddingHorizontal: 24,
          fontSize: 16,
          color: colors.BTN_DEFAULT_TEXT_COLOR,
        }}
      >
        {title}
      </Text>
    </TouchableOpacity>
  );
}

export function BtnTransparent({ title, onPress }) {
  return (
    <TouchableOpacity
      style={{ backgroundColor: colors.TRANSPARENT }}
      onPress={onPress}
    >
      <Text
        style={{
          paddingVertical: 12,
          paddingHorizontal: 24,
          fontSize: 16,
          color: colors.BTN_DEFAULT_BG,
        }}
      >
        {title}
      </Text>
    </TouchableOpacity>
  );
}

export function BtnTransparentRed({ title, onPress }) {
  return (
    <TouchableOpacity
      style={{ backgroundColor: colors.TRANSPARENT }}
      onPress={onPress}
    >
      <Text
        style={{
          paddingVertical: 12,
          paddingHorizontal: 24,
          fontSize: 16,
          color: 'red',
        }}
      >
        {title}
      </Text>
    </TouchableOpacity>
  );
}

export function Hint({ text }) {
  if (!text) return null;

  return (
    <TouchableOpacity onPress={() => Alert.alert('Подсказка', text)}>
      <MaterialCommunityIcons
        name={'comment-question'}
        size={20}
        color={colors.CONTENT_ICON_COLOR}
      />
    </TouchableOpacity>
  );
}
