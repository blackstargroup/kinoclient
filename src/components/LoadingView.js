import React from 'react';
import { Image, View, Text, ActivityIndicator } from 'react-native';

import { BtnDefault } from './Buttons';
import { NavHeader, BackBtnNavHeader } from './NavHeaders';
import { colors } from '../utils/theme';

export function LoadingView({
  title,
  subTitle,
  onBackPress,
  loadingText,
  progress,
}) {
  if (!title) {
    return <Loading loadingText={loadingText} progress={progress} />;
  }

  if (!onBackPress) {
    return (
      <View style={{ flex: 1 }}>
        <NavHeader title={title} subTitle={subTitle} />
        <Loading loadingText={loadingText} progress={progress} />
      </View>
    );
  }

  return (
    <View style={{ flex: 1 }}>
      <BackBtnNavHeader
        title={title}
        subTitle={subTitle}
        onBackPress={onBackPress}
      />
      <Loading loadingText={loadingText} progress={progress} />
    </View>
  );
}

function Loading({ loadingText, progress }) {
  let text;
  if (progress) {
    text = `${loadingText || '🤔 Загрузка'}... ${progress}%`;
  } else if (loadingText) {
    text = `${loadingText}...`;
  }

  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      {text && <Text>{text}</Text>}
      <ActivityIndicator size="large" />
    </View>
  );
}
