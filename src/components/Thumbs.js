/*
  Using standard Images is a bad solution, because we hav bad caching
  https://medium.com/react-native-training/caching-images-in-react-native-709fd94b3e71

  also Image.source  cache property works only on ios 
  todo: need to use React-native-cached-image package
*/

import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Platform,
  Image,
  TouchableHighlight,
  TouchableOpacity,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { colors } from '../utils/theme';
import { NO_AVATAR, NO_PROJ_THUMB } from '../utils/constants';

const IS_ANDROID = Platform.OS === 'android';

export function Thumb({ source, action, actionIcon, thumbSize }) {
  const iconSize = 20;
  const iconCyrcleSize = 26;
  const iconCyrcleRadius = iconCyrcleSize / 2;

  return (
    <View style={{ padding: 5 }}>
      <View>
        <View style={{ position: 'absolute' }}>
          <Image
            style={{
              width: thumbSize,
              height: thumbSize,
            }}
            source={source}
          />
        </View>
        <View
          style={{
            position: 'relative',
            width: thumbSize,
            height: thumbSize,
            padding: 5,
          }}
        >
          <TouchableHighlight
            style={{
              alignSelf: 'flex-end',
              backgroundColor: 'white',
              width: iconCyrcleSize,
              height: iconCyrcleSize,
              borderRadius: iconCyrcleRadius,
              justifyContent: 'center',
              alignItems: 'center',
            }}
            onPress={action}
          >
            <FontAwesome
              name={actionIcon}
              size={iconSize}
              color={colors.ADD_RM_IMAGE_BTN_TEXT}
            />
          </TouchableHighlight>
        </View>
      </View>
    </View>
  );
}

export function ThumbAddVideo({ action, thumbSize }) {
  const iconSize = 20;
  const iconCyrcleSize = 26;
  const iconCyrcleRadius = iconCyrcleSize / 2;

  return (
    <View style={{ padding: 5 }}>
      <TouchableOpacity onPress={action}>
        <View style={{ position: 'absolute' }}>
          <FontAwesome
            name={'file-video-o'}
            size={thumbSize}
            color={colors.ADD_RM_IMAGE_BTN_TEXT}
          />
        </View>
        <View
          style={{
            position: 'relative',
            width: thumbSize,
            height: thumbSize,
            padding: 5,
          }}
        >
          <View
            style={{
              alignSelf: 'flex-end',
              backgroundColor: 'white',
              width: iconCyrcleSize,
              height: iconCyrcleSize,
              borderRadius: iconCyrcleRadius,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <FontAwesome
              name={'plus'}
              size={iconSize}
              color={colors.ADD_RM_IMAGE_BTN_TEXT}
            />
          </View>
        </View>
      </TouchableOpacity>
    </View>
  );
}

export function Avatar({ path, size }) {
  let style = {
    width: size,
    height: size,
    borderRadius: size / 2,
  };

  let containerStyle = {
    width: size,
    height: size,
    borderRadius: size / 2,
    backgroundColor: 'white',
  };

  // if we set source{{uri: ''}} then we have a warning
  // thats why we have if statement

  if (path) {
    return (
      <View style={containerStyle}>
        <Image
          style={style}
          source={{
            uri: path,
            cache: 'force-cache',
          }}
          defaultSource={NO_AVATAR}
        />
      </View>
    );
  }

  // android doesn't understand defaultSource prop
  // https://stackoverflow.com/questions/44644755/react-native-how-to-set-image-defaultsource-on-android
  return <Image style={style} source={NO_AVATAR} />;
}

export function AvatarWithText({ path, text, size }) {
  const radius = size / 2;

  return (
    <View>
      <Avatar size={size} path={path} />
      <View
        style={{
          width: size,
          height: size,
          borderRadius: radius,
          position: 'absolute',
          justifyContent: 'center',
          alignContent: 'center',
          backgroundColor: 'rgba(0, 0, 0, 0.4)',
        }}
      >
        <Text
          style={{
            color: 'white',
            fontWeight: 'bold',
            textAlign: 'center',
          }}
        >
          {text}
        </Text>
      </View>
    </View>
  );
}

export function ProjThumb({ path, size }) {
  let containerStyle = {
    width: size,
    height: size,
    backgroundColor: 'white',
  };

  let style = {
    ...containerStyle,
    borderRadius: 5,
    borderWidth: 0.5,
    borderColor: 'lightgrey',
  };

  if (path) {
    return (
      <View style={containerStyle}>
        <Image
          style={style}
          source={{
            uri: path,
            cache: 'force-cache',
          }}
          defaultSource={NO_PROJ_THUMB}
        />
      </View>
    );
  }

  // android doesn't understand defaultSource prop
  // https://stackoverflow.com/questions/44644755/react-native-how-to-set-image-defaultsource-on-android
  return <Image style={style} source={NO_PROJ_THUMB} />;
}
