import React from 'react';
import { Image, View, Text, ActivityIndicator } from 'react-native';

import { BtnDefault } from './Buttons';
import { NavHeader, BackBtnNavHeader } from './NavHeaders';
import { colors } from '../utils/theme';

export function ErrorView({
  title,
  subTitle,
  onBackPress,
  errorText,
  errorSubText,
  contentBtns,
}) {
  if (!onBackPress) {
    if (!title) {
      return (
        <Error
          errorText={errorText}
          errorSubText={errorSubText}
          contentBtns={contentBtns}
        />
      );
    }

    return (
      <View style={{ flex: 1 }}>
        <NavHeader title={title} subTitle={subTitle} />
        <Error
          errorText={errorText}
          errorSubText={errorSubText}
          contentBtns={contentBtns}
        />
      </View>
    );
  }

  return (
    <View style={{ flex: 1 }}>
      <BackBtnNavHeader
        title={title}
        subTitle={subTitle}
        onBackPress={onBackPress}
      />
      <Error
        errorText={errorText}
        errorSubText={errorSubText}
        contentBtns={contentBtns}
      />
    </View>
  );
}

function Error({ errorText, errorSubText, contentBtns }) {
  errorText = errorText || 'Произошла ошибка во время загрузки 😔';
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text style={{ color: colors.CONTENT_TEXT_COLOR }}>{errorText}</Text>
      {errorSubText && <Text style={{ color: colors.GREY_TEXT_COLOR }} />}

      {contentBtns &&
        contentBtns.length &&
        contentBtns.map((btn, key) => (
          <BtnDefault
            key={key}
            style={{ marginTop: 24 }}
            title={btn.text}
            onPress={btn.onPress}
          />
        ))}
    </View>
  );
}
