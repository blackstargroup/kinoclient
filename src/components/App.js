import React from 'react';
import { ApolloProvider } from 'react-apollo';

import store, { client } from '../store';
import Layout from './Layout';
import reportService from '../utils/report-service';

export default class App extends React.Component {
  componentDidCatch(error, info) {
    let { componentStack } = info;
    let title = `Component error, stack: ${componentStack}`;
    reportService.sendErrorReportSafety({ title, error });
  }

  render() {
    return (
      <ApolloProvider store={store} client={client}>
        <Layout />
      </ApolloProvider>
    );
  }
}
