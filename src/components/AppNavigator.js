import React from 'react';
import { View, Text } from 'react-native';
import {
  createStackNavigator,
  createBottomTabNavigator,
} from 'react-navigation';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

/* Line below dismiss yellow box warning. Need investigate more why this warning happens */
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings([
  'Warning: isMounted(...) is deprecated',
  'Module RCTImageLoader',
  // uncomment following on testing: need investigate this issues
  'Required dispatch_sync to load constants for RCTAppState',
  'RCTBridge required dispatch_sync to load RNDeviceInfo',
  'RCTBridge required dispatch_sync to load RCTAppState',
  'RCTBridge required dispatch_sync to load RCTDevLoadingView',
  'Required dispatch_sync to load constants for RNDeviceInfo.',
  'Class RCTCxxModule was not exported.',
  'Sending `FCMTokenRefreshed` with no listeners registered', // todo: check notifs and fix that
]);
/*************************************/

import ProbeAsActorScreen from '../screens/ProbeAsActorScreen';
import ProbeAsDirectorScreen from '../screens/ProbeAsDirectorScreen';

import AccountScreen from '../screens/AccountScreen';
import ContactScreen from '../screens/ContactScreen';
import ReportsScreen from '../screens/ReportsScreen';
import NewProjScreen from '../screens/NewProjScreen';
import NotificationsScreen from '../screens/NotificationsScreen';
import ProjsScreen from '../screens/ProjsScreen';
import ProjCardScreen from '../screens/ProjCardScreen';
import ProjDraftCardScreen from '../screens/ProjDraftCardScreen';
import ProjDraftEditScreen from '../screens/ProjDraftEditScreen';
import RoleDraftEditScreen from '../screens/RoleDraftEditScreen';
import ProfileConfigScreen from '../screens/ProfileConfigScreen';
import RoleCardScreen from '../screens/RoleCardScreen';
import ProbeDraftScreen from '../screens/ProbeDraftScreen';
import GeneralScreen from '../screens/GeneralScreen';
import { colors, sizes } from '../utils/theme';

const TAB_ICON_SIZE = 20;

const cardStyle = {
  backgroundColor: colors.CONTENT_BG,
};

const SettingsNavigator = createStackNavigator(
  {
    General: {
      screen: GeneralScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    ProfileConfig: {
      screen: ProfileConfigScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    Account: {
      screen: AccountScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    Contact: {
      screen: ContactScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    NewProj: {
      screen: NewProjScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    Reports: {
      screen: ReportsScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
  },
  {
    initialRouteName: 'General',
    cardStyle,
  },
);

const ProjsNavigator = createStackNavigator(
  {
    Projs: {
      screen: ProjsScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    ProjCard: {
      screen: ProjCardScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    ProjDraftCard: {
      screen: ProjDraftCardScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    ProjDraftEdit: {
      screen: ProjDraftEditScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    RoleDraftEdit: {
      screen: RoleDraftEditScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    RoleCard: {
      screen: RoleCardScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    ProbeDraft: {
      screen: ProbeDraftScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
  },
  {
    cardStyle,
  },
);

const NotificationsNavigator = createStackNavigator(
  {
    Notifications: {
      screen: NotificationsScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    ProbeAsActor: {
      screen: ProbeAsActorScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
    ProbeAsDirector: {
      screen: ProbeAsDirectorScreen,
      navigationOptions: () => ({
        header: null,
      }),
    },
  },
  {
    initialRouteName: 'Notifications',
    cardStyle,
  },
);

const AppNavigator = createBottomTabNavigator(
  {
    Home: {
      screen: ProjsNavigator,
      navigationOptions: () => ({
        tabBarIcon: ({ tintColor }) => (
          <FontAwesome size={TAB_ICON_SIZE} color={tintColor} name="home" />
        ),
      }),
    },
    Notifications: {
      screen: NotificationsNavigator,
      navigationOptions: () => ({
        tabBarIcon: ({ tintColor }) => (
          <FontAwesome size={TAB_ICON_SIZE} color={tintColor} name="comments" />
        ),
      }),
    },
    Settings: {
      screen: SettingsNavigator,
      navigationOptions: () => ({
        tabBarIcon: ({ tintColor }) => (
          <FontAwesome size={TAB_ICON_SIZE} color={tintColor} name="gear" />
        ),
      }),
    },
  },
  {
    lazy: true,
    tabBarPosition: 'bottom',
    initialRouteName: 'Home',
    swipeEnabled: false,

    tabBarOptions: {
      showIcon: true,
      showLabel: false,
      activeTintColor: colors.NAV_ACTIVE_BTN,
      inactiveTintColor: colors.NAV_INACTIVE_BTN,
      style: {
        backgroundColor: colors.NAV_BAR_COLOR,
        height: sizes.TAB_NAV_HEIGHT,
        paddingVertical: 5,
        borderTopColor: colors.NAV_BAR_BORDER,
        borderTopWidth: sizes.BORDER_SEPARATOR_WIDTH,
      },
    },
  },
);

export default AppNavigator;
