import React from 'react';
import { Image, Button, View, Text, ActivityIndicator } from 'react-native';
import { ErrorView } from './ErrorView';
import { LoadingView } from './LoadingView';

import { uploadingFinish } from '../actions/avatarUploadStatus';
import store from '../store';

export function AvatarUploadingView({
  avatarUploadStatus,
  title,
  onBackPress,
}) {
  function hide() {
    store.dispatch(uploadingFinish());
  }

  function backAndHide() {
    hide();
    onBackPress();
  }

  if (avatarUploadStatus.error) {
    return (
      <ErrorView
        title={title}
        onBackPress={backAndHide}
        errorText={avatarUploadStatus.errorText}
        errorSubText={avatarUploadStatus.errorSubText}
        contentBtns={[
          {
            text: 'OK',
            onPress: hide,
          },
        ]}
      />
    );
  }

  if (avatarUploadStatus.visible) {
    return (
      <LoadingView
        loadingText={avatarUploadStatus.status}
        progress={avatarUploadStatus.progress}
        title={title}
        onBackPress={backAndHide}
      />
    );
  }
}
