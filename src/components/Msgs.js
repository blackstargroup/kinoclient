import React from 'react';

import {
  StyleSheet,
  Text,
  View,
  Button,
  FlatList,
  Dimensions,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { Avatar } from '../components/Thumbs';
import { colors } from '../utils/theme';
import { msgTypes } from '../utils/constants';
import probeUiHelper from '../utils/ui-helpers/probe-ui-helper';

function AltImage(user, { attachment }, contentWidth) {
  let { path, ratio } = attachment;
  let imgHeight = (0.8 * contentWidth) / ratio;

  let avatarPath;
  if (user) {
    avatarPath = user.avatar.path;
  }

  return (
    <View style={{ marginTop: 10, flexDirection: 'row' }}>
      <View style={{ paddingRight: 12 }}>
        <View style={styles.avatar}>
          <Avatar size={32} path={avatarPath} />
        </View>
      </View>
      <Image
        style={{
          flex: 1,
          height: imgHeight,
        }}
        source={{ uri: path }}
        resizeMode={'cover'}
      />
      <View style={{ flex: 0.2 }} />
    </View>
  );
}

function AltVideo(user, { attachment }, contentWidth) {
  let { path, thumbPath, ratio } = attachment;
  let imgHeight = (0.8 * contentWidth) / ratio;
  if (!thumbPath) return null;

  let avatarPath;
  if (user) {
    avatarPath = user.avatar.path;
  }

  return (
    <View style={{ marginTop: 10, flexDirection: 'row' }}>
      <View style={{ paddingRight: 12 }}>
        <View style={styles.avatar}>
          <Avatar size={32} path={avatarPath} />
        </View>
      </View>
      <TouchableOpacity
        style={{ flex: 1 }}
        onPress={() => probeUiHelper.showVideo({ path })}
      >
        <Image
          style={{
            flex: 1,
            height: imgHeight,
          }}
          source={{ uri: thumbPath }}
          resizeMode={'cover'}
        />
        <VideoThumbBody />
      </TouchableOpacity>
      <View style={{ flex: 0.2 }} />
    </View>
  );
}

function AltText(user, { text }) {
  let avatarPath;
  if (user) {
    avatarPath = user.avatar.path;
  }

  return (
    <View style={{ marginTop: 10, flexDirection: 'row' }}>
      <View style={{ paddingRight: 12 }}>
        <View style={styles.avatar}>
          <Avatar size={32} path={avatarPath} />
        </View>
      </View>
      <View
        style={{
          flex: 0.8,
          backgroundColor: colors.MSG_ALT_BG,
          padding: 8,
          borderRadius: 15,
        }}
      >
        <Text style={{ color: colors.MSG_ALT_COLOR }}>{text}</Text>
      </View>
      <View style={{ flex: 0.2 }} />
    </View>
  );
}

function MyImage(user, { attachment }, contentWidth) {
  let { path, ratio } = attachment;
  let imgHeight = (0.7 * contentWidth) / ratio;
  return (
    <View style={{ marginTop: 10, flexDirection: 'row' }}>
      <View style={{ flex: 0.3 }} />
      <Image
        style={{
          flex: 1,
          height: imgHeight,
        }}
        source={{ uri: path }}
        resizeMode={'cover'}
      />
    </View>
  );
}

function MyVideo(user, { attachment }, contentWidth) {
  let { path, thumbPath, ratio } = attachment;
  let imgHeight = (0.7 * contentWidth) / ratio;
  if (!thumbPath) return null;

  return (
    <View style={{ marginTop: 10, flexDirection: 'row' }}>
      <View style={{ flex: 0.3 }} />
      <TouchableOpacity
        style={{ flex: 1 }}
        onPress={() => probeUiHelper.showVideo({ path })}
      >
        <Image
          style={{
            flex: 1,
            height: imgHeight,
          }}
          source={{ uri: thumbPath }}
          resizeMode={'cover'}
        />
        <VideoThumbBody />
      </TouchableOpacity>
    </View>
  );
}

function MyText(user, { text }) {
  return (
    <View style={{ marginTop: 10, flexDirection: 'row' }}>
      <View style={{ flex: 0.3 }} />

      <View
        style={{
          flex: 0.7,
          backgroundColor: colors.MSG_MY_BG,
          padding: 8,
          borderRadius: 15,
        }}
      >
        <Text style={{ color: colors.MSG_MY_COLOR }}>{text}</Text>
      </View>
    </View>
  );
}

export function MsgAlt({ user, msg, contentWidth }) {
  if (msg.msgType === msgTypes.IMAGE) {
    return AltImage(user, msg, contentWidth);
  }
  if (msg.msgType === msgTypes.VIDEO) {
    return AltVideo(user, msg, contentWidth);
  }
  return AltText(user, msg);
}

export function Msg({ user, msg, contentWidth }) {
  if (msg.msgType === msgTypes.IMAGE) {
    return MyImage(user, msg, contentWidth);
  }
  if (msg.msgType === msgTypes.VIDEO) {
    return MyVideo(user, msg, contentWidth);
  }
  return MyText(user, msg);
}

function VideoThumbBody() {
  return (
    <View style={styles.videoThumbBody}>
      <FontAwesome name="play" size={36} color={'white'} />
      <Text style={{ color: 'white', marginTop: 8 }}>Смотреть видео</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  avatar: {
    //borderWidth: 1,
    // borderColor: 'black'
  },
  videoThumbBody: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
  },
});
