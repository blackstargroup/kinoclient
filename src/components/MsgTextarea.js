import React from 'react';
import {
  View,
  TouchableOpacity,
  Platform,
  Text,
  TextInput,
  StyleSheet,
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { colors, sizes, shadow, rainbow, baseStyles } from '../utils/theme';

const IS_ANDROID = Platform.OS === 'android';
const IS_IOS = Platform.OS === 'ios';
const TEXTAREA_PADDING_BOTTOM = 10;

const initialState = {
  value: '',
};

class MsgTextarea extends React.Component {
  constructor(props) {
    super(props);
    this.state = initialState;
  }

  static defaultProps = {
    onSend: () => console.warn('onSend not implemented'),
    onAttach: () => console.warn('onAttach not implemented'),
    keyboardOffset: 0,
  };

  async trySend() {
    try {
      await this.props.onSend(this.state.value);
      this.setState(initialState);
    } catch (error) {}
  }

  async tryAttach() {
    await this.props.onAttach();
  }

  render() {
    let containerStyle = {
      flexDirection: 'row',
      padding: 10,
      paddingBottom: TEXTAREA_PADDING_BOTTOM,
      backgroundColor: colors.MSG_TEXTAREA_BG,
    };

    let { keyboardOffset } = this.props;
    if (IS_ANDROID && keyboardOffset) {
      containerStyle = {
        ...containerStyle,
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
      };
    }

    if (IS_IOS && keyboardOffset) {
      containerStyle = {
        ...containerStyle,
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        paddingBottom:
          keyboardOffset - sizes.TAB_NAV_HEIGHT + TEXTAREA_PADDING_BOTTOM,
      };
    }

    return (
      <View style={containerStyle}>
        <View style={styles.content}>
          <View style={styles.textboxWrapper}>
            <TextInput
              multiline={true}
              underlineColorAndroid="transparent"
              style={styles.textbox}
              value={this.state.value}
              placeholder="Текст сообщения..."
              onChangeText={value => this.setState({ value })}
            />
          </View>
          <TouchableOpacity
            style={styles.onSendBtn}
            onPress={async () => {
              await this.trySend();
            }}
          >
            <FontAwesome name={'send'} size={16} color={colors.MSG_ATL_COLOR} />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.onAttachBtn}
            onPress={async () => {
              await this.props.onAttach();
            }}
          >
            <MaterialCommunityIcons
              name={'paperclip'}
              size={20}
              color={colors.MSG_ATL_COLOR}
            />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default MsgTextarea;

const styles = StyleSheet.create({
  content: {
    flex: 1,
    flexDirection: 'row',
    //borderWidth: 1,
    // borderColor: 'red',
  },
  textboxWrapper: {
    //borderWidth: 1,
    //borderColor: 'green',
    flex: 1,
  },
  textbox: {
    backgroundColor: 'white',
    padding: 5,
    borderRadius: 4,
    textAlignVertical: 'top',
  },
  onSendBtn: {
    paddingLeft: 8,
    paddingRight: 4,
    paddingVertical: 4,
  },
  onAttachBtn: {
    paddingHorizontal: 4,
    paddingVertical: 4,
  },
});
