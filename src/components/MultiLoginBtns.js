import React from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import accountManager from '../utils/account-manager';

export default function MultiLoginButtons({ loginPhones }) {
  if (!loginPhones || !loginPhones.length) return null;

  return (
    <View
      style={{ paddingTop: 10, flexDirection: 'row', justifyContent: 'center' }}
    >
      {loginPhones.map(({ phone, title }) => (
        <TouchableOpacity
          key={phone}
          style={styles.btn}
          onPress={() => accountManager.loginByPhone(phone)}
        >
          <Text>{title}</Text>
        </TouchableOpacity>
      ))}
    </View>
  );
}

const styles = StyleSheet.create({
  btn: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#DDDDDD',
    margin: 8,
    width: 48,
    height: 48,
    borderRadius: 24,
  },
});
