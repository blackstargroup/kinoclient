import React from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { ProjThumb, Avatar } from './Thumbs';
import { colors, sizes, shadow } from '../utils/theme';

export function BackBtnNavHeader({ onBackPress, title, subTitle }) {
  return (
    <View style={styles.headerContainer}>
      <View style={{ flexDirection: 'row', height: 48 }}>
        {subTitle ? (
          <View style={styles.centralSide}>
            <Text
              style={{
                color: colors.NAV_FIRST_TEXT_COLOR,
                fontSize: 16,
                fontWeight: 'bold',
              }}
            >
              {title}
            </Text>
            <Text style={{ color: colors.NAV_FIRST_TEXT_COLOR }}>
              {subTitle}
            </Text>
          </View>
        ) : (
          <View style={styles.centralSide}>
            <Text
              style={{
                color: colors.NAV_FIRST_TEXT_COLOR,
                fontSize: 16,
                fontWeight: 'bold',
              }}
            >
              {title}
            </Text>
          </View>
        )}

        <TouchableOpacity style={styles.leftSide} onPress={onBackPress}>
          <FontAwesome
            name="angle-left"
            size={35}
            color={colors.NAV_ACTIVE_BTN}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

export function BtnsNavHeader({
  leftTitle,
  onLeftPress,
  rightTitle,
  onRightPress,
  title,
}) {
  return (
    <View style={styles.headerContainer}>
      <View style={{ flexDirection: 'row', height: 48 }}>
        <View style={styles.centralSide}>
          <Text
            style={{
              color: colors.NAV_FIRST_TEXT_COLOR,
              fontSize: 16,
              fontWeight: 'bold',
            }}
          >
            {title}
          </Text>
        </View>

        <TouchableOpacity
          style={[styles.leftSide, { width: 100 }]}
          onPress={onLeftPress}
        >
          <Text style={styles.btnText}>{leftTitle}</Text>
        </TouchableOpacity>

        <TouchableOpacity
          style={[styles.rightSide, { width: 100 }]}
          onPress={onRightPress}
        >
          <Text style={styles.btnText}>{rightTitle}</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export function NavHeader({ title }) {
  return (
    <View style={[styles.headerContainer, { height: 48 }]}>
      <View style={styles.centralSide}>
        <View style={{ marginLeft: 10 }}>
          <Text
            style={{
              color: colors.NAV_FIRST_TEXT_COLOR,
              fontSize: 16,
              fontWeight: 'bold',
            }}
          >
            {title}
          </Text>
        </View>
      </View>
    </View>
  );
}

export function ProbeAsActorNavHeader({
  onPressLeft,
  onPressRight,
  projThumbPath,
  projTitle,
}) {
  return (
    <View style={styles.headerContainer}>
      <View style={{ flexDirection: 'row', height: 48 }}>
        <TouchableOpacity style={styles.leftSide} onPress={onPressLeft}>
          <FontAwesome
            name="angle-left"
            size={35}
            color={colors.NAV_ACTIVE_BTN}
          />
        </TouchableOpacity>

        <View style={styles.centralSide}>
          <ProjThumb path={projThumbPath} size={30} />
          <View style={{ marginLeft: 10 }}>
            <Text style={{ color: colors.NAV_FIRST_TEXT_COLOR }}>
              {projTitle}
            </Text>
          </View>
        </View>

        <TouchableOpacity style={styles.rightSide} onPress={onPressRight}>
          <MaterialCommunityIcons
            name="flag"
            size={28}
            color={colors.NAV_ACTIVE_BTN}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

export function ProbeAsDirectorNavHeader({
  onPressLeft,
  onPressRight,
  userAvatarPath,
  userFirstName,
  userLastName,
}) {
  return (
    <View style={styles.headerContainer}>
      <View style={{ flexDirection: 'row', height: 48 }}>
        <TouchableOpacity style={styles.leftSide} onPress={onPressLeft}>
          <FontAwesome
            name="angle-left"
            size={35}
            color={colors.NAV_ACTIVE_BTN}
          />
        </TouchableOpacity>

        <View style={styles.centralSide}>
          <Avatar path={userAvatarPath} size={30} />
          <View style={{ marginLeft: 10 }}>
            <Text style={{ color: colors.NAV_FIRST_TEXT_COLOR }}>
              {userFirstName} {userLastName}
            </Text>
          </View>
        </View>

        <TouchableOpacity style={styles.rightSide} onPress={onPressRight}>
          <MaterialCommunityIcons
            name="flag"
            size={28}
            color={colors.NAV_ACTIVE_BTN}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: colors.NAV_BAR_COLOR,
    borderBottomWidth: sizes.BORDER_SEPARATOR_WIDTH,
    borderBottomColor: colors.NAV_BAR_BORDER,
    paddingBottom: 8,
    ...shadow,
  },
  leftSide: {
    position: 'absolute',
    left: 0,
    top: 0,
    bottom: 0,
    //backgroundColor: 'yellow',
    width: 30,
    paddingRight: 10,
    paddingLeft: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  centralSide: {
    position: 'absolute',
    left: 30,
    right: 30,
    top: 0,
    bottom: 0,
    //backgroundColor: 'pink',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  rightSide: {
    position: 'absolute',
    right: 0,
    top: 0,
    bottom: 0,
    width: 30,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',

    //backgroundColor: 'yellow',
  },
  btnText: {
    color: colors.NAV_FIRST_TEXT_COLOR,
    fontSize: 16,
  },
});
