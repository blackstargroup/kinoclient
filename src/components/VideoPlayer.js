import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import Video from 'react-native-video';
import store from '../store';
import { videoPlayerClose } from '../actions/videoPlayer';

export function VideoPlayer({ path }) {
  return (
    <TouchableOpacity style={styles.container} onPress={closeVideo}>
      <Video
        source={{
          uri: path,
        }} // Can be a URL or a local file.
        ref={ref => {
          this.player = ref;
        }} // Store reference
        onVideoError={() => closeVideo()}
        onError={() => closeVideo()}
        onEnd={() => closeVideo()}
        style={styles.video}
      />
    </TouchableOpacity>
  );
}

function closeVideo() {
  store.dispatch(videoPlayerClose());
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'black',
  },
  video: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
});
