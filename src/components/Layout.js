import React from 'react';

import {
  StatusBar,
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Keyboard,
  Dimensions,
} from 'react-native';

import { connect } from 'react-redux';

import navService from '../utils/nav-service';
import LoginScreen from '../screens/LoginScreen';
import AppNavigator from './AppNavigator';
import { colors } from '../utils/theme';
import { VideoPlayer } from './VideoPlayer';
import store from '../store';
import { commonSetKeyboardOffset } from '../actions/common';

function mapStateToProps(state) {
  let { me, videoPlayer, common } = state;
  return { me, videoPlayer, common };
}

function keyboardDidHide() {
  console.log('hide keyboard');
  let keyboardOffset = 0;
  store.dispatch(commonSetKeyboardOffset({ keyboardOffset }));
}

function keyboardDidShow({ endCoordinates }) {
  console.log('show keyboard');
  let win = Dimensions.get('window');
  let winHeight = win.height;
  let keyboardOffset = endCoordinates.height;
  store.dispatch(commonSetKeyboardOffset({ keyboardOffset }));
}

class Container extends React.Component {
  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      keyboardDidShow,
    );
    this.keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      keyboardDidHide,
    );
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  render() {
    let { me, videoPlayer, common } = this.props;

    if (!me._id) {
      return (
        <SafeAreaView style={styles.container}>
          <StatusBar barStyle="light-content" />
          <LoginScreen />
        </SafeAreaView>
      );
    }

    return (
      <SafeAreaView style={styles.container}>
        <StatusBar barStyle="light-content" />
        <AppNavigator
          ref={ref => navService.setTopLevelNavigator(ref)}
          onNavigationStateChange={navService.onNavigationStateChange}
        />
        {videoPlayer.visible && <VideoPlayer path={videoPlayer.path} />}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.SAFE_VIEW_COLOR,
  },
});

export default connect(mapStateToProps)(Container);
