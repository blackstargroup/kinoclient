import React from 'react';
import { Image, View, Text, ActivityIndicator } from 'react-native';

import { BtnDefault } from './Buttons';
import { NavHeader, BackBtnNavHeader } from './NavHeaders';
import { colors } from '../utils/theme';

export function SuccessView({
  title,
  subTitle,
  onBackPress,
  onOkPress,
  status,
}) {
  if (!title) {
    <Success status={status} onOkPress={onOkPress} />;
  }

  if (!onBackPress) {
    return (
      <View style={{ flex: 1 }}>
        <NavHeader title={title} subTitle={subTitle} />
        <Success status={status} onOkPress={onOkPress} />
      </View>
    );
  }

  return (
    <View style={{ flex: 1 }}>
      <BackBtnNavHeader
        title={title}
        subTitle={subTitle}
        onBackPress={onBackPress}
      />
      <Success status={status} onOkPress={onOkPress} />
    </View>
  );
}

function Success({ status, onOkPress }) {
  status = status || 'Успешно выполнено';
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>{status}</Text>
      <BtnDefault style={{ marginTop: 24 }} onPress={onOkPress} title="OK" />
    </View>
  );
}
