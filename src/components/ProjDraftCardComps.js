import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Button,
  FlatList,
  TouchableOpacity,
  Alert,
} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import navService from '../utils/nav-service';
import { colors } from '../utils/theme';
import { BtnTransparentRed, BtnDefault } from './Buttons';

import { projDraftStatuses } from '../utils/constants';
import projDraftUIHelper from '../utils/ui-helpers/projDraft-ui-helper';

export function WarnView({ title, text }) {
  return (
    <View
      style={{
        flexDirection: 'row',
        padding: 16,
        marginBottom: 16,
        borderWidth: 4,
        borderColor: colors.ORANGE,
      }}
    >
      <FontAwesome name="warning" size={32} color={colors.ORANGE} />
      <View style={{ flex: 1, paddingLeft: 16 }}>
        <Text style={{ fontWeight: 'bold' }}>{title}</Text>
        <Text>{text}</Text>
      </View>
    </View>
  );
}

export function InfoView({ text }) {
  return (
    <View
      style={{
        flexDirection: 'row',
        marginBottom: 16,
        //borderWidth: 4,
        //borderColor: colors.CONTENT_ICON_COLOR,
      }}
    >
      <View style={{ flex: 1 }}>
        <Text style={{ color: colors.GREY_TEXT_COLOR }}>{text}</Text>
      </View>
    </View>
  );
}

export function ProjDraftCardBtns({ me, projDraft, proj }) {
  const { deleted, status, moderComment, dirComment } = projDraft;
  const projDraftId = projDraft._id;

  let isNew = false;
  let isAffected = false;
  let isApproved = false;
  let isDenied = false;
  let isModerating = false;
  let isPublished = false;
  const isClosed = proj.closed;

  if (!deleted && !isClosed) {
    switch (status) {
      case projDraftStatuses.NEW:
        isNew = true;
        break;
      case projDraftStatuses.AFFECTED:
        isAffected = true;
        break;
      case projDraftStatuses.APPROVED:
        isApproved = true;
        break;
      case projDraftStatuses.DENIED:
        isDenied = true;
        break;
      case projDraftStatuses.MODERATING:
        isModerating = true;
        break;
      case projDraftStatuses.PUBLISHED:
        isPublished = true;
        break;
      default:
        break;
    }
  }

  let btnEdit =
    !isClosed &&
    (isNew ||
      isAffected ||
      isApproved ||
      isDenied ||
      isPublished ||
      (isModerating && me.isModer));
  let btnModerate = !isClosed && isAffected; // || ((isDenied || isApproved) && me.isModer));
  let btnToAffected = !isClosed && isModerating && !me.isModer;
  let btnApprove = !isClosed && isModerating && me.isModer;
  let btnDeny = !isClosed && isModerating && me.isModer;
  let btnPublish = !isClosed && isApproved;
  let btnDelete = !proj._id && !deleted;
  let btnClose = !isClosed && proj._id;

  return (
    <View style={{ flex: 1 }}>
      {isNew && (
        <InfoView
          text={
            'Вначале нужно заполнить черновик, добавить описание кастинга и роли. ' +
            'Затем отправить черновик на модерацию. После модерации вы сможете опубликовать кастинг ' +
            'в любое время. Далее вы увидите первые пробы от посетителей'
          }
        />
      )}

      {isAffected && (
        <InfoView
          text={
            'Если вы считаете, что заполнили все необходимые поля и внесли все поправки, ' +
            'отправляйте проект на модерацию'
          }
        />
      )}

      {isPublished && (
        <InfoView
          text={
            'Ваш кастинг видят сотни пользователей, скоро вы получите первые пробы. ' +
            'Если вы желаете внести изменения, то Вам необходимо снова отредактировать проект. ' +
            'Пользователи увидят изменения только после модерации и повтороной публикации.'
          }
        />
      )}

      {isClosed && (
        <InfoView
          text={
            'Вы завершили кастинг. Его невозможно возобновить, однако вы можете создать новый'
          }
        />
      )}

      {isApproved && (
        <WarnView
          title={'Модератор одобрил черновик'}
          text={`Вы можете опубликовать кастинг в любое время, после чего его увидят пользователи.\n ${moderComment}`}
        />
      )}

      {isDenied && (
        <WarnView title={'Модератор отклонил черновик'} text={moderComment} />
      )}

      {isModerating && me.isModer && dirComment && (
        <WarnView title={'Комментарий'} text={dirComment} />
      )}

      {isModerating && !me.isModer && (
        <InfoView text={'Ожидайте ответа от модератора'} />
      )}

      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        {btnEdit && (
          <BtnDefault
            style={styles.btn}
            title={'Редактировать черновик'}
            onPress={() =>
              navService.navigate('ProjDraftEdit', { projDraftId })
            }
          />
        )}
        {btnModerate && (
          <BtnDefault
            style={styles.btn}
            title={'Отправить на модерацию'}
            onPress={() =>
              projDraftUIHelper.tryPromoteToModerating({ projDraft })
            }
          />
        )}
        {btnToAffected && (
          <BtnDefault
            style={styles.btn}
            title={'Отменить модерацию'}
            onPress={() =>
              projDraftUIHelper.tryPromoteToAffected({ projDraft })
            }
          />
        )}
        {btnPublish && (
          <BtnDefault
            style={styles.btn}
            title={'Опубликовать'}
            onPress={() =>
              projDraftUIHelper.tryPromoteToPublished({ projDraft })
            }
          />
        )}
        {btnApprove && (
          <BtnDefault
            style={[styles.btn, { backgroundColor: colors.GREEN }]}
            title={'Разрешить публикацию'}
            onPress={() =>
              projDraftUIHelper.tryPromoteToApproved({ projDraft })
            }
          />
        )}
        {btnDeny && (
          <BtnDefault
            style={[styles.btn, { backgroundColor: colors.RED }]}
            title={'Отклонить публикацию'}
            onPress={() => projDraftUIHelper.tryPromoteToDenied({ projDraft })}
          />
        )}
        {btnClose && (
          <BtnTransparentRed
            style={styles.btn}
            title={'Завершить кастинг'}
            onPress={() => {
              projDraftUIHelper.tryClose({ proj });
            }}
          />
        )}
        {btnDelete && (
          <BtnTransparentRed
            title={'Удалить черновик'}
            onPress={() => projDraftUIHelper.tryDelete({ projDraft })}
          />
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  btn: {
    marginBottom: 8,
  },
});
