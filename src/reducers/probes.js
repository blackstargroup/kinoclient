import { PROBES_LOADING, PROBES_SET, PROBES_ERROR } from '../actions/probes';

const initialState = {
  loading: false,
  error: false,
  items: [],
};

export default function(state = initialState, action) {
  const payload = action.payload || {};
  switch (action.type) {
    case PROBES_LOADING:
      return {
        ...initialState,
        loading: true,
      };
    case PROBES_ERROR:
      return {
        ...initialState,
        error: true,
      };
    case PROBES_SET:
      return {
        ...initialState,
        items: payload.probes || [],
      };
    default:
      return state;
  }
}
