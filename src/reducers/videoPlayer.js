import { VIDEO_PLAYER_SET, VIDEO_PAYER_CLOSE } from '../actions/videoPlayer';

const initialState = {
  visible: false,
  path: null,
};

export default function(state = initialState, action) {
  const { payload } = action;
  switch (action.type) {
    case VIDEO_PLAYER_SET:
      return {
        ...initialState,
        visible: true,
        path: payload.path,
      };
    case VIDEO_PAYER_CLOSE:
      return {
        ...initialState,
      };
    default:
      return state;
  }
}
