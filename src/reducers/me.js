import { ME_SET } from '../actions/me';

let initialState = {
  _id: null,
  loginPhone: null,
  firstName: null,
  secondName: null,
  avatar: {},
};

export default function(state = initialState, action) {
  switch (action.type) {
    case ME_SET:
      if (!action.payload._id) {
        return initialState;
      }

      return {
        ...state,
        ...action.payload,
      };

    default:
      return state;
  }
}
