import { CACHED_USERS_ADD } from '../actions/cachedUsers';

const initialState = {};

export default function(state = initialState, action) {
  const payload = action.payload || {};
  switch (action.type) {
    case CACHED_USERS_ADD:
      const { _id, avatar, firstName, lastName } = payload.user;
      const newState = { ...state };
      newState[_id] = { avatar, firstName, lastName };
      return newState;
    default:
      return state;
  }
}
