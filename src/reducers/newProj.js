import {
  NEW_PROJ_ACCEPT_RULES,
  NEW_PROJ_ACCEPT_AGREEMENT,
} from '../actions/newProj';

const initialState = {
  rulesOK: false,
  agreementOK: false,
  success: false,
  error: false,
};

export default function(state = initialState, action) {
  const payload = action.payload || {};
  switch (action.type) {
    case NEW_PROJ_ACCEPT_RULES:
      return {
        ...state,
        rulesOK: payload.accept,
      };
    case NEW_PROJ_ACCEPT_AGREEMENT:
      return {
        ...state,
        agreementOK: payload.accept,
      };
    default:
      return state;
  }
}
