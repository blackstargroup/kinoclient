import { PROJS_SET } from '../actions/projs';

const initialState = {
  items: [],
};

export default function(state = initialState, action) {
  const payload = action.payload || {};
  switch (action.type) {
    case PROJS_SET:
      return {
        ...initialState,
        items: payload.projs || [],
      };
    default:
      return state;
  }
}
