import { CHAT_LOADING, CHAT_ERROR, CHAT_SET } from '../actions/chat';

const initialState = {
  loading: false,
  error: false,
  probeId: null,
  probe: {},
  msgs: [],
  text: '',
  progress: 0,
  loadingText: null,
};

export default function(state = initialState, action) {
  const { payload } = action;
  switch (action.type) {
    case CHAT_LOADING:
      return {
        ...initialState,
        loading: true,
        probe: payload.probe,
        progress: payload.progress,
        loadingText: payload.loadingText,
      };
    case CHAT_ERROR:
      return {
        ...initialState,
        error: true,
        probeId: payload.probe && payload.probe._id,
        probe: payload.probe || {},
      };
    case CHAT_SET:
      return {
        ...initialState,
        probeId: payload.probe._id,
        probe: payload.probe,
        msgs: payload.msgs || [],
      };
    default:
      return state;
  }
}
