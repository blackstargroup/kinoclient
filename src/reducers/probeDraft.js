import {
  PROBE_DRAFT_LOADING,
  PROBE_DRAFT_ERROR,
  PROBE_DRAFT_SET,
  PROBE_DRAFT_SUCCESS,
} from '../actions/probeDraft';

const initialState = {
  loading: false,
  success: false,
  error: false,
  projId: null,
  roleId: null,
  role: {},
  proj: {},
  progress: 0,
  status: null,
};

export default function(state = initialState, action) {
  let { payload } = action;
  switch (action.type) {
    case PROBE_DRAFT_LOADING:
      return {
        ...state,
        loading: true,
        error: false,
        success: false,
        progress: payload.progress,
        status: payload.status,
      };
    case PROBE_DRAFT_ERROR:
      return {
        ...state,
        loading: false,
        error: true,
        success: false,
        projId: payload.projId || state.projId,
        roleId: payload.roleId || state.roleId,
      };
    case PROBE_DRAFT_SUCCESS:
      return {
        ...state,
        loading: false,
        error: false,
        success: true,
      };
    case PROBE_DRAFT_SET: {
      let proj = payload.proj || {};
      let role = payload.role || {};
      return {
        ...initialState,
        projId: proj._id,
        roleId: role._id,
        role,
        proj,
      };
    }
    default:
      return state;
  }
}
