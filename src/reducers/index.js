import { combineReducers } from 'redux';

import me from './me';
import probeDraft from './probeDraft';
import probes from './probes';
import projs from './projs';
import projCard from './projCard';
import projDraftCard from './projDraftCard';
import newProj from './newProj';
import chat from './chat';
import common from './common';
import cachedUsers from './cachedUsers';
import avatarUploadStatus from './avatarUploadStatus';
import videoPlayer from './videoPlayer';

export default combineReducers({
  me,
  probeDraft,
  probes,
  projs,
  projCard,
  projDraftCard,
  newProj,
  chat,
  common,
  cachedUsers,
  avatarUploadStatus,
  videoPlayer,
});
