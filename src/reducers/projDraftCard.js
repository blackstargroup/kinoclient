import {
  PROJ_DRAFT_CARD_SET,
  PROJ_DRAFT_CARD_ERROR,
  PROJ_DRAFT_CARD_LOADING,
} from '../actions/projDraftCard';

const initialState = {
  projDraftId: null,
  projDraft: {},
  roleDraftId: {},
  roleDraft: {},
  proj: {},
  error: false,
  errorText: null,
  loading: false,
  loadingText: null,
};

export default function(state = initialState, action) {
  const payload = action.payload || {};
  switch (action.type) {
    case PROJ_DRAFT_CARD_SET:
      return {
        ...initialState,
        projDraftId: payload.projDraft ? payload.projDraft._id : null,
        projDraft: payload.projDraft || {},
        roleDraftId: payload.roleDraft ? payload.roleDraft._id : null,
        roleDraft: payload.roleDraft || {},
        proj: payload.proj || {},
      };
    case PROJ_DRAFT_CARD_ERROR:
      return {
        ...initialState,
        projDraftId: payload.projDraftId,
        roleDraftId: payload.roleDraftId,
        error: true,
        errorText: payload.errorText,
      };
    case PROJ_DRAFT_CARD_LOADING:
      return {
        ...initialState,
        loading: true,
        loadingText: payload.loadingText,
      };
    default:
      return state;
  }
}
