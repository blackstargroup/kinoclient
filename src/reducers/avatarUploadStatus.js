import {
  AVATAR_UPLOADING_PROGRESS,
  AVATAR_UPLOADING_FINISH,
  AVATAR_UPLOADING_ERROR,
  AVATAR_UPLOADING_START,
} from '../actions/avatarUploadStatus';

let initialState = {
  visible: false,
  progress: 0,
  status: null,
  error: false,
  errorText: null,
  errorSubText: null,
};

export default function(state = initialState, action) {
  let payload = action.payload;
  switch (action.type) {
    case AVATAR_UPLOADING_START:
      return {
        ...initialState,
        status: payload.status,
        visible: true,
      };

    case AVATAR_UPLOADING_PROGRESS:
      return {
        ...state,
        progress: payload.progress,
        status: payload.status,
      };

    case AVATAR_UPLOADING_ERROR:
      return {
        ...initialState,
        error: true,
        errorText: payload.errorText,
        errorSubText: payload.errorSubText,
      };

    case AVATAR_UPLOADING_FINISH:
      return {
        ...initialState,
      };

    default:
      return state;
  }
}
