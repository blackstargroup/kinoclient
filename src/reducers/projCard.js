import {
  PROJ_CARD_SET,
  PROJ_CARD_LOADING,
  PROJ_CARD_ERROR,
} from '../actions/projCard';

const initialState = {
  projId: null,
  roleId: null,
  loading: false,
  error: false,
  proj: {},
  role: {},
};

export default function(state = initialState, action) {
  const payload = action.payload || {};
  switch (action.type) {
    case PROJ_CARD_SET:
      return {
        ...initialState,
        projId: payload.proj ? payload.proj._id : null,
        roleId: payload.role ? payload.role._id : null,
        proj: payload.proj || {},
        role: payload.role || {},
      };
    case PROJ_CARD_LOADING:
      return {
        ...initialState,
        loading: true,
      };
    case PROJ_CARD_ERROR:
      return {
        ...initialState,
        projId: payload.projId,
        roleId: payload.roleId,
        error: true,
      };

    default:
      return state;
  }
}
