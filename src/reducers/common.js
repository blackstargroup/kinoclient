import {
  COMMON_SET_MULTI_LOGIN,
  COMMON_SET_KEYBOARD_OFFSET,
} from '../actions/common';
import { COMMON_SET_LOGIN_PHONES } from '../actions/common';

const initialState = {
  multiLoginToken: null,
  loginPhones: [],
  keyboardOffset: 0,
};

export default function(state = initialState, action) {
  const payload = action.payload;
  switch (action.type) {
    case COMMON_SET_MULTI_LOGIN:
      return {
        ...state,
        multiLoginToken: payload.multiLoginToken,
      };
    case COMMON_SET_LOGIN_PHONES:
      return {
        ...state,
        loginPhones: payload.loginPhones || [],
      };
    case COMMON_SET_KEYBOARD_OFFSET:
      return {
        ...state,
        keyboardOffset: payload.keyboardOffset,
      };
    default:
      return state;
  }
}
