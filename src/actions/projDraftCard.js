export const PROJ_DRAFT_CARD_SET = 'PROJ_DRAFT_CARD_SET';
export const PROJ_DRAFT_CARD_ERROR = 'PROJ_DRAFT_CARD_ERROR';
export const PROJ_DRAFT_CARD_LOADING = 'PROJ_DRAFT_CARD_LOADING';

export function projDraftCardSet({ projDraft, roleDraft, proj }) {
  return {
    type: PROJ_DRAFT_CARD_SET,
    payload: { projDraft, roleDraft, proj },
  };
}

export function projDraftCardError({ projDraftId, roleDraftId, errorText }) {
  return {
    type: PROJ_DRAFT_CARD_ERROR,
    payload: { projDraftId, roleDraftId, errorText },
  };
}

export function projDraftCardLoading({ loadingText }) {
  return {
    type: PROJ_DRAFT_CARD_LOADING,
    payload: { loadingText },
  };
}
