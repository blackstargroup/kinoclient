export const PROBE_DRAFT_SET = 'PROBE_DRAFT_SET';
export const PROBE_DRAFT_LOADING = 'PROBE_DRAFT_LOADING';
export const PROBE_DRAFT_ERROR = 'PROBE_DRAFT_ERROR';
export const PROBE_DRAFT_SUCCESS = 'PROBE_DRAFT_SUCCESS';

export function probeDraftSet({ proj, role, attachments }) {
  return {
    type: PROBE_DRAFT_SET,
    payload: { proj, role, attachments },
  };
}

export function probeDraftSuccess() {
  return {
    type: PROBE_DRAFT_SUCCESS,
    payload: {},
  };
}

export function probeDraftLoading({ progress, status }) {
  return {
    type: PROBE_DRAFT_LOADING,
    payload: { progress, status },
  };
}

export function probeDraftError({ projId, roleId }) {
  return {
    type: PROBE_DRAFT_ERROR,
    payload: { projId, roleId },
  };
}
