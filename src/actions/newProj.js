export const NEW_PROJ_ACCEPT_RULES = 'NEW_PROJ_ACCEPT_RULES';
export const NEW_PROJ_ACCEPT_AGREEMENT = 'NEW_PROJ_ACCEPT_AGREEMENT';

export function newProjAcceptRules({ accept }) {
  return {
    type: NEW_PROJ_ACCEPT_RULES,
    payload: { accept },
  };
}

export function newProjAcceptAgreement({ accept }) {
  return {
    type: NEW_PROJ_ACCEPT_AGREEMENT,
    payload: { accept },
  };
}
