export const PROJ_CARD_SET = 'PROJ_CARD_SET';
export const PROJ_CARD_LOADING = 'PROJ_CARD_LOADING';
export const PROJ_CARD_ERROR = 'PROJ_CARD_ERROR';

export function projCardSet({ proj, role }) {
  return {
    type: PROJ_CARD_SET,
    payload: { proj, role },
  };
}

export function projCardLoading() {
  return {
    type: PROJ_CARD_LOADING,
    payload: {},
  };
}

export function projCardError({ projId, roleId }) {
  return {
    type: PROJ_CARD_ERROR,
    payload: { projId, roleId },
  };
}
