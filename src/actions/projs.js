export const PROJS_SET = 'PROJS_SET';

export function projsSet({ projs }) {
  return {
    type: PROJS_SET,
    payload: { projs },
  };
}
