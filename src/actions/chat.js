export const CHAT_SET = 'CHAT_SET';
export const CHAT_LOADING = 'CHAT_LOADING';
export const CHAT_ERROR = 'CHAT_ERROR';

export function chatSet({ probe, msgs }) {
  return {
    type: CHAT_SET,
    payload: { probe, msgs },
  };
}

export function chatLoading({ probe, progress, loadingText }) {
  return {
    type: CHAT_LOADING,
    payload: { probe, progress, loadingText },
  };
}

export function chatError({ probe, error }) {
  return {
    type: CHAT_ERROR,
    payload: { probe, error },
  };
}
