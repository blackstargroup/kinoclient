export const ME_SET = 'ME_SET';

export function setMe(me) {
  return {
    type: ME_SET,
    payload: me || {},
  };
}
