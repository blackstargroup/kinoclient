export const PROBES_SET = 'PROBES_SET';
export const PROBES_LOADING = 'PROBES_LOADING';
export const PROBES_ERROR = 'PROBES_ERROR';

export function probesSet({ probes }) {
  return {
    type: PROBES_SET,
    payload: { probes },
  };
}

export function probesLoading() {
  return {
    type: PROBES_LOADING,
    payload: {},
  };
}

export function probesError(error) {
  return {
    type: PROBES_ERROR,
    payload: { error },
  };
}
