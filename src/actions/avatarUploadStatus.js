export const AVATAR_UPLOADING_START = 'AVATAR_UPLOADING_START';
export const AVATAR_UPLOADING_PROGRESS = 'AVATAR_UPLOADING_PROGRESS';
export const AVATAR_UPLOADING_FINISH = 'AVATAR_UPLOADING_FINISH';
export const AVATAR_UPLOADING_ERROR = 'AVATAR_UPLOADING_ERROR';

export function uploadingStart({ status }) {
  return {
    type: AVATAR_UPLOADING_START,
    payload: { status },
  };
}

export function setUploadingProgress({ progress, status }) {
  return {
    type: AVATAR_UPLOADING_PROGRESS,
    payload: { progress, status },
  };
}

export function setUploadingError({ errorText, errorSubText }) {
  return {
    type: AVATAR_UPLOADING_ERROR,
    payload: { errorText, errorSubText },
  };
}

export function uploadingFinish() {
  return {
    type: AVATAR_UPLOADING_FINISH,
    payload: {},
  };
}
