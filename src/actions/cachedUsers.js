export const CACHED_USERS_ADD = 'CACHED_USERS_ADD';

export function cachedUsersAdd(user) {
  return {
    type: CACHED_USERS_ADD,
    payload: { user },
  };
}
