export const COMMON_SET_MULTI_LOGIN = 'COMMON_SET_MULTI_LOGIN';
export const COMMON_SET_LOGIN_PHONES = 'COMMON_SET_LOGIN_PHONES';
export const COMMON_SET_KEYBOARD_OFFSET = 'COMMON_SET_KEYBOARD_OFFSET';

export function commonSetMultiLoginToken({ multiLoginToken }) {
  return {
    type: COMMON_SET_MULTI_LOGIN,
    payload: { multiLoginToken },
  };
}

export function commonSetLoginPhones({ loginPhones }) {
  return {
    type: COMMON_SET_LOGIN_PHONES,
    payload: { loginPhones },
  };
}

export function commonSetKeyboardOffset({ keyboardOffset }) {
  return {
    type: COMMON_SET_KEYBOARD_OFFSET,
    payload: { keyboardOffset },
  };
}
