export const VIDEO_PLAYER_SET = 'VIDEO_PLAYER_SET';
export const VIDEO_PAYER_CLOSE = 'VIDEO_PLAYER_CLOSE';

export function videoPlayerSet({ path }) {
  return {
    type: VIDEO_PLAYER_SET,
    payload: { path },
  };
}

export function videoPlayerClose() {
  return {
    type: VIDEO_PAYER_CLOSE,
    payload: {},
  };
}
