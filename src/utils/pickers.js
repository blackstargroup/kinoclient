import ImagePicker from 'react-native-image-crop-picker';
import RNThumbnail from 'react-native-thumbnail';

import { requestPhotoPermission } from './permissions';

const COMPRESS_AVATAR_QUALITY = 0.6;
const AVATAR_SIZE = 300;

const COMPRESS_PROBE_IMAGE_QUALITY = 0.6;
const COMPRESS_PROBE_IMAGE_MAX_WIDTH = 800;
const COMPRESS_PROBE_IMAGE_MAX_HEIGHT = 800;

const IMAGE_PICKER_LOADING_TEXT = 'Сохранение файла...';

export async function pickAvatar() {
  try {
    await requestPhotoPermission();

    res = await ImagePicker.openPicker({
      mediaType: 'photo',
      cropping: true,
      width: AVATAR_SIZE,
      height: AVATAR_SIZE,
      cropperCircleOverlay: true,
      compressImageQuality: COMPRESS_AVATAR_QUALITY,
      // ios only
      loadingLabelText: IMAGE_PICKER_LOADING_TEXT,
    });
    return res;
  } catch (error) {
    console.log(error);
    return null;
  }
}

export async function pickProbeAttachment() {
  let file;
  let thumbFile = null;
  try {
    await requestPhotoPermission();
    file = await ImagePicker.openPicker({
      compressImageQuality: COMPRESS_PROBE_IMAGE_QUALITY,
      compressImageMaxWidth: COMPRESS_PROBE_IMAGE_MAX_WIDTH,
      compressImageMaxHeight: COMPRESS_PROBE_IMAGE_MAX_HEIGHT,
      // ios only
      loadingLabelText: IMAGE_PICKER_LOADING_TEXT,
    });

    if (file.mime.startsWith('video')) {
      let thumbRes = await RNThumbnail.get(file.path);
      thumbFile = {
        path: thumbRes.path,
        width: thumbRes.width,
        height: thumbRes.height,
        mime: 'image/jpeg',
        size: 200 * 1024,
      };
    }
  } catch (error) {
    console.log(error);
    return { file: null, thumbFile: null };
  }
  return { file, thumbFile };
}
