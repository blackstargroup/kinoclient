import { AppState, Platform } from 'react-native';
import FCM, {
  FCMEvent,
  RemoteNotificationResult,
  WillPresentNotificationResult,
  NotificationType,
  NotificationActionType,
  NotificationActionOption,
  NotificationCategoryOption,
} from 'react-native-fcm';

import store from '../store';
import navService from './nav-service';
import onFetchFCMToken from '../app/on-fetch-fcm-token';
import { alertOk } from './helpers/alerts';

function showLocalNotification({ text }) {
  FCM.presentLocalNotification({
    channel: 'default',
    //id: new Date().valueOf().toString(), // (optional for instant notification)
    //title: text, // as FCM payload
    body: text, // as FCM payload (required)
    //sound: "bell.mp3", // "default" or filename
    priority: 'high', // as FCM payload

    wake_screen: true, // Android only, wake up screen when notification arrives
    lights: true, // Android only, LED blinking (default false)
    show_in_foreground: true,
  });

  return;
  /*
  FCM.presentLocalNotification({
    channel: 'default',
    //id: new Date().valueOf().toString(), // (optional for instant notification)
    //title: text, // as FCM payload
    body: text, // as FCM payload (required)
    sound: "bell.mp3", // "default" or filename
    priority: "high", // as FCM payload
    click_action: "com.myapp.MyCategory", // as FCM payload - this is used as category identifier on iOS.
    badge: 10, // as FCM payload IOS only, set 0 to clear badges
    number: 10, // Android only
    auto_cancel: true, // Android only (default true)
    icon: "ic_launcher", // as FCM payload, you can relace this with custom icon you put in mipmap
    color: "red", // Android only
    vibrate: 300, // Android only default: 300, no vibration if you pass 0
    wake_screen: true, // Android only, wake up screen when notification arrives
    //group: "group", // Android only
    //ongoing: true, // Android only
    my_custom_data: "my_custom_field_value", // extra data you want to throw
    lights: true, // Android only, LED blinking (default false)
    show_in_foreground: true, // notification when app is in foreground (local & remote)
  });
  */
}

function scheduleLocalNotification() {
  FCM.scheduleLocalNotification({
    id: 'testnotif',
    fire_date: new Date().getTime() + 5000,
    vibrate: 500,
    title: 'Hello',
    body: 'Test Scheduled Notification',
    sub_text: 'sub text',
    priority: 'high',
    large_icon:
      'https://image.freepik.com/free-icon/small-boy-cartoon_318-38077.jpg',
    show_in_foreground: true,
    picture:
      'https://firebase.google.com/_static/af7ae4b3fc/images/firebase/lockup.png',
    wake_screen: true,
    extra1: { a: 1 },
    extra2: 1,
  });
}

function notificationHandler(notif) {
  /*
    this sections hides notification when user in foreground state
  if (
    Platform.OS === 'ios' &&
    notif._notificationType === NotificationType.WillPresent &&
    !notif.local_notification
  ) {
    notif.finish(WillPresentNotificationResult.All);
    return;
  }
  */
  //scheduleLocalNotification();

  let { fcm } = notif;
  let body = fcm.body || fcm.title;
  console.warn(fcm);
  showLocalNotification({ text: body });

  console.warn(notif);
  alertOk('Notif handler!!!! @$');

  if (notif.opened_from_tray) {
    notificationTapped(notif);
  }

  if (Platform.OS === 'ios') {
    //optional
    //iOS requires developers to call completionHandler to end notification process. If you do not call it your background remote notifications could be throttled, to read more about it see the above documentation link.
    //This library handles it for you automatically with default behavior (for remote notification, finish with NoData; for WillPresent, finish depend on "show_in_foreground"). However if you want to return different result, follow the following code to override
    //notif._notificationType is available for iOS platfrom
    switch (notif._notificationType) {
      case NotificationType.Remote:
        notif.finish(RemoteNotificationResult.NewData); //other types available: RemoteNotificationResult.NewData, RemoteNotificationResult.ResultFailed
        break;
      case NotificationType.NotificationResponse:
        notif.finish();
        break;
      case NotificationType.WillPresent:
        notif.finish(WillPresentNotificationResult.All); //other types available: WillPresentNotificationResult.None
        // this type of notificaiton will be called only when you are in foreground.
        // if it is a remote notification, don't do any app logic here. Another notification callback will be triggered with type NotificationType.Remote
        break;
    }
  }
}

function notificationTapped(notif) {
  console.warn('User tappterd on notif');
  console.log(`!!User tapped notification\n${JSON.stringify(notif)}`);
  const { me } = store.getState();
  if (me._id) {
    handleNotificationAction(notif);
    handleNotificationTarget(notif);
  }
}

function handleNotificationAction(notif) {
  let { _actionIdentifier } = notif;
  if (_actionIdentifier === 'reply') {
    alertOk('!!User replied ' + JSON.stringify(notif._userText));
  }
  if (_actionIdentifier === 'view') {
    alertOk('!!User clicked View in App');
  }
  if (_actionIdentifier === 'dismiss') {
    alertOk('!!User clicked Dismiss');
  }
}

function handleNotificationTarget(notif) {
  let { targetScreen } = notif;
  if (!targetScreen) return;

  setTimeout(() => {
    alertOk('handle notif', targetScreen);
    if (targetScreen === 'Probe') {
      navService.navigate('Chats');
    }
  }, 500);
}

export async function refreshFCMToken() {
  let token = await FCM.getFCMToken();
  await onFetchFCMToken(token);
}

export async function initFCM() {
  FCM.on(FCMEvent.RefreshToken, onFetchFCMToken);

  FCM.createNotificationChannel({
    id: 'default',
    name: 'Default',
    description: 'used for example',
    priority: 'high',
  });

  /*
  FCM.enableDirectChannel() is only needed if you want to have back and forth communication with firebase though socket.
  if you only need receive notifications, you don't need it

  FCM.enableDirectChannel();

  FCM.on(FCMEvent.DirectChannelConnectionChanged, data => {
    console.log('!!direct channel connected' + data);
  });

  setTimeout(() => {
    FCM.isDirectChannelEstablished().then(d => console.log(d));
  }, 1000);
  */

  FCM.setNotificationCategories([
    {
      id: 'com.myidentifi.fcm.text',
      actions: [
        {
          type: NotificationActionType.TextInput,
          id: 'reply',
          title: 'Быстрый ответ',
          textInputButtonTitle: 'Отправить',
          textInputPlaceholder: 'Текст сообщения',
          intentIdentifiers: [],
          options: NotificationActionOption.AuthenticationRequired,
        },
        {
          type: NotificationActionType.Default,
          id: 'view',
          title: 'Открыть в приложении',
          intentIdentifiers: [],
          options: NotificationActionOption.Foreground,
        },
        {
          type: NotificationActionType.Default,
          id: 'dismiss',
          title: 'Отклонить',
          intentIdentifiers: [],
          options: NotificationActionOption.Destructive,
        },
      ],
      options: [
        NotificationCategoryOption.CustomDismissAction,
        NotificationCategoryOption.PreviewsShowTitle,
      ],
    },
  ]);

  FCM.on(FCMEvent.Notification, notificationHandler);

  FCM.getInitialNotification().then(notif => {
    notif && notificationTapped(notif);
  });
}
