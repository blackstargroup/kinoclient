import db from '../db-service';

export default {
  sendCustomReport,
  sendErrorReportSafety,
  getReports,
};

async function sendCustomReport({ kind, title, desc }) {
  await db.addReport({ kind, title, desc });
}

async function sendErrorReportSafety({ title, error }) {
  try {
    let desc;
    if (!error) {
      desc = 'unknown error';
    } else if (error.message) {
      desc = error.message;
    } else {
      console.log('ERROR', error);
      desc = error.toString();
    }
    await db.addReport({ kind: 'error', title, desc });
  } catch (sendingError) {
    console.log(`cant send report ${title}`);
  }
}

async function getReports() {
  return db.getReports({ count: 20, kind: '' });
}
