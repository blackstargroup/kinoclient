import gql from 'graphql-tag';
import { client } from '../../store';

export default {
  getTokenByFacebookToken,
  getTokenByCode,
};

async function getTokenByFacebookToken(accountKitToken) {
  try {
    const { data } = await client.mutate({
      mutation: gql`
        mutation($accountKitToken: String!) {
          login(accountKitToken: $accountKitToken) {
            token
          }
        }
      `,
      variables: {
        accountKitToken,
      },
    });

    return data.login.token;
  } catch (error) {
    throw new Error("Can't login with facebook. Or user pressed back");
  }
}

async function getTokenByCode(code) {
  let res = await client.mutate({
    mutation: gql`
      mutation($code: String!) {
        loginByCode(code: $code) {
          token
        }
      }
    `,
    variables: { code },
  });
  return res.data.loginByCode.token;
}
