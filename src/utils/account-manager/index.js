import FacebookAccountHelper from '../helpers/facebook-account-helper';
import deviceStorage from '../device-storage';
import dataService from '../data-service';
import onLogin from '../../app/on-login';
import onLogout from '../../app/on-logout';
import db from './db';
import uploadAvatar from '../helpers/upload-avatar';

async function login(token) {
  await deviceStorage.setAuthToken(token);
  const { me } = await dataService.setup();
  await onLogin({ me, isAutoLogin: false });
  return me;
}

export default class AccountManager {
  static async loginUsingFacebookKit() {
    const accountKitToken = await FacebookAccountHelper.getToken();
    const token = await db.getTokenByFacebookToken(accountKitToken);
    return login(token);
  }

  static async loginByPhone(phone) {
    let multiLoginToken = await deviceStorage.getMultiLoginToken();
    let code = `${multiLoginToken}:${phone}`;
    return login(await db.getTokenByCode(code));
  }

  static async tryAutoLogin() {
    try {
      let token = await deviceStorage.getAuthToken();
      if (!token) return;
      let { me } = await dataService.setup();
      if (!me) return;
      await onLogin({ me, isAutoLogin: true });
    } catch (error) {
      console.warn('Cant auto login', error);
    }
  }

  static async logout() {
    try {
      await FacebookAccountHelper.logout();
    } catch (error) {
      // i don't handle all exceptions, i just want handle only facebook excetpion
      // because i want to show where it happens
      console.log('Cant logout from facebook');
      throw error;
    }
    await deviceStorage.setAuthToken('');
    await onLogout();
  }

  static async changeAvatar() {
    try {
      await uploadAvatar();
    } catch (error) {
      console.log(error);
    }
  }
}
