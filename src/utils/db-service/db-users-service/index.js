import { client } from '../../../store';
import GET_USER_QUERY from '../../../graphql/queries/getUser';
import GET_ME_QUERY from '../../../graphql/queries/getMe';
import UPDATE_ME_MUTATION from '../../../graphql/mutations/updateMe';
import SET_AVATAR_GET_ME from '../../../graphql/mutations/setAvatar';
import REGISTER_DIR_MUTATION from '../../../graphql/mutations/registerDir';
import SET_TESTER_MUTATION from '../../../graphql/mutations/setTester';
import { handleMe, handleUser } from './db-handle-users';

export default {
  getMe,
  updateMe,
  setAvatar,
  registerDir,
  setTester,
  getUser,
};

async function getUser(_id) {
  console.log(`get user from db ${_id}`);
  const { data } = await client.query({
    query: GET_USER_QUERY,
    variables: { _id },
  });
  return handleUser(data.getUser);
}

async function getMe() {
  console.log(`get me from db`);
  const { data } = await client.query({
    query: GET_ME_QUERY,
  });
  return handleMe(data.getMe);
}

async function updateMe(me) {
  console.log(`update me db request`);
  const result = await client.mutate({
    mutation: UPDATE_ME_MUTATION,
    variables: me,
  });
  return handleMe(result.data.updateMe);
}

async function setAvatar({ serverName, fileId }) {
  console.log(`set avatar get me db request servName=${serverName}`);
  const result = await client.mutate({
    mutation: SET_AVATAR_GET_ME,
    variables: { serverName, fileId },
  });
  return handleMe(result.data.setAvatar);
}

async function registerDir(dir) {
  console.log(`register dir db request`);
  const result = await client.mutate({
    mutation: REGISTER_DIR_MUTATION,
    variables: dir,
  });

  return handleMe(result.data.registerDir);
}

async function setTester({ loginPhone, value }) {
  let res = await client.mutate({
    mutation: SET_TESTER_MUTATION,
    variables: { loginPhone, value },
  });

  let success = res.data.setTester.success;
  if (!success) {
    throw new Error(`Set tester success=${success}`);
  }
}
