export function handleMe(me) {
  return {
    ...me,
    avatar: me.avatar || {},
  };
}

export function handleUser(user) {
  return {
    ...user,
    avatar: user.avatar || {},
  };
}
