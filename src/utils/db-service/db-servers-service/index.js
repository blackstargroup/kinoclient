import { client } from '../../../store';
import GET_UPLOAD_SERVER from '../../../graphql/queries/getUploadServer';

export default {
  getUploadServer,
};

async function getUploadServer() {
  console.log(`get upload avatar server`);
  const { data } = await client.query({
    query: GET_UPLOAD_SERVER,
  });
  return data.getUploadServer;
}
