import { projDraftStatuses } from '../../constants';
import { colors } from '../../theme';

export function handleProjDrafts(projDrafts) {
  return projDrafts.map(handleProjDraft);
}

export function handleProjDraft(projDraft) {
  let statusTitle;
  let statusColor;

  if (projDraft.deleted) {
    statusTitle = 'Удалён';
    statusColor = colors.RED;
  } else {
    switch (projDraft.status) {
      case projDraftStatuses.NEW:
        statusTitle = 'Новый';
        statusColor = colors.RED;
        break;
      case projDraftStatuses.AFFECTED:
        statusTitle = 'Есть изменения';
        statusColor = colors.RED;
        break;
      case projDraftStatuses.MODERATING:
        statusTitle = 'Ожидает модерации';
        statusColor = colors.RED;
        break;
      case projDraftStatuses.APPROVED:
        statusTitle = 'Утвержден';
        statusColor = colors.ORANGE;
        break;
      case projDraftStatuses.DENIED:
        statusTitle = 'Отказано в публикации';
        statusColor = colors.RED;
        break;
      case projDraftStatuses.PUBLISHED:
        statusTitle = 'Опубликован';
        statusColor = colors.GREEN;
        break;
      default:
        statusTitle = '';
        statusColor = colors.RED;
        break;
    }
  }

  const roles = projDraft.roles ? handleRoleDrafts(projDraft.roles) : [];

  return {
    ...projDraft,
    thumb: projDraft.thumb || {},
    roles,
    statusTitle,
    statusColor,
    createdAtTime: new Date(projDraft.createdAt).getTime(),
    updatedAtTime: new Date(projDraft.updatedAt).getTime(),
  };
}

export function handleRoleDrafts(roleDrafts) {
  return roleDrafts.map(handleRoleDraft);
}

export function handleRoleDraft(roleDraft) {
  return {
    ...roleDraft,
    createdAtTime: new Date(roleDraft.createdAt).getTime(),
    updatedAtTime: new Date(roleDraft.updatedAt).getTime(),
  };
}
