import { client } from '../../../store';

import GET_PROJ_DRAFT_BRIEFS from '../../../graphql/queries/getProjDraftBriefs';
import GET_PROJ_DRAFT from '../../../graphql/queries/getProjDraft';

import CREATE_PROJ_DRAFT from '../../../graphql/mutations/createProjDraft';
import DELETE_PROJ_DRAFT from '../../../graphql/mutations/deleteProjDraft';
import UPDATE_PROJ_DRAFT from '../../../graphql/mutations/updateProjDraft';
import PROMOTE_PROJ_DRAFT from '../../../graphql/mutations/promoteProjDraft';

import CREATE_ROLE_DRAFT from '../../../graphql/mutations/createRoleDraft';
import UPDATE_ROLE_DRAFT from '../../../graphql/mutations/updateRoleDraft';
import DELETE_ROLE_DRAFT from '../../../graphql/mutations/deleteRoleDraft';

import GET_PROJ_BRIEFS_FROM_UPDATED_AT_TIME from '../../../graphql/queries/getProjBriefsFromDate';
import GET_RPOJ from '../../../graphql/queries/getProj';
import CLOSE_PROJ from '../../../graphql/mutations/closeProj';

import fixFromTime from '../fixFromTime';
import {
  handleProjDraft,
  handleProjDrafts,
  handleRoleDraft,
} from './db-handle-projDrafts-roleDrafts';
import { handleProj, handleProjBriefs } from './db-handle-projs-roles';

export default {
  getProjBriefsFromUpdatedAtTime: async updatedAtTime => {
    const from = fixFromTime(updatedAtTime);
    const res = await client.query({
      query: GET_PROJ_BRIEFS_FROM_UPDATED_AT_TIME,
      variables: { from },
    });
    return handleProjBriefs(res.data.getProjsFromDate);
  },
  getProj: async _id => {
    const res = await client.query({
      query: GET_RPOJ,
      variables: { _id },
    });
    return handleProj(res.data.getProj);
  },
  closeProj: async _id => {
    const res = await client.mutate({
      mutation: CLOSE_PROJ,
      variables: { _id },
    });
    return handleProj(res.data.closeProj);
  },
  getProjDraftBriefs: async () => {
    const res = await client.query({
      query: GET_PROJ_DRAFT_BRIEFS,
    });
    return handleProjDrafts(res.data.getProjDrafts);
  },
  getProjDraft: async _id => {
    const res = await client.query({
      query: GET_PROJ_DRAFT,
      variables: { _id },
    });
    return handleProjDraft(res.data.getProjDraft);
  },
  createProjDraft: async ({ title, test }) => {
    const res = await client.mutate({
      mutation: CREATE_PROJ_DRAFT,
      variables: { title, test },
    });
    const dbProjDraft = handleProjDraft(res.data.createProjDraft);
    return dbProjDraft;
  },
  deleteProjDraft: async _id => {
    const res = await client.mutate({
      mutation: DELETE_PROJ_DRAFT,
      variables: { _id },
    });
    const dbProjDraft = handleProjDraft(res.data.deleteProjDraft);
    return dbProjDraft;
  },
  updateProjDraft: async projDraft => {
    const res = await client.mutate({
      mutation: UPDATE_PROJ_DRAFT,
      variables: projDraft,
    });
    const dbProjDraft = handleProjDraft(res.data.updateProjDraft);
    return dbProjDraft;
  },
  promoteProjDraft: async ({ _id, status }) => {
    const res = await client.mutate({
      mutation: PROMOTE_PROJ_DRAFT,
      variables: { _id, status },
    });
    const dbProjDraft = handleProjDraft(res.data.promoteProjDraft);
    return dbProjDraft;
  },
  createRoleDraft: async ({ projDraftId, title }) => {
    console.log('PROJDAFDSFSD ID', projDraftId);
    const res = await client.mutate({
      mutation: CREATE_ROLE_DRAFT,
      variables: { projDraftId, title },
    });
    const dbRoleDraft = handleRoleDraft(res.data.createRoleDraft);
    return dbRoleDraft;
  },
  updateRoleDraft: async roleDraft => {
    const res = await client.mutate({
      mutation: UPDATE_ROLE_DRAFT,
      variables: roleDraft,
    });
    const dbRoleDraft = handleRoleDraft(res.data.updateRoleDraft);
    return dbRoleDraft;
  },
  deleteRoleDraft: async _id => {
    const res = await client.mutate({
      mutation: DELETE_ROLE_DRAFT,
      variables: { _id },
    });
    const success = res.data.deleteRoleDraft.success;
    if (!success) {
      throw new Error(`Delete roleDraft returns success=${success}`);
    }
  },
};
