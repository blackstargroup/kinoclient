export function handleProjBriefs(projs) {
  return projs.map(handleProjBrief);
}

function handleProjBrief(proj) {
  return {
    ...proj,
    thumb: proj.thumb || {},
    closedAtTime: new Date(proj.closedAt).getTime(),
    createdAtTime: new Date(proj.createdAt).getTime(),
    updatedAtTime: new Date(proj.updatedAt).getTime(),
  };
}

export function handleProj(proj) {
  const roles = proj.roles ? handleRoles(proj.roles) : [];

  return {
    ...proj,
    thumb: proj.thumb || {},
    roles,
    closedAtTime: new Date(proj.closedAt).getTime(),
    createdAtTime: new Date(proj.createdAt).getTime(),
    updatedAtTime: new Date(proj.updatedAt).getTime(),
  };
}

export function handleRoles(roles) {
  return roles.map(handleRole);
}

export function handleRole(role) {
  return {
    ...role,
    createdAtTime: new Date(role.createdAt).getTime(),
    updatedAtTime: new Date(role.updatedAt).getTime(),
  };
}
