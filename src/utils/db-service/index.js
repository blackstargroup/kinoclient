import dbProbesService from './db-probes-service';
import dbProjsService from './db-projs-service';
import dbUsersService from './db-users-service';
import dbServersService from './db-servers-service';
import dbReportsService from './db-reports-service';

export default {
  ...dbProbesService,
  ...dbProjsService,
  ...dbUsersService,
  ...dbServersService,
  ...dbReportsService,
};
