import { client } from '../../../store';
import GET_REPORTS from '../../../graphql/queries/getReports';
import ADD_REPORT from '../../../graphql/mutations/addReport';

export default {
  addReport,
  getReports,
};

async function getReports({ kind, count }) {
  console.log(`get reports from db`);
  const { data } = await client.query({
    query: GET_REPORTS,
    variables: { kind, count },
  });
  return data.getReports;
}

async function addReport({ kind, title, desc }) {
  console.log(`add report`);
  const { data } = await client.mutate({
    mutation: ADD_REPORT,
    variables: { kind, title, desc },
  });

  let status = data.addReport;
  if (!status.success) {
    throw new Error('Cant add report');
  }
}
