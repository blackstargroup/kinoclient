import { client } from '../../../store';

import GET_PROBES_AND_INFOS from '../../../graphql/queries/getProbesAndInfos';
import GET_RPOBES_FROM_DATE from '../../../graphql/queries/getProbesFromDate';
import GET_PROBE_INFOS_FROM_DATE from '../../../graphql/queries/getProbeInfosFromDate';
import GET_PROBE_MSGS_FROM_DATE from '../../../graphql/queries/getProbeMsgsFromDate';
import GET_PROBE_WITH_MSGS from '../../../graphql/queries/getProbeWithMsgs';

import ADD_PROBE_MSG from '../../../graphql/mutations/addProbeMsg';
import VISIT_PROBE from '../../../graphql/mutations/visitProbe';
import DELETE_PROBE from '../../../graphql/mutations/deleteProbe';
import CREATE_PROBE from '../../../graphql/mutations/createProbe';

import fixFromTime from '../fixFromTime';
import { handleProbe, handleProbes } from './db-handle-probes';
import { handleProbeInfo, handleProbeInfos } from './db-handle-probeInfos';
import { handleProbeMsg, handleProbeMsgs } from './db-handle-probeMsgs';

export default {
  getProbesAndInfos: async () => {
    const { data } = await client.query({
      query: GET_PROBES_AND_INFOS,
    });
    return {
      probes: handleProbes(data.getProbes),
      infos: handleProbeInfos(data.getProbeInfos),
    };
  },
  getProbesFromUpdatedAtTime: async updatedAtTime => {
    const from = fixFromTime(updatedAtTime);
    const res = await client.query({
      query: GET_RPOBES_FROM_DATE,
      variables: { from },
    });
    return handleProbes(res.data.getProbesFromDate);
  },
  getProbeInfosFromUpdatedAtTime: async updatedAtTime => {
    const from = fixFromTime(updatedAtTime);
    console.log(`get probe infos from ${from}`);
    const res = await client.query({
      query: GET_PROBE_INFOS_FROM_DATE,
      variables: { from },
    });
    return handleProbeInfos(res.data.getProbeInfosFromDate);
  },
  getProbeMsgsFromUpdatedAtTime: async (probeId, updatedAtTime) => {
    const from = fixFromTime(updatedAtTime);
    const res = await client.query({
      query: GET_PROBE_MSGS_FROM_DATE,
      variables: { probeId, from },
    });
    return handleProbeMsgs(res.data.getProbeMsgsFromDate);
  },
  getProbeWithMsgs: async probeId => {
    const { data } = await client.query({
      query: GET_PROBE_WITH_MSGS,
      variables: { probeId },
    });
    return {
      probe: handleProbe(data.getProbe),
      msgs: handleProbeMsgs(data.getProbeMsgs),
    };
  },
  sendMsg: async ({ probeId, text, attachmentJson }) => {
    console.log('db send prob msg', probeId, text, attachmentJson);
    const res = await client.mutate({
      mutation: ADD_PROBE_MSG,
      variables: { probeId, text, attachmentJson },
    });
    const msg = handleProbeMsg(res.data.addProbeMsg);
  },
  visitProbe: async _id => {
    const res = await client.mutate({
      mutation: VISIT_PROBE,
      variables: { _id },
    });
    const success = res.data.visitProbe.success;
    if (!success) {
      throw new Error(`Visit probe returns success=${success}`);
    }
  },
  createProbe: async ({ roleId, text, attachmentJson }) => {
    const res = await client.mutate({
      mutation: CREATE_PROBE,
      variables: { roleId, text, attachmentJson },
    });
    return handleProbe(res.data.createProbe);
  },
  deleteProbe: async _id => {
    const res = await client.mutate({
      mutation: DELETE_PROBE,
      variables: { _id },
    });
    const success = res.data.deleteProbe.success;
    if (!success) {
      throw new Error(`Delete probe returns success=${success}`);
    }
  },
};
