export function handleProbeInfos(infos) {
  return infos.map(handleProbeInfo);
}

export function handleProbeInfo(info) {
  return {
    ...info,
    updatedAtTime: new Date(info.updatedAt).getTime(),
  };
}
