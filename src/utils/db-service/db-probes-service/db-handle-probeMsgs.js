import { msgTypes } from '../../constants';

export function handleProbeMsgs(msgs) {
  return msgs.map(handleProbeMsg);
}

export function handleProbeMsg(msg) {
  let attachment = msg.attachment || {};
  let { mime, width, height, path, thumbPath, size } = attachment;

  if (!mime) mime = '';
  if (!path) path = '';
  if (!thumbPath) thumbPath = '';

  let ratio = 1;
  if (width && height) {
    ratio = width / height;
  }

  let msgType = msgTypes.TEXT;
  if (mime.startsWith('image')) {
    msgType = msgTypes.IMAGE;
  } else if (mime.startsWith('video')) {
    msgType = msgTypes.VIDEO;
  }

  return {
    ...msg,
    msgType,
    attachment: {
      path,
      thumbPath,
      mime,
      ratio,
      width,
      height,
      size,
    },
    createdAtTime: new Date(msg.createdAt).getTime(),
  };
}
