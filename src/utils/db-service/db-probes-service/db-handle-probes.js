import { makeFullUrl } from '../../url-helpers';

export function handleProbes(probes) {
  return probes.map(handleProbe);
}

export function handleProbe(probe) {
  return {
    ...probe,
    updatedAtTime: new Date(probe.updatedAt).getTime(),
  };
}
