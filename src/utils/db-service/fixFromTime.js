import { intervals, MIN_TIME } from '../constants';

export default function fixFromTime(time) {
  if (Date.now() > time + intervals.NOSQL_FULL_REFRESH) {
    return new Date(time);
  }
  if (time > MIN_TIME) {
    return new Date(time - intervals.NOSQL_FULL_REFRESH);
  }
  return new Date(MIN_TIME);
}
