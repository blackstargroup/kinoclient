import probesService from './probes';
import usersService from './users';
import projsService from './projs';

export default {
  setup: async () => {
    console.log('setup data service');
    await usersService.setup();
    const me = await usersService.getMe();
    if (!me) return { me };

    await probesService.setup();
    await projsService.setup();
    return { me };
  },
  refresh: async () => {
    await usersService.refresh();
    await probesService.refresh();
    await projsService.refresh();
  },
  // probes
  visitProbe: probesService.visitProbe,
  probeSendText: probesService.probeSendText,
  probeSendAttachment: probesService.probeSendAttachment,
  deleteProbe: probesService.deleteProbe,
  createProbe: probesService.createProbe,
  // users
  getMe: usersService.getMe,
  refreshMe: usersService.refreshMe,
  updateMe: usersService.updateMe,
  setAvatarFile: usersService.setAvatarFile,
  registerDir: usersService.registerDir,
  getUser: usersService.getUser,
  touchUser: usersService.touchUser,
  // projs
  dispatchProj: projsService.dispatchProj,
  forceRefetchProj: projsService.forceRefetchProj,
  closeProj: projsService.closeProj,
  dispatchProjDraft: projsService.dispatchProjDraft,
  dispatchProbeDraft: projsService.dispatchProbeDraft,
  createProjDraft: projsService.createProjDraft,
  deleteProjDraft: projsService.deleteProjDraft,
  updateProjDraft: projsService.updateProjDraft,
  promoteProjDraft: projsService.promoteProjDraft,
  createRoleDraft: projsService.createRoleDraft,
  updateRoleDraft: projsService.updateRoleDraft,
  deleteRoleDraft: projsService.deleteRoleDraft,
};
