import { MIN_TIME, YEAR } from '../../constants';
import projsSource from './source-projs';
import projDraftsSource from './source-proj-drafts';

let list = null;

export default {
  reset: () => {
    list = null;
  },
  isChanged: () => {
    return !list;
  },
  generate: () => {
    list = [];
    let projsAdded = {};
    let projDraftBriefs = projDraftsSource.getBriefs();
    let projBriefs = projsSource.getBriefs();

    Object.entries(projDraftBriefs).forEach(([key, projDraft]) => {
      const {
        _id,
        projId,
        status,
        statusTitle,
        statusColor,
        deleted,
      } = projDraft;

      if (deleted) return;

      const projBrief = projId ? projBriefs[projId] : null;
      if (projBrief) {
        projsAdded[projId] = true;
      }

      const order = projDraft.createdAtTime + 10 * YEAR;
      list.push({
        projId: projBrief && projBrief._id,
        projDraftId: _id,
        status,
        statusTitle,
        statusColor,
        thumb: projDraft.thumb,
        title: projDraft.title,
        country: projDraft.country,
        city: projDraft.city,
        test: projBrief ? projBrief.test : projDraft.test,
        closed: projBrief && projBrief.closed,
        order,
      });
    });

    Object.entries(projBriefs).forEach(([_id, projBrief]) => {
      if (projsAdded.hasOwnProperty(_id)) return;

      const {
        thumb,
        title,
        country,
        city,
        test,
        closed,
        createdAtTime,
      } = projBrief;

      let order = createdAtTime;
      if (closed) {
        order -= YEAR;
      }

      list.push({
        projId: _id,
        projDraftId: null,
        status: null,
        statusTitle: null,
        statusColor: null,
        thumb,
        title,
        country,
        city,
        test,
        closed,
        order,
      });
    });

    list.sort(comparerProjs);
    return list;
  },
};

function comparerProjs(p1, p2) {
  return p2.order - p1.order;
}
