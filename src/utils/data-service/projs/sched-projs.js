import { intervals, MIN_TIME } from '../../constants';
let items;

export default {
  setup,
  isOutOfDate,
  setLockedFetchStamp,
  setSuccessFetchStamp,
  setErrorFetchStamp,
  setOutdatedStamp,
  hasError,
};

function setup() {
  items = {};
}

function getItem(projId) {
  let item = items[projId];
  if (!item) {
    item = {
      time: MIN_TIME,
      error: false,
    };
    items[projId] = item;
  }
  return item;
}

function isOutOfDate(projId) {
  const item = getItem(projId);
  return Date.now() > item.time;
}

function setOutdatedStamp(projId) {
  const item = getItem(projId);
  item.time = MIN_TIME;
  item.error = false;
}

function setLockedFetchStamp(projId) {
  const item = getItem(projId);
  item.time = Date.now() + intervals.FETCH_LOCK_DELAY;
  item.error = false;
}

function setSuccessFetchStamp(projId) {
  const item = getItem(projId);
  item.time = Date.now() + intervals.REFETCH_PROJ;
  item.error = false;
}

function setErrorFetchStamp(projId) {
  const item = getItem(projId);
  item.time = Date.now() + intervals.FETCH_DELAY_AFTER_ERROR;
  item.error = true;
}

function hasError(projId) {
  return getItem(projId).error;
}
