import store from '../../../store';
import { projsSet } from '../../../actions/projs';
import {
  projCardSet,
  projCardLoading,
  projCardError,
} from '../../../actions/projCard';
import {
  probeDraftSet,
  probeDraftLoading,
  probeDraftError,
} from '../../../actions/probeDraft';
import {
  projDraftCardSet,
  projDraftCardError,
  projDraftCardLoading,
} from '../../../actions/projDraftCard';

import navService from '../../nav-service';
import { PROJ_DRAFT_CARD } from '../../nav-service/constants';

import projsSource from './source-projs';
import projDraftsSource from './source-proj-drafts';
import briefsCollection from './briefs-collection';

import { projDraftStatuses, intervals } from '../../constants';
import { sleep } from '../../helpers/sleep';

export default {
  setup,
  refresh,
  dispatchProj,
  forceRefetchProj,
  closeProj,
  dispatchProjDraft,
  dispatchProbeDraft,
  createProjDraft,
  deleteProjDraft,
  updateProjDraft,
  promoteProjDraft,
  createRoleDraft,
  updateRoleDraft,
  deleteRoleDraft,
};

async function setup() {
  projsSource.setup();
  projDraftsSource.setup();
  briefsCollection.reset();
}

async function refresh() {
  await tryRefreshProjBriefs();
  await tryRefreshProjDraftBriefs();

  const { routeName, params } = navService.getActiveRoute();
  if (routeName === PROJ_DRAFT_CARD) {
    const { projDraftId } = params;
    if (projDraftsSource.isProjDraftOutdated(projDraftId)) {
      await dispatchProjDraft({ projDraftId });
    }
  }
}

async function tryRefreshProjBriefs() {
  try {
    await projsSource.refreshBriefsIfOutdated();
    dispatchBriefs();
  } catch (error) {
    console.log(error);
  }
}

async function tryRefreshProjDraftBriefs() {
  try {
    await projDraftsSource.refreshBriefsIfOutdated();
    dispatchBriefs();
  } catch (error) {
    console.log(error);
  }
}

//------------------------------------------ PROJS ---------------------------------

async function dispatchProj({ projId, roleId }) {
  let proj = projsSource.getProjIfNotOutdated(projId);
  let role = proj && roleId && proj.roles.filter(x => x._id === roleId)[0];
  if (proj && (!roleId || role)) {
    store.dispatch(projCardSet({ proj, role }));
    return;
  }

  store.dispatch(projCardLoading());
  try {
    proj = await projsSource.refreshProj(projId);
    if (!proj) throw new Error('Proj not found');
    role = proj.roles.filter(x => x._id === roleId)[0];
    if (roleId && !role) throw new Error('Role not found');
    store.dispatch(projCardSet({ proj, role }));
  } catch (error) {
    console.log(error);
    store.dispatch(projCardError({ projId, roleId }));
  }
}

async function forceRefetchProj(projId) {
  return await projsSource.refreshProj(projId);
}

async function closeProj(projId) {
  await projsSource.closeProj(projId);
}

async function dispatchProbeDraft({ projId, roleId }) {
  let proj = projsSource.getProjIfNotOutdated(projId);
  let role = proj && proj.roles.filter(x => x._id === roleId)[0];
  if (proj && role) {
    store.dispatch(probeDraftSet({ proj, role }));
    return;
  }

  store.dispatch(probeDraftLoading({ progress: 0, status: null }));
  try {
    proj = await projsSource.refreshProj(projId);
    role = proj.roles.filter(x => x._id === roleId)[0];
    if (!proj || !role) throw new Error('Proj or role not found');
    store.dispatch(probeDraftSet({ proj, role }));
  } catch (error) {
    console.log(error);
    store.dispatch(probeDraftError({ projId, roleId }));
  }
}

//------------------------------------------ PROJ DRAFTS ---------------------------------

async function dispatchProjDraft({ projDraftId, roleDraftId }) {
  store.dispatch(projDraftCardLoading({ loadingText: null }));
  try {
    let projDraft = projDraftsSource.getProjDraftIfNotOutdated(projDraftId);
    if (!projDraft) {
      projDraft = await projDraftsSource.refreshProjDraft(projDraftId);
    }

    if (!projDraft) throw new Error('Cant fetch projDraft');

    let roleDraft = null;
    if (roleDraftId) {
      roleDraft = projDraft.roles.filter(r => r._id === roleDraftId)[0];
    }

    let { projId } = projDraft;
    let proj = null;
    if (!projId) {
      store.dispatch(projDraftCardSet({ projDraft, roleDraft, proj }));
      return;
    }

    proj = projsSource.getProjIfNotOutdated(projId);
    if (!proj) {
      proj = await projsSource.refreshProj(projId);
    }

    if (!proj) throw new Error('Cant fetch proj');

    store.dispatch(projDraftCardSet({ projDraft, roleDraft, proj }));
    dispatchBriefs();
  } catch (error) {
    store.dispatch(projDraftCardError({ projDraftId, roleDraftId }));
    console.log(error);
  }
}

async function createProjDraft({ title, test }) {
  try {
    let projDraft = await projDraftsSource.createProjDraft({ title, test });
    await dispatchProjDraft({ projDraftId: projDraft._id });
    dispatchBriefs();
  } catch (error) {
    store.dispatch(projDraftCardError({ projDraftId: null }));
  }
}

async function deleteProjDraft(projDraftId) {
  try {
    await projDraftsSource.deleteProjDraft(projDraftId);
    await dispatchProjDraft({ projDraftId });
    dispatchBriefs();
  } catch (error) {
    store.dispatch(projDraftCardError({ projDraftId }));
  }
}

async function updateProjDraft(projDraft) {
  let projDraftId = projDraft._id;
  try {
    store.dispatch(
      projDraftCardLoading({
        loadingText: 'Сохранение черновика...',
      }),
    );
    await projDraftsSource.updateProjDraft(projDraft);
    await dispatchProjDraft({ projDraftId });
    dispatchBriefs();
  } catch (error) {
    store.dispatch(
      projDraftCardError({
        projDraftId,
        errorText: 'Ошибка сохранения черновика',
      }),
    );
  }
}

async function promoteProjDraft({ _id, status }) {
  let projDraftId = _id;
  let publish = status === projDraftStatuses.PUBLISHED;
  loadingText = publish ? 'Публикация проекта...' : 'Изменение состояния...';
  errorText = 'Произошла ошибка';

  try {
    store.dispatch(projDraftCardLoading({ loadingText }));

    let projDraft = await projDraftsSource.promoteProjDraft({ _id, status });
    if (publish) {
      await sleep(intervals.NOSQL_REFRESH_DELAY);
      let proj = await projsSource.refreshProj(projDraft.projId);
      console.log(proj);
    }

    await dispatchProjDraft({ projDraftId });
    dispatchBriefs();
  } catch (error) {
    store.dispatch(
      projDraftCardError({
        projDraftId,
        errorText,
      }),
    );
    console.log(error);
  }
}

async function createRoleDraft({ projDraftId, title }) {
  try {
    store.dispatch(
      projDraftCardLoading({
        loadingText: 'Добавление роли...',
      }),
    );
    let roleDraft = await projDraftsSource.createRoleDraft({
      projDraftId,
      title,
    });
    let { _id: roleDraftId } = roleDraft;
    await dispatchProjDraft({ projDraftId, roleDraftId });
  } catch (error) {
    store.dispatch(
      projDraftCardError({
        projDraftId,
        errorText: 'Ошибка создания роли',
      }),
    );
    console.log(error);
  }
}

async function updateRoleDraft(roleDraft) {
  const { _id: roleDraftId, projDraftId } = roleDraft;
  try {
    store.dispatch(
      projDraftCardLoading({
        loadingText: 'Обновление роли...',
      }),
    );
    await projDraftsSource.updateRoleDraft(roleDraft);
    await dispatchProjDraft({ projDraftId, roleDraftId });
  } catch (error) {
    store.dispatch(
      projDraftCardError({
        projDraftId,
        roleDraftId,
        errorText: 'Ошибка обновления роли',
      }),
    );
    console.log(error);
  }
}

async function deleteRoleDraft({ projDraftId, roleDraftId }) {
  try {
    store.dispatch(
      projDraftCardLoading({
        loadingText: 'Удаление роли...',
      }),
    );
    await projDraftsSource.deleteRoleDraft({ projDraftId, roleDraftId });
    await dispatchProjDraft({ projDraftId, roleDraftId });
  } catch (error) {
    store.dispatch(
      projDraftCardError({
        projDraftId,
        roleDraftId,
        errorText: 'Ошибка удаления роли',
      }),
    );
    console.log(error);
  }
}

function dispatchBriefs() {
  if (!briefsCollection.isChanged()) return;
  console.log('dispatch briefs');
  let { isTester } = store.getState().me;
  let projs = briefsCollection.generate();
  if (!isTester) {
    projs = projs.filter(x => !x.test);
  }

  projs.forEach(x => console.log(`test ${x.projDraftId} ${x.test} `));
  store.dispatch(projsSet({ projs }));
}
