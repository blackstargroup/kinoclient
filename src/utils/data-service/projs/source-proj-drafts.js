import { projDraftStatuses, intervals, SECOND } from '../../constants';
import db from '../../db-service';
import usersService from '../users';
import briefsCollection from './briefs-collection';
import schedProjDrafts from './sched-proj-drafts';
import schedProjDraftBriefs from './sched-proj-draft-briefs';
import { sleep } from '../../helpers/sleep';

let briefsCache = {};
let projDraftsCache = {};

export default {
  setup: () => {
    briefsCache = {};
    projDraftsCache = {};
    schedProjDrafts.setup();
    schedProjDraftBriefs.setup();
  },
  createProjDraft,
  updateProjDraft,
  deleteProjDraft,
  promoteProjDraft,
  createRoleDraft,
  updateRoleDraft,
  deleteRoleDraft,
  refreshProjDraft,
  refreshBriefsIfOutdated: async () => {
    if (schedProjDraftBriefs.areOutOfDate()) {
      await refreshBriefs();
    }
  },
  isProjDraftOutdated: projDraftId => {
    return schedProjDrafts.isOutOfDate(projDraftId);
  },
  getProjDraftIfNotOutdated: projDraftId => {
    if (schedProjDrafts.isOutOfDate(projDraftId)) return null;
    return projDraftsCache[projDraftId];
  },
  getBriefs: () => {
    return briefsCache;
  },
};

async function createProjDraft({ title, test }) {
  let projDraft;
  try {
    projDraft = await db.createProjDraft({ title, test });
    addProjDraft(projDraft);
    schedProjDrafts.setSuccessFetchStamp(dbProjDraft._id);
  } catch (error) {
    throw error;
  }
  return projDraft;
}

async function updateProjDraft(projDraft) {
  let { _id } = projDraft;
  let updatedProjDraft;
  try {
    schedProjDrafts.setLockedFetchStamp(_id);
    updatedProjDraft = await db.updateProjDraft(projDraft);
    addProjDraft(updatedProjDraft);
    schedProjDrafts.setSuccessFetchStamp(_id);
  } catch (error) {
    schedProjDrafts.setErrorFetchStamp(_id);
    throw error;
  }
  return updatedProjDraft;
}

async function deleteProjDraft(projDraftId) {
  let projDraft;
  try {
    schedProjDrafts.setLockedFetchStamp(projDraftId);
    projDraft = await db.deleteProjDraft(projDraftId);
    addProjDraft(projDraft);
    schedProjDrafts.setSuccessFetchStamp(projDraftId);
  } catch (error) {
    schedProjDrafts.setErrorFetchStamp(projDraftId);
    throw error;
  }
  return projDraft;
}

async function promoteProjDraft({ _id, status }) {
  let projDraft;
  try {
    schedProjDrafts.setLockedFetchStamp(_id);
    projDraft = await db.promoteProjDraft({ _id, status });
    addProjDraft(projDraft);
    schedProjDrafts.setSuccessFetchStamp(_id);
  } catch (error) {
    schedProjDrafts.setErrorFetchStamp(_id);
    throw error;
  }
  return projDraft;
}

async function createRoleDraft({ projDraftId, title }) {
  let projDraft;
  let roleDraft;
  try {
    schedProjDrafts.setLockedFetchStamp(projDraftId);
    roleDraft = await db.createRoleDraft({ projDraftId, title });
    await sleep(intervals.NOSQL_REFRESH_DELAY);
    projDraft = await db.getProjDraft(projDraftId);
    addProjDraft(projDraft);
    schedProjDrafts.setSuccessFetchStamp(projDraftId);
  } catch (error) {
    schedProjDrafts.setErrorFetchStamp(projDraftId);
    throw error;
  }
  return roleDraft;
}

async function updateRoleDraft(roleDraft) {
  let projDraft;
  let { projDraftId } = roleDraft;
  try {
    schedProjDrafts.setLockedFetchStamp(projDraftId);
    roleDraft = await db.updateRoleDraft(roleDraft);
    await sleep(intervals.NOSQL_REFRESH_DELAY);
    projDraft = await db.getProjDraft(projDraftId);
    addProjDraft(projDraft);
    schedProjDrafts.setSuccessFetchStamp(projDraftId);
  } catch (error) {
    schedProjDrafts.setErrorFetchStamp(projDraftId);
    throw error;
  }
  return roleDraft;
}

async function deleteRoleDraft({ projDraftId, roleDraftId }) {
  let projDraft;
  try {
    schedProjDrafts.setLockedFetchStamp(projDraftId);
    await db.deleteRoleDraft(roleDraftId);
    await sleep(intervals.NOSQL_REFRESH_DELAY);
    projDraft = await db.getProjDraft(projDraftId);
    addProjDraft(projDraft);
    schedProjDrafts.setSuccessFetchStamp(projDraftId);
  } catch (error) {
    schedProjDrafts.setErrorFetchStamp(projDraftId);
    throw error;
  }
}

async function refreshProjDraft(projDraftId) {
  let projDraft;
  try {
    schedProjDrafts.setLockedFetchStamp(projDraftId);
    projDraft = await db.getProjDraft(projDraftId);
    addProjDraft(projDraft);
    schedProjDrafts.setSuccessFetchStamp(projDraftId);
  } catch (error) {
    schedProjDrafts.setErrorFetchStamp(projDraftId);
    throw error;
  }
  return projDraft;
}

async function refreshBriefs() {
  let me = await usersService.getMe();
  if (!me.isDir && !me.isModer) {
    schedProjDraftBriefs.setSuccessNoDraftsFetchStamp();
    return false;
  }

  try {
    schedProjDraftBriefs.setLockedFetchStamp();
    let briefs = await db.getProjDraftBriefs();
    addProjDraftBriefs(briefs);
    if (countBriefs()) {
      schedProjDraftBriefs.setSuccessHaveDraftsFetchStamp();
    } else {
      schedProjDraftBriefs.setSuccessNoDraftsFetchStamp();
    }
  } catch (error) {
    schedProjDraftBriefs.setErrorFetchStamp();
    throw error;
  }
}

function countBriefs() {
  return Object.entries(briefsCache).length;
}

function addProjDraftBriefs(drafts) {
  drafts.forEach(addProjDraftBrief);
}

function addProjDraftBrief(draft) {
  let { _id, updatedAtTime } = draft;
  let cachedBrief = briefsCache[_id];
  if (!cachedBrief || cachedBrief.updatedAtTime < updatedAtTime) {
    briefsCache[_id] = draft;
    briefsCollection.reset();
  }

  let cachedProjDraft = projDraftsCache[_id];
  if (cachedProjDraft && cachedProjDraft.updatedAtTime < updatedAtTime) {
    schedProjDrafts.setOutdatedStamp(_id);
  }
}

function addProjDraft(draft) {
  let { _id, updatedAtTime } = draft;

  projDraftsCache[_id] = draft;

  let cachedBrief = briefsCache[_id];
  if (!cachedBrief || cachedBrief.updatedAtTime < updatedAtTime) {
    briefsCache[_id] = draft;
    briefsCollection.reset();
  }
}
