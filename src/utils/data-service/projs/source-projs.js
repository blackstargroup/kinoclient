import { MIN_TIME } from '../../constants';
import db from '../../db-service';
import briefsCollection from './briefs-collection';
import schedProjs from './sched-projs';
import schedProjBriefs from './sched-proj-briefs';

let projsCache = {};
let briefsCache = {};
let lastProjBriefsUpdatedAtTime = MIN_TIME;

export default {
  setup: () => {
    projsCache = {};
    briefsCache = {};
    lastProjBriefsUpdatedAtTime = MIN_TIME;

    schedProjs.setup();
    schedProjBriefs.setup();
  },
  getProj: projId => {
    if (!projId) return null;
    let proj = projsCache[projId];
    return proj;
  },
  getProjIfNotOutdated: projId => {
    if (!projId) return null;
    if (schedProjs.isOutOfDate(projId)) return null;
    return projsCache[projId];
  },
  closeProj,
  refreshProj,
  refreshBriefsIfOutdated: async () => {
    if (schedProjBriefs.areOutOfDate()) {
      await refreshBriefs();
    }
  },
  getBriefs: () => {
    return briefsCache;
  },
};

async function refreshProj(projId) {
  if (!projId) return null;
  let proj;
  try {
    schedProjs.setLockedFetchStamp(projId);
    proj = await db.getProj(projId);
    if (!proj) throw new Error('Proj not found');
    addProj(proj);
    schedProjs.setSuccessFetchStamp(projId);
  } catch (error) {
    schedProjs.setErrorFetchStamp(projId);
    throw error;
  }
  return proj;
}

async function closeProj(projId) {
  let proj;
  try {
    schedProjs.setLockedFetchStamp(projId);
    proj = await db.closeProj(projId);
    if (!proj) throw new Error('Proj not found');
    addProj(proj);
    schedProjs.setSuccessFetchStamp(projId);
  } catch (error) {
    schedProjs.setErrorFetchStamp(projId);
    throw error;
  }
  return proj;
}

async function refreshBriefs() {
  try {
    schedProjBriefs.setLockedFetchStamp();
    let briefs = await db.getProjBriefsFromUpdatedAtTime(
      lastProjBriefsUpdatedAtTime,
    );
    addProjBriefs(briefs);
    schedProjBriefs.setSuccessFetchStamp();
  } catch (error) {
    schedProjBriefs.setErrorFetchStamp();
    throw error;
  }
}

function addProj(proj) {
  let { _id, updatedAtTime } = proj;

  projsCache[_id] = proj;

  let cachedBrief = briefsCache[_id];
  if (!cachedBrief || cachedBrief.updatedAtTime < updatedAtTime) {
    briefsCache[_id] = proj;
    briefsCollection.reset();
  }
}

function addProjBriefs(dbProjs) {
  dbProjs.forEach(proj => {
    const { _id, updatedAtTime } = proj;
    let cachedBrief = briefsCache[_id];
    if (!cachedBrief || cachedBrief.updatedAtTime < updatedAtTime) {
      briefsCache[_id] = proj;
      briefsCollection.reset();
    }

    if (lastProjBriefsUpdatedAtTime < updatedAtTime) {
      lastProjBriefsUpdatedAtTime = updatedAtTime;
    }

    const cachedProj = projsCache[_id];
    if (cachedProj && cachedProj.updatedAtTime < updatedAtTime) {
      schedProjs.setOutdatedStamp(_id);
    }
  });
}
