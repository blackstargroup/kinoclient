import { intervals, MIN_TIME } from '../../constants';
let items;

export default {
  setup,
  isOutOfDate,
  setLockedFetchStamp,
  setSuccessFetchStamp,
  setErrorFetchStamp,
  setOutdatedStamp,
  hasError,
};

function setup() {
  items = {};
}

function getItem(projDraftId) {
  let item = items[projDraftId];
  if (!item) {
    item = {
      time: MIN_TIME,
      error: false,
    };
    items[projDraftId] = item;
  }
  return item;
}

function isOutOfDate(projDraftId) {
  const item = getItem(projDraftId);
  return Date.now() > item.time;
}

function setOutdatedStamp(projDraftId) {
  const item = getItem(projDraftId);
  item.time = MIN_TIME;
  item.error = false;
}

function setLockedFetchStamp(projDraftId) {
  const item = getItem(projDraftId);
  item.time = Date.now() + intervals.FETCH_LOCK_DELAY;
  item.error = false;
}

function setSuccessFetchStamp(projDraftId) {
  const item = getItem(projDraftId);
  item.time = Date.now() + intervals.REFETCH_PROJ_DRAFT;
  item.error = false;
}

function setErrorFetchStamp(projDraftId) {
  const item = getItem(projDraftId);
  item.time = Date.now() + intervals.FETCH_DELAY_AFTER_ERROR;
  item.error = true;
}

function hasError(projDraftId) {
  return getItem(projDraftId).error;
}
