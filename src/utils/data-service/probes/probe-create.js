import store from '../../../store';
import {
  probeDraftLoading,
  probeDraftError,
  probeDraftSuccess,
} from '../../../actions/probeDraft';
import reportService from '../../report-service';
import db from '../../db-service';
import { sleep } from '../../helpers/sleep';
import { DEV_SLEEP, uploadKinds } from '../../constants';
import uploadFiles from '../../helpers/upload-files';
import schedInfos from './sched-infos';

export default async function probeCreate({ text, attachments }) {
  let { roleId, projId } = store.getState().probeDraft;

  try {
    store.dispatch(probeDraftLoading({ progress: 0, status: '🤔 подготовка' }));

    __DEV__ && (await sleep(DEV_SLEEP));
    let { serverName, uploadUrl } = await db.getUploadServer();

    let attachmentJson = await uploadAttachments({
      serverName,
      uploadUrl,
      attachments,
    });
    __DEV__ && (await sleep(DEV_SLEEP));

    store.dispatch(
      probeDraftLoading({ progress: 0, status: '🤔 сохранение пробы' }),
    );

    __DEV__ && (await sleep(DEV_SLEEP));

    let probe = await createProbe({ roleId, text, attachmentJson });

    __DEV__ && (await sleep(DEV_SLEEP));

    schedInfos.setOutdatedStamp();

    store.dispatch(probeDraftSuccess());
  } catch (error) {
    reportService.sendErrorReportSafety({
      title: 'Error while sending probe',
      error,
    });
    store.dispatch(probeDraftError({ projId, roleId }));
    console.log(error);
  }
}

async function uploadAttachments({ serverName, uploadUrl, attachments }) {
  let files = [];

  for (let i = 0; i < attachments.length; ++i) {
    let { file, thumbFile } = attachments[i];
    if (!file) throw new Error('invalid input');
    files.push(file);
    if (thumbFile) files.push(thumbFile);
  }

  let res = await uploadFiles({
    uploadUrl,
    kind: uploadKinds.PROBE_FILE,
    files,
    progressCallback: ({ progress, status }) => {
      store.dispatch(probeDraftLoading({ progress, status }));
      console.log(progress);
    },
  });

  let obj = {
    serverName,
    attachments: [],
  };

  for (let i = 0; i < attachments.length; ++i) {
    let { file, thumbFile } = attachments[i];
    let fileId = res[file.path];
    let thumbFileId = thumbFile && thumbFile.path && res[thumbFile.path];
    obj.attachments.push({
      fileId,
      thumbFileId,
    });
  }

  return JSON.stringify(obj);
}

async function createProbe({ roleId, text, attachmentJson }) {
  let probe = await db.createProbe({ roleId, text, attachmentJson });
  return probe;
}
