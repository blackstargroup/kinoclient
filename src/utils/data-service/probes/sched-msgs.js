import { intervals, MIN_TIME } from '../../constants';
let items;

export default {
  setup,
  areOutOfDate,
  setLockedFetchStamp,
  setSuccessFetchStamp,
  setErrorFetchStamp,
  setOutdatedStamp,
  hasError,
};

function setup() {
  items = {};
}

function getItem(probeId) {
  let item = items[probeId];
  if (!item) {
    item = {
      time: MIN_TIME,
      error: false,
    };
    items[probeId] = item;
  }
  return item;
}

function areOutOfDate(probeId) {
  const item = getItem(probeId);
  return Date.now() > item.time;
}

function setOutdatedStamp(probeId) {
  const item = getItem(probeId);
  item.time = MIN_TIME;
  item.error = false;
}

function setLockedFetchStamp(probeId) {
  const item = getItem(probeId);
  item.time = Date.now() + intervals.FETCH_LOCK_DELAY;
  item.error = false;
}

function setSuccessFetchStamp(probeId) {
  const item = getItem(probeId);
  item.time = Date.now() + intervals.REFETCH_PROBE_MSGS;
  item.error = false;
}

function setErrorFetchStamp(probeId) {
  const item = getItem(probeId);
  item.time = Date.now() + intervals.FETCH_DELAY_AFTER_ERROR;
  item.error = true;
}

function hasError(probeId) {
  return getItem(probeId).error;
}
