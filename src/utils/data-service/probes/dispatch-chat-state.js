import { chatSet, chatError } from '../../../actions/chat';
import store from '../../../store';
import source from './source';
import schedMsgs from './sched-msgs';

export default function dispatchChatState(probeId) {
  const error = schedMsgs.hasError(probeId);
  const { probe, msgs } = source.getItem(probeId);
  if (error) {
    store.dispatch(chatError({ probe, error }));
    return;
  }
  store.dispatch(chatSet({ probe, msgs }));
}
