import { intervals, MIN_TIME, SECOND, MINUTE, HOUR } from '../../constants';
import source from './source';

let nextFetch;

export default {
  setup,
  areOutOfDate,
  setOutdatedStamp,
  setLockedFetchStamp,
  setSuccessFetchStamp,
  setErrorFetchStamp,
};

function setup() {
  nextFetch = MIN_TIME;
}

function areOutOfDate() {
  return Date.now() > nextFetch;
}

function setOutdatedStamp() {
  nextFetch = MIN_TIME;
}

function setLockedFetchStamp() {
  nextFetch = Date.now() + intervals.FETCH_LOCK_DELAY;
}

function setErrorFetchStamp() {
  nextFetch = Date.now() + intervals.FETCH_DELAY_AFTER_ERROR;
}

function setSuccessFetchStamp() {
  nextFetch = calcNextFetchTime();
}

function calcNextFetchTime() {
  const now = Date.now();
  const lastInfoTime = source.getLastUpdatedAtInfoTime();
  const diff = now - lastInfoTime;

  if (__DEV__) {
    if (diff > 2 * MINUTE) {
      return now + 10 * SECOND;
    }
    return now + 2 * SECOND;
  }

  if (diff > 2 * HOUR) {
    return now + MINUTE;
  }
  if (diff > HOUR) {
    return now + 30 * SECOND;
  }
  if (diff > 5 * MINUTE) {
    return now + 15 * SECOND;
  }
  if (diff > MINUTE) {
    return now + 10 * SECOND;
  }
  if (diff > 10 * SECOND) {
    return now + 5 * SECOND;
  }
  return now + 3 * SECOND;
}
