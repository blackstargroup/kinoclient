import { chatSet, chatLoading, chatError } from '../../../actions/chat';
import store from '../../../store';
import source from './source';
import schedInfos from './sched-infos';
import schedMsgs from './sched-msgs';

import navService from '../../nav-service';
import db from '../../db-service';
import dispatchChatState from './dispatch-chat-state';

export default async function probeSendText({ probeId, text }) {
  text = text ? text.trim() : null;
  if (!text) return;

  const route = navService.getActiveRoute();
  const item = source.getItem(probeId);

  try {
    schedMsgs.setLockedFetchStamp(probeId);
    schedInfos.setLockedFetchStamp();
    await db.sendMsg({ probeId, text });
    const dbMsgs = await db.getProbeMsgsFromUpdatedAtTime(
      probeId,
      item.lastUpdatedAtMsgTime,
    );
    source.addMsgs(probeId, dbMsgs);
    schedMsgs.setSuccessFetchStamp(probeId);
    schedInfos.setOutdatedStamp();
  } catch (error) {
    schedMsgs.setErrorFetchStamp(probeId);
    schedInfos.setErrorFetchStamp();
    console.log(`send probe msg probe=${probeId} error=`, error);
  }

  if (navService.routeEqualToActive(route)) {
    await dispatchChatState(probeId);
  }
}
