import { chatSet, chatLoading, chatError } from '../../../actions/chat';
import store from '../../../store';
import source from './source';
import schedInfos from './sched-infos';
import schedMsgs from './sched-msgs';

import navService from '../../nav-service';
import db from '../../db-service';
import dispatchChatState from './dispatch-chat-state';
import { sleep } from '../../helpers/sleep';
import { DEV_SLEEP, uploadKinds } from '../../constants';
import uploadFiles from '../../helpers/upload-files';
import reportService from '../../report-service';

export default async function probeSendAttachment({ probeId, attachments }) {
  let route = navService.getActiveRoute();
  let item = source.getItem(probeId);
  let { probe } = item;

  try {
    schedMsgs.setLockedFetchStamp(probeId);
    schedInfos.setLockedFetchStamp();

    store.dispatch(
      chatLoading({ probe, progress: 0, loadingText: '🤔 подготовка' }),
    );

    __DEV__ && (await sleep(DEV_SLEEP));
    let { serverName, uploadUrl } = await db.getUploadServer();

    let attachmentJson = await uploadAttachments({
      probe,
      serverName,
      uploadUrl,
      attachments,
    });

    __DEV__ && (await sleep(DEV_SLEEP));

    await db.sendMsg({ probeId, attachmentJson });

    let dbMsgs = await db.getProbeMsgsFromUpdatedAtTime(
      probeId,
      item.lastUpdatedAtMsgTime,
    );
    source.addMsgs(probeId, dbMsgs);
    schedMsgs.setSuccessFetchStamp(probeId);
    schedInfos.setOutdatedStamp();
  } catch (error) {
    schedMsgs.setErrorFetchStamp(probeId);
    schedInfos.setErrorFetchStamp();
    reportService.sendErrorReportSafety({
      title: 'Cant send msg with attachment',
      error,
    });
    console.log(`send probe msg probe=${probeId} error=`, error);
  }

  if (navService.routeEqualToActive(route)) {
    await dispatchChatState(probeId);
  }
}

async function uploadAttachments({
  probe,
  serverName,
  uploadUrl,
  attachments,
}) {
  let files = [];
  for (let i = 0; i < attachments.length; ++i) {
    let { file, thumbFile } = attachments[i];
    if (!file) throw new Error('invalid input');
    files.push(file);
    if (thumbFile) files.push(thumbFile);
  }

  let res = await uploadFiles({
    uploadUrl,
    kind: uploadKinds.PROBE_FILE,
    files,
    progressCallback: ({ progress, status }) => {
      store.dispatch(chatLoading({ probe, progress, loadingText: status }));
      console.log(progress);
    },
  });

  let obj = {
    serverName,
    attachments: [],
  };

  for (let i = 0; i < attachments.length; ++i) {
    let { file, thumbFile } = attachments[i];
    let fileId = res[file.path];
    let thumbFileId = thumbFile && thumbFile.path && res[thumbFile.path];
    obj.attachments.push({
      fileId,
      thumbFileId,
    });
  }

  return JSON.stringify(obj);
}
