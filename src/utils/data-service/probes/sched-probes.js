import { intervals, MIN_TIME } from '../../constants';
let nextFetch;

export default {
  setup,
  areOutOfDate,
  setLockedFetchStamp,
  setSuccessFetchStamp,
  setErrorFetchStamp,
  setOutdatedStamp,
};

function setup() {
  nextFetch = MIN_TIME;
}

function areOutOfDate() {
  return Date.now() > nextFetch;
}

function setOutdatedStamp() {
  nextFetch = MIN_TIME;
}

function setLockedFetchStamp() {
  nextFetch = Date.now() + intervals.FETCH_LOCK_DELAY;
}

function setErrorFetchStamp() {
  nextFetch = Date.now() + intervals.FETCH_DELAY_AFTER_ERROR;
}

function setSuccessFetchStamp() {
  nextFetch = Date.now() + intervals.REFETCH_PROBES;
}
