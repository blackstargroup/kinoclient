import { MIN_TIME } from '../../constants';
import usersService from '../users';

lastUpdatedAtInfoTime = MIN_TIME;
lastUpdatedAtProbeTime = MIN_TIME;
items = {};
probesArray = null;

let foundNewMsgDelegate;
let foundNewProbeDelegate;

export default {
  setup: ({ onFoundNewMsg, onFoundNewProbe, dbProbes, dbInfos }) => {
    foundNewMsgDelegate = onFoundNewMsg;
    foundNewProbeDelegate = onFoundNewProbe;

    lastUpdatedAtInfoTime = MIN_TIME;
    lastUpdatedAtProbeTime = MIN_TIME;
    items = {};
    probesArray = null;

    addDbProbesAndInfosOnSetup({ dbProbes, dbInfos });
  },
  getItem,
  deleteItem: probeId => {
    // delete items[projId];
    // its enough remove info only,
    // cause if no info we don't show probes
    probesArray = null;
    getItem(probeId).info = null;
  },
  addProbes,
  addInfos,
  addMsgs,
  getProbesList,
  getLastUpdatedAtProbeTime: () => {
    return lastUpdatedAtProbeTime;
  },
  getLastUpdatedAtInfoTime: () => {
    return lastUpdatedAtInfoTime;
  },
  getVisitedByDir: probeId => {
    const { probe } = getItem(probeId);
    return probe ? probe.visitedByDir : false;
  },
  resetVisitedByDir: probeId => {
    const { probe } = getItem(probeId);
    probe && (probe.visitedByDir = true);
  },
  resetUnreadCount: probeId => {
    const { info } = getItem(probeId);
    if (info && info.unreadCount) {
      info.unreadCount = 0;
      probesArray = null;
    }
  },
};

function getItem(probeId) {
  let item = items[probeId];
  if (!item) {
    item = {
      probe: null,
      info: null,
      error: null,
      msgs: [],
      msgIds: {},
      lastUpdatedAtMsgTime: MIN_TIME,
    };
    items[probeId] = item;
  }
  return item;
}

function addDbProbesAndInfosOnSetup({ dbProbes, dbInfos }) {
  const ids = {};
  dbInfos.forEach(info => {
    ids[info.probeId] = true;
  });
  dbProbes.forEach(probe => {
    if (!ids.hasOwnProperty(probe._id)) return;
    const item = getItem(probe._id);
    item.probe = probe;
  });
  addInfos(dbInfos);
}

function addProbes(dbProbes) {
  let affected = false;
  dbProbes.forEach(probe => {
    const item = getItem(probe._id);
    item.probe = probe;
    usersService.touchUser(probe.userId);
    affected = true;
    probesArray = null;
  });

  return affected;
}

function addInfos(dbInfos) {
  let affected = false;

  dbInfos.forEach(info => {
    const { probeId, updatedAtTime } = info;

    const item = getItem(probeId);

    if (lastUpdatedAtInfoTime < updatedAtTime) {
      lastUpdatedAtInfoTime = updatedAtTime;
      affected = true;
      probesArray = null;
    }

    if (!item.info || item.info.msgId != info.msgId) {
      foundNewMsgDelegate && foundNewMsgDelegate(probeId);
    }

    item.info = info;

    if (!item.probe) {
      foundNewProbeDelegate && foundNewProbeDelegate(probeId);
    }
  });

  return affected;
}

function comparerMsgsASC(m1, m2) {
  return m1.createdAtTime - m2.createdAtTime;
}

function addMsgs(probeId, dbMsgs) {
  const item = getItem(probeId);

  let affected = false;
  dbMsgs.forEach(msg => {
    const { _id, userId, updatedAtTime } = msg;

    if (updatedAtTime > item.lastUpdatedAtMsgTime) {
      item.lastUpdatedAtMsgTime = updatedAtTime;
    }

    if (item.msgIds.hasOwnProperty(_id)) return;
    usersService.touchUser(userId);
    item.msgIds[_id] = true;
    item.msgs.push(msg);
    affected = true;
  });

  if (affected) {
    item.msgs.sort(comparerMsgsASC);
  }
  return affected;
}

function comparerProbesDESC(p1, p2) {
  return p2.order - p1.order;
}

function getProbesList(me) {
  if (probesArray) return probesArray;
  probesArray = [];

  Object.entries(items).forEach(([_id, item]) => {
    let { probe, info, lastUpdatedAtMsgTime } = item;

    if (!probe || !info) {
      return;
    }

    const isActor = me._id === probe.userId;
    let title;
    let thumbPath;

    if (isActor) {
      title = probe.projTitle;
      thumbPath = probe.projThumbPath;
    } else {
      title = `${probe.userFirstName} ${probe.userLastName}, ${
        probe.roleTitle
      }`;
      thumbPath = probe.userAvatarPath;
    }

    probesArray.push({
      _id: probe._id,
      isActor,
      thumbPath,
      title,
      msgText: info.msgText,
      lastUpdatedAtMsgTime,
      unreadCount: info.unreadCount,
      order: info.updatedAtTime || MIN_TIME,
    });
  });

  probesArray.sort(comparerProbesDESC);
  return probesArray;
}
