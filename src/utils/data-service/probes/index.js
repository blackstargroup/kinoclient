import { chatSet, chatLoading, chatError } from '../../../actions/chat';
import { probesSet } from '../../../actions/probes';
import store from '../../../store';
import source from './source';
import schedInfos from './sched-infos';
import schedProbes from './sched-probes';
import schedMsgs from './sched-msgs';
import db from '../../db-service';

import navService from '../../nav-service';
import { PROBE_AS_ACTOR, PROBE_AS_DIRECTOR } from '../../nav-service/constants';
import createProbe from './probe-create';
import probeSendText from './probe-send-text';
import probeSendAttachment from './probe-send-attachment';
import dispatchChatState from './dispatch-chat-state';

import usersService from '../users';

export default {
  setup,
  refresh,
  visitProbe,
  probeSendText,
  probeSendAttachment,
  deleteProbe,
  createProbe,
};

async function setup() {
  schedInfos.setup();
  schedProbes.setup();
  schedMsgs.setup();

  let dbProbes;
  let dbInfos;
  try {
    console.log('get probes and infos');
    schedProbes.setLockedFetchStamp();
    schedInfos.setLockedFetchStamp();
    const data = await db.getProbesAndInfos();
    dbProbes = data.probes;
    dbInfos = data.infos;
    schedInfos.setSuccessFetchStamp();
    schedProbes.setSuccessFetchStamp();
    console.log('get probes and infos OK');
  } catch (error) {
    console.log('get probes and infos failed', error);
    dbProbes = [];
    dbInfos = [];
    schedInfos.setErrorFetchStamp();
    schedProbes.setErrorFetchStamp();
  }

  source.setup({
    onFoundNewMsg: probeId => {
      schedMsgs.setOutdatedStamp(probeId);
    },
    onFoundNewProbe: probeId => {
      schedProbes.setOutdatedStamp();
    },
    dbProbes,
    dbInfos,
  });

  dispatchProbesState();
}

async function visitProbe(route) {
  const { routeName, params } = route;
  const { probeId } = params;

  console.log('visit probe ', new Date(Date.now()), route);

  if (!probeId) return;

  let error = schedMsgs.hasError(probeId);
  const outdated = schedMsgs.areOutOfDate(probeId);
  if (error && !outdated) {
    dispatchChatState(probeId);
    return;
  }

  let item = source.getItem(probeId);
  let { probe, msgs, info } = item;
  let hasProbeAndMsgs = probe && msgs.length;
  let hasUnreadMsgs = outdated || (info && info.unreadCount);

  if (hasProbeAndMsgs && !hasUnreadMsgs) {
    store.dispatch(chatSet({ probe, msgs }));
    return;
  }

  store.dispatch(chatLoading({ probe, progress: 0, loadingText: null }));
  await forceFetchChatMsgs(probeId);

  if (navService.routeEqualToActive(route)) {
    dispatchChatState(probeId);

    if (routeName === PROBE_AS_DIRECTOR && !source.getVisitedByDir()) {
      source.resetVisitedByDir();
      // don't wait result
      db.visitProbe(probeId);
    }
  }
}

async function refresh() {
  await tryRefreshInfos();
  await tryRefreshMsgs();
  await tryRefreshProbes();
}

async function tryRefreshProbes() {
  if (!schedProbes.areOutOfDate()) return;
  let dbProbes;
  try {
    schedProbes.setLockedFetchStamp();
    const from = source.getLastUpdatedAtProbeTime();
    dbProbes = await db.getProbesFromUpdatedAtTime(from);
    schedProbes.setSuccessFetchStamp();
  } catch (error) {
    console.log(error);
    schedProbes.setErrorFetchStamp();
  }
  if (dbProbes && dbProbes.length && source.addProbes(dbProbes)) {
    await dispatchProbesState();
  }
}

async function tryRefreshInfos() {
  if (!schedInfos.areOutOfDate()) return;
  let dbInfos;
  try {
    schedInfos.setLockedFetchStamp();
    const from = source.getLastUpdatedAtInfoTime();
    dbInfos = await db.getProbeInfosFromUpdatedAtTime(from);
    schedInfos.setSuccessFetchStamp();
  } catch (error) {
    console.log(error);
    schedInfos.setErrorFetchStamp();
  }
  if (dbInfos && dbInfos.length && source.addInfos(dbInfos)) {
    await dispatchProbesState();
  }
}

async function tryRefreshMsgs() {
  const route = navService.getActiveRoute();
  const { routeName, params } = route;
  const { probeId } = params;

  if (!probeId) return;
  if (routeName !== PROBE_AS_ACTOR && routeName !== PROBE_AS_DIRECTOR) {
    return;
  }

  if (!schedMsgs.areOutOfDate(probeId)) return;

  await forceFetchChatMsgs(probeId);

  if (navService.routeEqualToActive(route)) {
    dispatchChatState(probeId);
  }
}

async function dispatchProbesState() {
  const me = await usersService.getMe();
  const probes = source.getProbesList(me);
  store.dispatch(probesSet({ probes }));
}

async function forceFetchChatMsgs(probeId) {
  const item = source.getItem(probeId);
  const fetchMsgsOnly = item.probe && item.msgs.length;

  let fetchError;
  try {
    schedMsgs.setLockedFetchStamp(probeId);
    if (fetchMsgsOnly) {
      const dbMsgs = await db.getProbeMsgsFromUpdatedAtTime(
        probeId,
        item.lastUpdatedAtMsgTime,
      );
      source.addMsgs(probeId, dbMsgs);
    } else {
      const data = await db.getProbeWithMsgs(probeId);
      source.addProbes([data.probe]);
      source.addMsgs(probeId, data.msgs);
    }
    schedMsgs.setSuccessFetchStamp(probeId);
  } catch (fetchError) {
    schedMsgs.setErrorFetchStamp(probeId);
    console.log(fetchError);
  }

  if (!fetchError) {
    // actually no need call resetUnreadCount,
    // cause nex ProbeInfoMsg will reset that flag
    // but we don't want to wait
    if (item.info && item.info.unreadCount) {
      source.resetUnreadCount(probeId);
      dispatchProbesState();
    }
  }

  if (
    !schedInfos.areOutOfDate() &&
    item.lastUpdatedAtMsgTime > source.getLastUpdatedAtInfoTime()
  ) {
    schedInfos.setOutdatedStamp();
  }
}

async function deleteProbe(probeId) {
  await db.deleteProbe(probeId);
  source.deleteItem(probeId);
  dispatchProbesState();
}
