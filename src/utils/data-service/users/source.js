import { MIN_TIME } from '../../constants';

let items = {};
let itemsArray = [];
let me = null;

export default {
  setup,
  getItem,
  getItems,
  getMe,
  putMe,
};

function setup() {
  console.log('setup users service');
  items = {};
  itemsArray = [];
  me = null;
}

function getItem(userId) {
  let item = items[userId];
  if (item) return item;
  item = {
    userId,
    user: null,
    lockTime: MIN_TIME,
  };
  items[userId] = item;
  itemsArray.push(item);
  return item;
}

function getItems() {
  return itemsArray;
}

function getMe() {
  return me;
}

function putMe(user) {
  me = user;
  const item = getItem(user._id);
  item.user = user;
}
