import onRefreshProfile from '../../../app/on-refresh-profile';
import store from '../../../store';
import { setMe } from '../../../actions/me';
import { cachedUsersAdd } from '../../../actions/cachedUsers';

import source from './source';
import db from '../../db-service';

import { intervals } from '../../constants';

export default {
  setup,
  getMe,
  refreshMe,
  updateMe,
  setAvatarFile,
  registerDir,
  getUser,
  refresh,
  touchUser,
};

async function setup() {
  source.setup();
}

async function getMe() {
  const me = source.getMe();
  if (me) return me;
  return refreshMe();
}

async function refreshMe() {
  const me = await db.getMe();
  if (me) {
    source.putMe(me);
    store.dispatch(cachedUsersAdd(me));
  }
  store.dispatch(setMe(me));
  // dont wait result
  onRefreshProfile(me);
  return me;
}

async function updateMe(me) {
  me = await db.updateMe(me);
  if (me) {
    source.putMe(me);
    store.dispatch(cachedUsersAdd(me));
  }
  store.dispatch(setMe(me));
  // dont wait result
  onRefreshProfile(me);
  return me;
}

async function setAvatarFile({ serverName, fileId }) {
  let me = await db.setAvatar({ serverName, fileId });

  if (me) {
    source.putMe(me);
    store.dispatch(cachedUsersAdd(me));
  }
  store.dispatch(setMe(me));
  // dont wait result
  onRefreshProfile(me);
  return me;
}

async function registerDir(dir) {
  const me = await db.registerDir(dir);
  if (me) {
    source.putMe(me);
    store.dispatch(cachedUsersAdd(me));
  }
  store.dispatch(setMe(me));
  // dont wait result
  onRefreshProfile(me);
  return me;
}

async function getUser(userId) {
  const item = source.getItem(userId);
  if (item.user) return item.user;

  item.lockTime = Date.now() + 10000;
  user = await db.getUser(userId);
  if (user) {
    item.user = user;
    store.dispatch(cachedUsersAdd(user));
  }
  return user;
}

function refresh() {
  const items = source.getItems();
  items.forEach(handleItem);
}

function touchUser(userId) {
  const item = source.getItem(userId);
  handleItem(item);
}

function handleItem(item) {
  if (item.user) return;
  if (item.lockTime > Date.now()) return;

  item.lockTime = Date.now() + 10000;
  db.getUser(item.userId)
    .then(user => {
      item.user = user;
      store.dispatch(cachedUsersAdd(user));
    })
    .catch(error => {
      const lockTime = Date.now() + intervals.FETCH_DELAY_AFTER_ERROR;
      if (item.lockTime < lockTime) {
        item.lockTime = lockTime;
      }
    });
}
