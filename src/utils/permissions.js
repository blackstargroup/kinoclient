import { Alert, Platform } from 'react-native';
import Permissions from 'react-native-permissions';
// https://github.com/yonahforst/react-native-permissions

export function requestPhotoPermission() {
  return new Promise(async (resolve, reject) => {
    let perm = await Permissions.check('photo');

    if (perm === 'authorized') {
      resolve();
      return;
    }

    if (perm === 'undetermined') {
      perm = await Permissions.request('photo');
      perm === 'authorized'
        ? resolve()
        : reject('Пользователь запретил досуп к галерее');
      return;
    }

    Alert.alert(
      'Разрешить доступ к фото/видео?',
      'Вы сможете обмениваться фото/видео с другими пользователями, а так же участвовать в кастингах',
      [
        {
          text: 'Нет',
          onPress: () => {},
        },
        {
          text: 'Да',
          onPress: () => {
            Permissions.openSettings();
          },
        },
      ],
    );

    reject('Отсутствует доступ к галерее');
  });
}

export function requestNotificationPermission() {
  /*
    FCM way to get notificaitons
   let result = await FCM.requestPermissions({
      badge: false,
      sound: true,
      alert: true,
    }); */

  return new Promise(async (resolve, reject) => {
    if (Platform.OS !== 'ios') {
      resolve();
      return;
    }

    let perm = await Permissions.check('notification', { type: ['alert'] });

    if (perm === 'authorized') {
      resolve();
      return;
    }

    if (perm === 'undetermined') {
      perm = await Permissions.request('notification', {
        type: ['alert', 'sound'],
      });
      perm === 'authorized'
        ? resolve()
        : reject('Пользователь запретил досуп к уведомлениям');
      return;
    }

    Alert.alert(
      'Разрешить уведомления?',
      'Вы сможете получать уведомления о сообщениях когда приложение закрыто',
      [
        {
          text: 'Нет',
          onPress: () => {},
        },
        {
          text: 'Да',
          onPress: () => {
            Permissions.openSettings();
          },
        },
      ],
    );

    reject('Отсутствует доступ к уведомлениям');
  });
}
