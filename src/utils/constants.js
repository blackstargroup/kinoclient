/* colors https://casesandberg.github.io/react-color/ */

export const SECOND = 1000;
export const MINUTE = SECOND * 60;
export const HOUR = MINUTE * 60;
export const DAY = HOUR * 24;
export const YEAR = DAY * 365;
export const MIN_DATE = new Date(2000, 0, 1); // 1 of Jan 2000
export const MIN_TIME = MIN_DATE.getTime();

export const projDraftStatuses = {
  NEW: 'new',
  AFFECTED: 'affected',
  MODERATING: 'moderating',
  APPROVED: 'approved',
  DENIED: 'denied',
  PUBLISHED: 'published',
};

export const intervals = {
  REFETCH_PROJ_BRIEFS: 20 * MINUTE,
  REFETCH_PROJ: 30 * DAY,
  REFETCH_PROJ_DRAFT: __DEV__ ? 10 * SECOND : 40 * SECOND,
  REFETCH_PROJ_DRAFT_BRIEFS_HAVE_DRAFTS: 2 * MINUTE,
  REFETCH_PROJ_DRAFT_BRIEFS_NO_DRAFTS: HOUR,
  REFETCH_PROBE_MSGS: 2 * HOUR,
  REFETCH_PROBES: 2 * HOUR,
  REFRESH_PROJS_AFTER_PROJ_PUBLISH: 10 * SECOND,
  USER_TOUCH_LOCK_DELAY: 10 * SECOND,
  TASKS_SERVICE_ERROR_DELAY: 2 * SECOND,
  FETCH_DELAY_AFTER_ERROR: 5 * SECOND,
  FETCH_LOCK_DELAY: MINUTE,
  // as we use no sql db, we wan't to be sure that we fetch all msgs
  NOSQL_FULL_REFRESH: MINUTE,
  NOSQL_REFRESH_DELAY: __DEV__ ? 1 * SECOND : 3 * SECOND,
};

export const msgTypes = {
  TEXT: 'text',
  IMAGE: 'image',
  VIDEO: 'video',
};

export const uploadKinds = {
  AVATAR: 'avatar',
  PROBE_FILE: 'probe-file',
};

export const DEV_SLEEP = 300;

export const MAX_PROBE_ATTACHMENTS = 2;
export const NO_AVATAR = require('../../res/noavatar.png');
export const NO_PROJ_THUMB = require('../../res/noprojthumb.png');
