import navService from './nav-service';
import dataService from './data-service';
import { MIN_TIME, MINUTE, SECOND } from '../utils/constants';

const PULSE = 200;
const ERROR_DELAY = 5 * SECOND;
const LOCK_DELAY = MINUTE;

let lock = MIN_TIME;

async function tasks() {
  await dataService.refresh();
}

function executor() {
  if (Date.now() < lock) return;
  lock = Date.now() + LOCK_DELAY;
  tasks()
    .then(() => {
      lock = MIN_TIME;
    })
    .catch(error => {
      console.warn(error);
      lock = Date.now() + ERROR_DELAY;
    });
}

let intervalId;

function restart() {
  clearInterval(intervalId);
  console.log('start tasks service');
  intervalId = setInterval(executor, PULSE);
}

function stop() {
  console.log('stop tasks service');
  clearInterval(intervalId);
}

export default {
  restart,
  stop,
};
