import { AsyncStorage } from 'react-native';

const cache = {};

async function getValue(key) {
  let val = cache[key];
  if (val === undefined) {
    val = await AsyncStorage.getItem(key);
    if (val === undefined) {
      val = '';
    }
    cache[key] = val;
  }
  return val;
}

async function setValue(key, val) {
  await AsyncStorage.setItem(key, val);
  cache[key] = val;
}

const APP_AUTH_TOKEN = 'AUTH_TOKEN';
const MULTI_LOGIN_TOKEN = 'MULTI_LOGIN_TOKEN';
const LOGIN_PHONES = 'LOGIN_PHONES';
const APP_FCM_TOKEN = 'FCM_TOKEN';
const APP_APNS_TOKEN = 'APNS_TOKEN';

async function getLoginPhones() {
  let val = await getValue(LOGIN_PHONES);
  if (!val || !val.length) return [];
  let phones;
  try {
    phones = JSON.parse(val);
  } catch (error) {}
  return phones || [];
}

async function setLoginPhones(phones) {
  let val = JSON.stringify(phones);
  await setValue(LOGIN_PHONES, val);
}

export default {
  setAuthToken: async val => {
    await setValue(APP_AUTH_TOKEN, val);
  },
  getAuthToken: async () => {
    return await getValue(APP_AUTH_TOKEN);
  },
  setMultiLoginToken: async val => {
    return await setValue(MULTI_LOGIN_TOKEN, val);
  },
  getMultiLoginToken: async () => {
    return await getValue(MULTI_LOGIN_TOKEN);
  },
  addLoginPhone: async ({ phone, title }) => {
    if (!phone || !phone.startsWith('+') || !title) {
      throw new Error('invalid input');
    }
    let phones = await getLoginPhones();
    phones = phones.filter(item => {
      return item.phone != phone;
    });
    phones.push({ phone, title });
    await setLoginPhones(phones);
  },
  removeLoginPhone: async ({ phone }) => {
    if (!phone) {
      throw new Error('invalid input');
    }
    let phones = await getLoginPhones();
    let removed;
    phones = phones.filter(item => {
      if (item.phone != phone) return true;
      removed = true;
      return false;
    });

    if (!removed) {
      throw new Error('phone not found');
    }
    await setLoginPhones(phones);
  },
  clearLoginPhones: async () => {
    await setLoginPhones([]);
  },
  getLoginPhones,
  /*setFCMToken: async val => {
    await setValue(APP_FCM_TOKEN, val);
  },
  getFCMToken: async () => {
    return await getValue(APP_FCM_TOKEN);
  },
  setAPNSToken: async val => {
    await setValue(APP_APNS_TOKEN, val);
  },
  getAPNSToken: async () => {
    return await getValue(APP_APNS_TOKEN);
  },*/
};
