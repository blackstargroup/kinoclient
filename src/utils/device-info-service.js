import { Platform } from 'react-native';
import deviceInfo from 'react-native-device-info';

import { client } from '../store';
import SET_DEVICE_INFO from '../graphql/mutations/setDeviceInfo';

export default {
  sendToServer,
};

async function sendToServer({ fcmToken }) {
  let info = {
    deviceId: deviceInfo.getUniqueID(),
    os: Platform.OS,
    systemVersion: deviceInfo.getSystemVersion(),
    fcmToken,
    locale: deviceInfo.getDeviceLocale(),
    country: deviceInfo.getDeviceCountry(),
    timezone: deviceInfo.getTimezone(),
  };

  try {
    await setDeviceInfo(info);
  } catch (error) {
    console.log("Can't send device info to servier", error);
  }
}

async function setDeviceInfo(variables) {
  let { data } = await client.mutate({
    mutation: SET_DEVICE_INFO,
    variables,
  });

  return data.setDeviceInfo;
}
