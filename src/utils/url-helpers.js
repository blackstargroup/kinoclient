export function makeFullUrl(file) {
  if (!file) return null;
  let { server, path } = file;

  if (!server || !path) return null;

  return `${server}/${path}`;
}
