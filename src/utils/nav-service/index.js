/********************************
 * https://reactnavigation.org/docs/en/navigating-without-navigation-prop.html
 *******************************/

import { NavigationActions } from 'react-navigation';
import dataService from '../data-service';

import {
  PROBE_AS_ACTOR,
  PROBE_AS_DIRECTOR,
  PROJ_CARD,
  ROLE_CARD,
  PROBE_DRAFT,
  PROJ_DRAFT_CARD,
  ROLE_DRAFT_EDIT,
  PROJ_DRAFT_EDIT,
  NOTIFICATIONS,
} from './constants';

export default {
  navigate,
  navigateToNotifications,
  navigateToProbeAsActor,
  navigateToProbeAsDirector,
  setTopLevelNavigator,
  onNavigationStateChange,
  getActiveRoute,
  routeEqualToActive,
  routesEqual,
};

let _navigator;

const initialRoute = { routeName: '', params: {} };
let _route = initialRoute;

function setTopLevelNavigator(ref) {
  _navigator = ref;
}

function onRouteChanged(route) {
  const { routeName, params } = route;
  console.log(`navservice.onroutechanged ${routeName}`, params);

  try {
    switch (routeName) {
      case PROBE_AS_ACTOR:
      case PROBE_AS_DIRECTOR:
        // don't wait result
        dataService.visitProbe(route);
        break;
      case PROJ_CARD: {
        const { projId } = route.params;
        dataService.dispatchProj({ projId });
        break;
      }
      case ROLE_CARD: {
        const { projId, roleId } = route.params;
        dataService.dispatchProj({ projId, roleId });
        break;
      }
      case PROBE_DRAFT: {
        const { projId, roleId } = route.params;
        dataService.dispatchProbeDraft({ projId, roleId });
        break;
      }
      case PROJ_DRAFT_CARD: {
        let { projDraftId } = route.params;
        dataService.dispatchProjDraft({ projDraftId });
        break;
      }
      case PROJ_DRAFT_EDIT: {
        let { projDraftId } = route.params;
        dataService.dispatchProjDraft({ projDraftId });
        break;
      }
      case ROLE_DRAFT_EDIT: {
        let { projDraftId, roleDraftId } = route.params;
        dataService.dispatchProjDraft({ projDraftId, roleDraftId });
        break;
      }
      default:
        break;
    }
  } catch (error) {
    console.log(`error on navservice.onroutechanged ${routeName}`, error);
  }
}

function routeEqualToActive(route) {
  return routesEqual(_route, route);
}

function routesEqual(route1, route2) {
  // todo: need to fix that
  //if routes are not exactly the same, we return false

  if (route1.routeName !== route2.routeName) return false;
  const a1 = Object.entries(route1.params);
  const a2 = Object.entries(route2.params);
  if (a1.length !== a2.length) return false;
  const len = a1.length;
  for (let i = 0; i < len; ++i) {
    const p1 = a1[i];
    const p2 = a2[i];
    if (p1[0] !== p2[0] || p1[1] !== p2[1]) {
      return false;
    }
  }
  return true;
}

function resetRouteObj({ routeName, params }) {
  const prev = _route;
  _route = { routeName, params: params || {} };

  if (!routeEqualToActive(prev)) {
    onRouteChanged(_route);
  }
}

function onNavigationStateChange(prevState, newState) {
  if (!newState.routes) {
    resetRouteObj({});
    return;
  }
  let val = newState.routes[newState.index];
  if (!val.routes) {
    resetRouteObj(val);
    return;
  }
  val = val.routes[val.index];
  resetRouteObj(val);
}

function getActiveRoute() {
  // actually we don't need call navigationStateChange
  // we can just get get current route from _navigator.state
  // but i think it is reset route obj onNavigationStateChage, and keep it clear
  return _route;
}

function navigate(routeName, params) {
  if (!_navigator) {
    return;
  }

  try {
    _navigator.dispatch(
      NavigationActions.navigate({
        routeName,
        params,
      }),
    );
  } catch (error) {
    console.warn(`Can't navigate to ${routeName} screen`);
    throw error;
  }
}

function navigateToNotifications() {
  navigate(NOTIFICATIONS);
}

function navigateToProbeAsActor({ probeId }) {
  navigate(PROBE_AS_ACTOR, { probeId });
}

function navigateToProbeAsDirector({ probeId }) {
  navigate(PROBE_AS_DIRECTOR, { probeId });
}
