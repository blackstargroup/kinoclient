import { Alert } from 'react-native';

export async function alertOk(title, text) {
  return new Promise((resolve, reject) => {
    Alert.alert(title, text, [
      {
        text: 'OK',
        onPress: resolve,
      },
    ]);
  });
}
