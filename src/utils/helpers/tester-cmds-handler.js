import React from 'react';
import deviceStorage from '../device-storage';
import store from '../../store';
import {
  commonSetMultiLoginToken,
  commonSetLoginPhones,
} from '../../actions/common';
import reportService from '../report-service';
import db from '../db-service';
import { alertOk } from './alerts';

function showCmdDoneAlert(msg) {
  alertOk('Done 😜', msg);
}

function showCmdFailAlert(error) {
  alertOk('Error 💩', `${error}`);
}

export async function handleTesterCommand(str) {
  if (!str || !str.length) return;
  let args = str.trim().split('$');
  let cmd = args[1].toLowerCase();

  switch (cmd) {
    case 'setmultilogintoken':
      try {
        await reportService.sendCustomReport({
          kind: 'cmd',
          title: 'SET MULTI LOGIN TOKEN',
          desc: '***',
        });
        let multiLoginToken = args[2];
        await deviceStorage.setMultiLoginToken(multiLoginToken);
        store.dispatch(commonSetMultiLoginToken({ multiLoginToken }));
        showCmdDoneAlert(`token='${multiLoginToken}'`);
      } catch (error) {
        showCmdFailAlert(error);
      }
      return true;
    case 'addloginphone':
      try {
        if (args.length !== 4) {
          throw new Error('Invalid arguments count');
        }
        let phone = args[2];
        let title = args[3];
        if (!phone || !phone.startsWith('+') || !title) {
          throw new Error('Invalid input');
        }
        await reportService.sendCustomReport({
          kind: 'cmd',
          title: 'ADD LOGIN PHONE',
          desc: `phone ${phone} title ${title}`,
        });
        await deviceStorage.addLoginPhone({ phone, title });
        let loginPhones = await deviceStorage.getLoginPhones();
        store.dispatch(commonSetLoginPhones({ loginPhones }));
        let loginPhonesStr = loginPhones
          .map(x => `phone='${x.phone}' title='${x.title}'`)
          .join('\n');
        showCmdDoneAlert(getLoginPhonesMsgString(loginPhones));
      } catch (error) {
        showCmdFailAlert(error);
      }
      return true;
    case 'removeloginphone':
      let phone = args[2];
      try {
        await reportService.sendCustomReport({
          kind: 'cmd',
          title: 'REMOVE LOGIN PHONE',
          desc: `${phone}`,
        });
        await deviceStorage.removeLoginPhone({ phone });
        let loginPhones = await deviceStorage.getLoginPhones();
        store.dispatch(commonSetLoginPhones({ loginPhones }));
        showCmdDoneAlert(getLoginPhonesMsgString(loginPhones));
      } catch (error) {
        showCmdFailAlert(error);
      }
      return true;
    case 'clearloginphones':
      try {
        await reportService.sendCustomReport({
          kind: 'cmd',
          title: 'CLEAR LOGIN PHONES',
          desc: '',
        });
        await deviceStorage.clearLoginPhones();
        let loginPhones = await deviceStorage.getLoginPhones();
        store.dispatch(commonSetLoginPhones({ loginPhones }));
        showCmdDoneAlert(getLoginPhonesMsgString(loginPhones));
      } catch (error) {
        showCmdFailAlert(error);
      }
      return true;

    case 'settester':
      try {
        let loginPhone = args[2];
        let value = JSON.parse(args[3].toLowerCase()); // bool
        await reportService.sendCustomReport({
          kind: 'cmd',
          title: 'SET TESTER ',
          desc: `phone ${loginPhone} isTester=${value}`,
        });
        await db.setTester({ loginPhone, value });
        showCmdDoneAlert(`Set tester ${loginPhone} value=${value} done`);
      } catch (error) {
        showCmdFailAlert(error);
      }
      return true;

    default:
      showCmdFailAlert('Unknown command');
      break;
  }
}

function getLoginPhonesMsgString(loginPhones) {
  let loginPhonesStr = loginPhones
    .map(({ phone, title }) => `phone='${phone}' title='${title}'`)
    .join('\n');
  return `Login phones:\n${loginPhonesStr}`;
}
