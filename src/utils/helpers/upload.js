import deviceStorage from '../device-storage';

export default async function upload(url, options) {
  const { body, headers, progressCallback } = options;
  const token = await deviceStorage.getAuthToken();

  console.log(`try upload file ${url}`);

  return new Promise(async (resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open('post', url);
    xhr.setRequestHeader('authorization', token || '');
    for (var headerName in headers || {}) {
      xhr.setRequestHeader(headerName, headers[headerName]);
    }

    xhr.onload = e => {
      const { status, _response } = e.target;
      if (status === 200 || status === 201) {
        resolve(JSON.parse(_response));
      } else {
        reject(`post req ${url} status=${status}`);
      }
    };

    xhr.onerror = () => reject(`post req ${url} error`);
    xhr.onabort = () => reject(`post req ${url} aborted`);

    if (xhr.upload && progressCallback) {
      xhr.upload.onprogress = ({ loaded, total }) => {
        let progress = ((loaded / total) * 100).toFixed(0);
        progressCallback(progress);
      };
    }
    xhr.send(body);
  });
}
