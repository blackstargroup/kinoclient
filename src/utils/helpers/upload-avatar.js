import uploadFiles from './upload-files';

import store from '../../store';
import {
  uploadingStart,
  setUploadingProgress,
  setUploadingError,
  uploadingFinish,
} from '../../actions/avatarUploadStatus';
import dataService from '../data-service';
import db from '../db-service';
import { sleep } from '../helpers/sleep';
import {
  DEV_SLEEP,
  uploadKinds,
  AVATAR_SIZE,
  COMPRESS_AVATAR_QUALITY,
} from '../constants';
import reportService from '../report-service';
import urlExists from '../helpers/urlExists';
import { pickAvatar } from '../pickers';

export default async function uploadAvatar() {
  let photo = await pickAvatar();
  if (photo) {
    await tryUploadPhoto(photo);
  }
}

async function tryUploadPhoto({ path, mime, width, height, size }) {
  try {
    store.dispatch(uploadingStart({ status: '🤔 Подготовка' }));

    __DEV__ && (await sleep(DEV_SLEEP));

    let { serverName, uploadUrl } = await db.getUploadServer();
    let uploadResult = await uploadFiles({
      uploadUrl,
      kind: uploadKinds.AVATAR,
      files: [
        {
          path,
          size,
          mime,
          width,
          height,
        },
      ],
      progressCallback: ({ progress, status }) => {
        store.dispatch(setUploadingProgress({ progress, status }));
        console.log(progress);
      },
    });

    let fileId = uploadResult[path];

    __DEV__ && (await sleep(DEV_SLEEP));

    store.dispatch(
      setUploadingProgress({ progress: 0, status: '🤔 Обновление профиля' }),
    );

    let me = await dataService.setAvatarFile({ serverName, fileId });
    let avaExists = await urlExists(me.avatar.path);
    if (!avaExists) {
      throw new Error(`url not available ${me.avatar.path}`);
    }

    __DEV__ && (await sleep(DEV_SLEEP));

    store.dispatch(uploadingFinish());
  } catch (error) {
    reportService.sendErrorReportSafety({
      title: 'upload avatar failed',
      error,
    });

    store.dispatch(
      setUploadingError({
        errorText: 'Ошибка загрузки 😔',
        errorSubText: 'Попробуйте повторить позже',
      }),
    );
  }
}
