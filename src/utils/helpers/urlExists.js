export default async function urlExists(url) {
  return new Promise((resolve, reject) => {
    if (!url) {
      reject('urlExists: url is empty');
      return;
    }
    try {
      const xhr = new XMLHttpRequest();
      xhr.open('HEAD', url, true);
      //xhr.onreadystatechange = () => {};
      xhr.onload = () => resolve(xhr.status === 200);
      xhr.onerror = () => resolve(false);
      xhr.onabort = () => resolve(false);
      xhr.send();
    } catch (error) {
      reject(`Can't make head req ${url}: ${error}`);
    }
  });
}
