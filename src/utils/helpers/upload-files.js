import upload from './upload';

export default async function uploadFiles({
  uploadUrl,
  kind,
  files,
  progressCallback,
}) {
  let count = files.length;
  let status = count > 1 ? '🤔 Загрузка файлов' : '🤔 Загрузка файла';
  let totalSize = files.reduce((sum, { size }) => {
    return sum + size;
  }, 0);
  let loadedSize = 0;

  let res = {};
  for (let i = 0; i < count; ++i) {
    let file = files[i];
    let { path, mime, width, height, size } = file;

    let body = new FormData();
    body.append('file', {
      uri: path.replace('file://', ''),
      type: mime,
      name: 'name',
    });

    body.append('kind', kind);
    body.append('width', width);
    body.append('height', height);

    let {
      success,
      data: { fileId },
    } = await upload(uploadUrl, {
      body,
      progressCallback: progress => {
        let totalProgress = (
          (100 * (loadedSize + (size * progress) / 100)) /
          totalSize
        ).toFixed(0);
        progressCallback({ progress: totalProgress, status });
      },
    });

    if (!success || !fileId) {
      throw new Error('Error happens on upload');
    }

    loadedSize += size;
    res[path] = fileId;
  }
  return res;
}
