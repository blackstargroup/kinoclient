import AccountKit, {
  Color,
  StatusBarStyle,
} from 'react-native-facebook-account-kit';

import { colors } from '../theme';

AccountKit.configure({
  theme: {
    backgroundColor: Color.hex(colors.NAV_BAR_COLOR),
    buttonBackgroundColor: Color.hex(colors.BTN_DEFAULT_BG),
    buttonBorderColor: Color.hex(colors.BTN_DEFAULT_BG),
    buttonTextColor: Color.hex('#fff'),
    buttonDisabledBackgroundColor: Color.rgba(32, 186, 228, 0.5),
    buttonDisabledBorderColor: Color.rgba(32, 186, 228, 0.5),
    buttonDisabledTextColor: Color.hex('#fff'),
    buttonBorderRadius: 0,
    headerBackgroundColor: Color.hex(colors.NAV_BAR_COLOR),
    headerTextColor: Color.hex('#fff'),
    headerButtonTextColor: Color.hex('#fff'),
    //iconColor: Color.rgba(0, 255, 0, 1),
    //inputBackgroundColor: Color.rgba(0, 255, 0, 1),
    inputBorderColor: Color.hex(colors.NAV_BAR_COLOR),
    inputTextColor: Color.hex(colors.CONTENT_TEXT_COLOR),
    textColor: Color.hex('#fff'),
    titleColor: Color.hex('#fff'),
    //backgroundImage: "background.png",
    //statusBarStyle: StatusBarStyle.LightContent,
  },
  //countryWhitelist: ['BY', 'RU', 'UA'],
  //countryBlacklist: [ "BR" ],
  defaultCountry: 'BY',
});

export default class FacebookAccountHelper {
  static async getToken() {
    const accountKitData = await AccountKit.getCurrentAccessToken();
    return accountKitData.token;
  }

  static logout() {
    return AccountKit.logout();
  }
}
