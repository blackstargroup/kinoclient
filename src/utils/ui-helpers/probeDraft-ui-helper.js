import React from 'react';
import { Alert } from 'react-native';

import { MAX_PROBE_ATTACHMENTS } from '../constants';

import dataService from '../data-service';
import { pickProbeAttachment } from '../pickers';
import { alertOk } from '../helpers/alerts';

const USERS_INITIAL_STATE = {
  userId: null,
  firstName: '',
  lastName: '',
};

let userStateMemory = {
  ...USERS_INITIAL_STATE,
};

const DRAFT_INITIAL_STATE = {
  roleId: null,
  text: '',
  attachments: [],
};

let draftStatesMemory = {};

let comp;

function saveUserState() {
  let { userId, firstName, lastName } = comp.state;
  userStateMemory = { userId, firstName, lastName };
}

function saveProbeState() {
  let { roleId, text, attachments } = comp.state;
  draftStatesMemory[roleId] = { roleId, text, attachments };
}

export default {
  componentInit: self => {
    comp = self;
    comp.state = { ...USERS_INITIAL_STATE, ...DRAFT_INITIAL_STATE };
  },
  componentWillUnmount: () => {
    saveUserState();
    saveProbeState();
  },
  getDerivedStateFromProps: (props, state) => {
    let { me, probeDraft } = props;

    let stateChanged = false;
    let newState = {};

    let { _id: userId, firstName, lastName } = me;

    if (userStateMemory.userId !== userId) {
      userStateMemory = { ...USERS_INITIAL_STATE, userId };
    }

    if (state.userId !== userId) {
      firstName = firstName || userStateMemory.firstName || '';
      lastName = lastName || userStateMemory.lastName || '';
      newState = { userId, firstName, lastName };
      stateChanged = true;
    }

    let { roleId } = probeDraft;
    if (state.roleId !== roleId) {
      let probeState = draftStatesMemory[roleId] || {
        ...DRAFT_INITIAL_STATE,
        roleId,
      };
      newState = { ...newState, ...probeState };
      stateChanged = true;
    }

    if (stateChanged) return newState;
    return null;
  },
  trySubmitAboutMeForm,
  setText: text => {
    dataService.probeDraftSetProp('text', text);
  },
  trySend,
  getForm: () => {
    return dataService.probeDraftGetForm();
  },
  addFile,
  removeFile: ({ path }) => {
    let attachments = comp.state.attachments.filter(x => x.file.path !== path);
    comp.setState({ attachments });
  },
};

async function trySubmitAboutMeForm({ firstName, lastName }) {
  if (!firstName || !lastName) {
    Alert.alert(
      'Пустые поля формы',
      `Необходимо заполнить основные поля, чтобы продолжить`,
      [
        {
          text: 'Ок',
        },
      ],
    );
    return;
  }

  try {
    await dataService.updateMe({
      firstName,
      lastName,
    });
  } catch (error) {
    Alert.alert('Ошибка сохранения', '', [
      {
        text: 'Ок',
      },
    ]);
    console.log(error);
  }
}

async function addFile() {
  let attachments = comp.state.attachments.slice();
  if (attachments.length >= MAX_PROBE_ATTACHMENTS) {
    Alert.alert(
      'Превышен лимит',
      `Можно прикрепить максимум ${MAX_PROBE_ATTACHMENTS} файлов`,
      [
        {
          text: 'Ок',
          onPress: () => {},
        },
      ],
    );
    return;
  }

  let { file, thumbFile } = await pickProbeAttachment();
  if (file && file.path) {
    attachments.push({ file, thumbFile });
    comp.setState({ attachments });
  }
}

async function trySend() {
  let { text, attachments } = comp.state;
  let { probeDraft } = comp.props;
  let { projId, roleId } = probeDraft;
  let proj;

  try {
    proj = await dataService.forceRefetchProj(projId);
  } catch (error) {
    await alertOk('Ошибка', 'Проверьте соединение или попробуйте позже');
    console.log(error);
    return;
  }

  if (proj.closed) {
    await alertOk(
      'Кастинг завершен',
      'Приложение попытается отправить заявку, однако сервер может ее не принять',
    );
  }

  if (!attachments.length) {
    await alertOk('Отправка заявки', 'Вы забыли прикрепить файлы');
    return;
  }

  Alert.alert('Отправка заявки', 'Вы готовы отправить заявку?', [
    {
      text: 'Нет',
      onPress: () => {},
    },
    {
      text: 'Да',
      onPress: async () => {
        await dataService.createProbe({
          text,
          attachments,
        });
        comp.setState({ ...DRAFT_INITIAL_STATE, roleId });
      },
    },
  ]);
}
