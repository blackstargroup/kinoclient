import React from 'react';
import { Alert } from 'react-native';
import navService from '../nav-service';
import dataService from '../data-service';
import store from '../../store';

const ROLE_DRAFT_INITIAL_STATE = {
  _id: null,
  projDraftId: null,
  title: '',
  shortDesc: '',
  desc: '',
  homework: '',
};

let comp;

export default {
  componentInit: self => {
    comp = self;
    comp.state = {
      ...ROLE_DRAFT_INITIAL_STATE,
    };
  },
  componentDidMount: ({ projDraft, roleDraft }) => {
    resetState({ projDraft, roleDraft });
  },
  componentDidUpdate: ({ prevProps, prevState }) => {
    let { roleDraft } = comp.props;
    if (comp.state._id === roleDraft._id) return;
    resetState({ roleDraft });
  },
  goBack,
  trySave: async () => {
    if (!isFormAffected()) {
      goBack();
      return;
    }

    const { me, projDraftCard } = store.getState();
    const { projDraft } = projDraftCard;

    if (me.isModer) {
      Alert.alert(
        'Сохранить изменения в черновике?',
        'Вы, как модератор, внесли изменения в черновик',
        [
          {
            text: 'Нет',
            onPress: () => {
              comp.setState(ROLE_DRAFT_INITIAL_STATE);
              goBack();
            },
          },
          {
            text: 'Да',
            onPress: () => {
              save();
            },
          },
        ],
      );
      return;
    }

    if (projDraft.status === projDraftStatuses.APPROVED) {
      Alert.alert(
        'Точно сохранить изменения?',
        'Модератор одобрил предыдущий черновик, но вы внесли новые изменения. Сохранив Вам придется отправить черновик модератору заново',
        [
          {
            text: 'Нет',
            onPress: () => {
              comp.setState(ROLE_DRAFT_INITIAL_STATE);
              goBack();
            },
          },
          {
            text: 'Да',
            onPress: () => {
              save();
            },
          },
        ],
      );
    } else {
      save();
    }
  },
};

function resetState({ roleDraft }) {
  let props = {};
  let entries = Object.entries(ROLE_DRAFT_INITIAL_STATE);
  entries.forEach(([key, defaultValue]) => {
    props[key] = roleDraft[key];
  });
  comp.setState(props);
}

function goBack() {
  let { projDraftCard } = store.getState();
  let { projDraft } = projDraftCard;
  let projDraftId = projDraft._id;
  if (projDraftId) {
    navService.navigate('ProjDraftEdit', { projDraftId });
  } else {
    navService.navigate('Projs');
  }
}

function isFormAffected() {
  const { projDraftCard } = store.getState();
  const { roleDraft } = projDraftCard;

  let affected = false;
  let entries = Object.entries(ROLE_DRAFT_INITIAL_STATE);

  entries.forEach(([key, defaultValue]) => {
    let value = comp.state[key];
    if (roleDraft[key] != value) {
      affected = true;
    }
  });

  return affected;
}

async function save() {
  let { _id, projDraftId, title, shortDesc, desc, homework } = comp.state;
  await dataService.updateRoleDraft({
    _id,
    projDraftId,
    title,
    shortDesc,
    desc,
    homework,
  });
  comp.setState(ROLE_DRAFT_INITIAL_STATE);
  goBack();
}
