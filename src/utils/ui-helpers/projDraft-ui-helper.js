import React from 'react';
import { Alert } from 'react-native';

import navService from '../nav-service';
import dataService from '../data-service';
import { projDraftStatuses } from '../constants';

export default {
  tryDelete: ({ projDraft }) => {
    Alert.alert(
      'Точно удалить черновик?',
      'После удаления черновика вы не сможете его восстановить, ' +
        ' и Вам придется создавать кастинг заново.',
      [
        {
          text: 'Нет',
          onPress: () => {},
        },
        {
          text: 'Да',
          onPress: async () => {
            await dataService.deleteProjDraft(projDraft._id);
            navService.navigate('Projs');
          },
        },
      ],
    );
  },
  tryClose: ({ proj }) => {
    Alert.alert(
      'Точно завершить кастинг?',
      'Действие является необратимым. После закрытия кастинга, пользователи еще в течении несокльких минут могут отправлять пробы',
      [
        {
          text: 'Нет',
          onPress: () => {},
        },
        {
          text: 'Да',
          onPress: async () => {
            await dataService.closeProj(proj._id);
            navService.navigate('Projs');
          },
        },
      ],
    );
  },
  tryPromoteToAffected: ({ projDraft }) => {
    Alert.alert(
      'Вернуться к редактированию?',
      'Вы подтверждаете, что желаете вернуться к редактированию',
      [
        {
          text: 'Нет',
          onPress: () => {},
        },
        {
          text: 'Да',
          onPress: () =>
            promote({ projDraft, status: projDraftStatuses.AFFECTED }),
        },
      ],
    );
  },

  tryPromoteToModerating: ({ projDraft }) => {
    Alert.alert(
      'Вы внесли все изменения?',
      'Черновик будет отправлен модератору',
      [
        {
          text: 'Нет',
          onPress: () => {},
        },
        {
          text: 'Да',
          onPress: () =>
            promote({ projDraft, status: projDraftStatuses.MODERATING }),
        },
      ],
    );
  },

  tryPromoteToApproved: ({ projDraft }) => {
    Alert.alert(
      'Точно одобрить?',
      'Вы подтверждаете, что проект готов для публикации',
      [
        {
          text: 'Нет',
          onPress: () => {},
        },
        {
          text: 'Да',
          onPress: () =>
            promote({ projDraft, status: projDraftStatuses.APPROVED }),
        },
      ],
    );
  },
  tryPromoteToDenied: ({ projDraft }) => {
    if (!projDraft.moderComment) {
      Alert.alert(
        'Отсутствует комментарий',
        'Откройте черновик и укажите причину отказа, и что необходимо исправить',
      );
      return;
    }

    Alert.alert(
      'Точно отклонить?',
      'Вы подтвержаете, что в проект нужно внести доработки',
      [
        {
          text: 'Нет',
          onPress: () => {},
        },
        {
          text: 'Да',
          onPress: () =>
            promote({ projDraft, status: projDraftStatuses.DENIED }),
        },
      ],
    );
  },
  tryPromoteToPublished: ({ projDraft }) => {
    Alert.alert(
      'Точно опубликовать?',
      'После публикации все пользователи приложения увидят Ваш кастинг.',
      [
        {
          text: 'Нет',
          onPress: () => {},
        },
        {
          text: 'Да',
          onPress: () =>
            promote({ projDraft, status: projDraftStatuses.PUBLISHED }),
        },
      ],
    );
  },
};

async function promote({ projDraft, status }) {
  const { _id } = projDraft;
  await dataService.promoteProjDraft({
    _id,
    status,
  });
}
