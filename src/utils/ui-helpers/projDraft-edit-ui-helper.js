import React from 'react';
import { Alert } from 'react-native';

import navService from '../nav-service';
import dataService from '../data-service';
import { projDraftStatuses } from '../constants';
import store from '../../store';

const DRAFT_INITIAL_STATE = {
  _id: null,
  title: '',
  desc: '',
  country: '',
  city: '',
  dirComment: '',
  moderComment: '',
};

let comp;

export default {
  componentInit: self => {
    comp = self;
    comp.state = {
      ...DRAFT_INITIAL_STATE,
    };
  },
  componentDidMount: ({ projDraft }) => {
    resetState({ projDraft });
  },
  goBack,
  tryCreateRoleDraft: projDraftId => {
    Alert.alert('Точно добавить роль?', 'Будет создана пустая роль', [
      {
        text: 'Нет',
        onPress: () => {},
      },
      {
        text: 'Да',
        onPress: async () => {
          await dataService.createRoleDraft({ projDraftId, title: '' });
        },
      },
    ]);
  },
  tryDeleteRoleDraft: ({ projDraftId, roleDraftId, title }) => {
    Alert.alert(`Точно удалить роль?`, `Роль ${title} будет удалена`, [
      {
        text: 'Нет',
        onPress: () => {},
      },
      {
        text: 'Да',
        onPress: async () => {
          await dataService.deleteRoleDraft({ projDraftId, roleDraftId });
        },
      },
    ]);
  },
  trySave: async () => {
    if (!isFormAffected()) {
      goBack();
      return;
    }

    let { me, projDraftCard } = store.getState();
    let { projDraft } = projDraftCard;

    if (me.isModer) {
      Alert.alert(
        'Сохранить изменения в черновике?',
        'Вы, как модератор, внесли изменения в черновик',
        [
          {
            text: 'Нет',
            onPress: () => {
              comp.setState(DRAFT_INITIAL_STATE);
              goBack();
            },
          },
          {
            text: 'Да',
            onPress: () => {
              save();
            },
          },
        ],
      );
      return;
    }

    if (projDraft.status === projDraftStatuses.APPROVED) {
      Alert.alert(
        'Точно сохранить изменения?',
        'Модератор одобрил предыдущий черновик, но вы внесли новые изменения. Сохранив Вам придется отправить черновик модератору заново',
        [
          {
            text: 'Нет',
            onPress: () => {
              comp.setState(DRAFT_INITIAL_STATE);
              goBack();
            },
          },
          {
            text: 'Да',
            onPress: () => {
              save();
            },
          },
        ],
      );
    } else {
      save();
    }
  },
};

function resetState({ projDraft }) {
  let props = {};
  let entries = Object.entries(DRAFT_INITIAL_STATE);
  entries.forEach(([key, defaultValue]) => {
    props[key] = projDraft[key];
  });
  comp.setState(props);
}

function goBack() {
  let { projDraftCard } = store.getState();
  let { projDraft } = projDraftCard;
  let { _id: projDraftId } = projDraft;
  if (projDraftId) {
    navService.navigate('ProjDraftCard', { projDraftId });
  } else {
    navService.navigate('Projs');
  }
}

async function save() {
  let {
    _id,
    title,
    desc,
    country,
    city,
    dirComment,
    moderComment,
  } = comp.state;
  await dataService.updateProjDraft({
    _id,
    title,
    desc,
    country,
    city,
    dirComment,
    moderComment,
  });
  comp.setState(DRAFT_INITIAL_STATE);
  goBack();
}

function isFormAffected() {
  let { projDraftCard } = store.getState();
  let { projDraft } = projDraftCard;

  let affected = false;

  let entries = Object.entries(DRAFT_INITIAL_STATE);
  entries.forEach(([key, defaultValue]) => {
    let value = comp.state[key];
    if (projDraft[key] != value) {
      affected = true;
      console.log(`Affected ${key} prev=${projDraft[key]} curr=${value}`);
    }
  });

  return affected;
}
