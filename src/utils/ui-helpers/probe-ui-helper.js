import React from 'react';
import { Alert } from 'react-native';

import store from '../../store';
import dataService from '../data-service';
import { pickProbeAttachment } from '../pickers';
import { videoPlayerSet } from '../../actions/videoPlayer';
import navService from '../nav-service';

export default {
  sendMsg,
  trySendAttachment,
  scrollList,
  showVideo,
  showFlagMenu,
  tryDeleteProbe,
};

async function tryDeleteProbe({ probeId }) {
  let msg = 'После удаления, вы не сможете просмотреть пробу';
  Alert.alert('Точно удалить?', msg, [
    {
      text: 'Нет',
      onPress: () => {},
    },
    {
      text: 'Да',
      onPress: async () => {
        await dataService.deleteProbe(probeId);
        navService.navigateToNotifications();
      },
    },
  ]);
}

function showVideo({ path }) {
  store.dispatch(videoPlayerSet({ path }));
}

function showFlagMenu({ probeId }) {
  Alert.alert('Действия', ``, [
    {
      text: 'Удалить пробу',
      onPress: () => tryDeleteProbe({ probeId }),
    },
    {
      text: 'Отмена',
      onPress: () => {},
    },
  ]);
}

async function sendMsg(text) {
  let { probe } = store.getState().chat;
  let probeId = probe._id;
  try {
    await dataService.probeSendText({ probeId, text });
  } catch (error) {
    Alert.alert(
      'Ошибка',
      `Не удалось отправить сообщеие, проверьте соединение`,
      [
        {
          text: 'Ок',
        },
      ],
    );
    console.log('cant send msg', error);
  }
}

async function trySendAttachment(text) {
  let { probe } = store.getState().chat;
  let probeId = probe._id;
  let attachment = await pickProbeAttachment();
  let { file, fileThumb } = attachment;
  if (!file || !file.path) return;

  Alert.alert('Точно отправить?', '', [
    {
      text: 'Нет',
    },
    {
      text: 'Да',
      onPress: () => {
        dataService.probeSendAttachment({ probeId, attachments: [attachment] });
      },
    },
  ]);
}

async function scrollList(list) {
  let doScroll = () => {
    if (!list || !list.scrollToEnd) return;
    list.scrollToEnd({ animated: false });
  };

  setTimeout(doScroll, 0);
  setTimeout(doScroll, 500);
  setTimeout(doScroll, 1000);
  setTimeout(doScroll, 2000);
}
