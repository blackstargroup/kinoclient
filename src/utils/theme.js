import { Platform } from 'react-native';

export const colors = {
  BORDER_SEPARATOR_COLOR: 'grey',
  NAV_BAR_COLOR: '#6b7683', //'#90A4AE',
  SAFE_VIEW_COLOR: '#6b7683' || '#1F3138',
  CONTENT_BG: '#F5F5F5',
  CONTENT_ICON_COLOR: '#999',
  CONTENT_TEXT_COLOR: '#1B1B1B',
  CONTENT_LINK_COLOR: 'blue',
  TEXTBOX_HINT_COLOR: '#999',
  TEXTBOX_COLOR: '#1B1B1B',
  GREY_TEXT_COLOR: '#999',
  MSG_MY_BG: '#20BAE4',
  MSG_MY_COLOR: 'white',
  MSG_ALT_BG: 'white' || '#E6E6E6',
  MSG_ALT_COLOR: '#1B1B1B',
  MSG_TEXTAREA_BG: '#E6E6E6',
  ADD_RM_IMAGE_BTN_TEXT: '#919BA7',
  NAV_ACTIVE_BTN: 'white',
  NAV_INACTIVE_BTN: '#919BA7',
  NAV_FIRST_TEXT_COLOR: 'white',
  NAV_SECOND_TEXT_COLOR: '#919BA7',
  NAV_BAR_BORDER: 'black',
  BTN_BG: '#999',
  BTN_DEFAULT_BG: '#20BAE4',
  BTN_DEFAULT_TEXT_COLOR: 'white',
  TRANSPARENT: 'transparent',
  WHITE: '#FFFFFF',
  GOLD: '#fcb900',
  ORANGE: 'orange',
  RED: 'red',
  GREEN: 'green',
};

export const sizes = {
  TAB_NAV_HEIGHT: 50,
  CHAT_MSGS_FOOTER_OFFSET: 20,
  CHAT_MSGS_WITH_KEYBOARD_FOOTER_OFFSET: 90,
  CONTAINER_PADDING: 10,
  BORDER_SEPARATOR_WIDTH: 0.5,
  CONTENT_HORIZONTAL_PADDING: 12,
  CONTENT_TOP_PADDING: 20,
};

export const rainbow = {
  one: '#ff6900',
  two: '#fcb900',
  three: '#00d084',
  four: '#DBDF00',
  five: '#0693e3',
  six: '#eb144c',
  seven: '#9900ef',
};

export const shadow = {
  shadowColor: '#000',
  shadowOffset: { width: 3, height: 3 },
  shadowOpacity: 0.5,
  shadowRadius: 5,
};

export const shadowInner = {
  shadowColor: '#000',
  shadowOffset: {
    width: 0,
    height: 1,
  },
  shadowRadius: 3,
  shadowOpacity: 1,
};

export const shadowText = {
  textShadowColor: 'grey',
  textShadowOffset: { width: -1, height: 1 },
  textShadowRadius: 10,
};

export const baseStyles = {
  container: { flex: 1 },
  content: {
    flex: 1,
    paddingHorizontal: sizes.CONTENT_HORIZONTAL_PADDING,
    paddingTop: sizes.CONTENT_TOP_PADDING,
  },
  scrollViewContent: {
    flex: 1,
    paddingHorizontal: sizes.CONTENT_HORIZONTAL_PADDING,
    paddingTop: sizes.CONTENT_TOP_PADDING,
    paddingBottom: 24,
  },
  textH1: {
    fontWeight: 'bold',
    fontSize: 24,
    marginBottom: 12,
  },
  textH2: {
    fontWeight: 'bold',
    fontSize: 16,
    marginBottom: 8,
  },
  textParagraph: {
    color: colors.CONTENT_TEXT_COLOR,
  },
  textbox: {
    padding: 10,
    minHeight: 32,
    backgroundColor: 'white',
    marginBottom: 8,
    color: colors.TEXTBOX_COLOR,
    textAlignVertical: 'top',
  },
  textboxHint: {
    color: colors.TEXTBOX_HINT_COLOR,
    marginBottom: 4,
  },
  btnsContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 24,
  },
};
