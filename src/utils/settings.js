import { Platform } from 'react-native';

const isAndroid = Platform.OS === 'android';

let host = 'http://popshop.by:3000';

if (__DEV__) {
  // normal avd localhost 10.0.2.2, genymotion's localhost is 10.0.3.2
  host = isAndroid ? 'http://10.0.2.2:3000' : 'http://localhost:3000';
}

export default {
  graphqlUrl: `${host}/graphql`,
};
