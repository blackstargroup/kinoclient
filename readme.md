-----------------------------------------------------------
CODE RULES:
-----------------------------------------------------------
use 'yarn prettier' (npm run pritter) before each commit
use let inside inner functions instead of using const
use const when you really need constants

-----------------------------------------------------------
COMPILE IOS PROJ:
-----------------------------------------------------------
clear node modules catalog if need
rm -rf ./node_modules
yarn install
cd ./ios && pod install && cd ..

open  workspace using XCode and run Proj

XCode proj build errors and fixes:
XCode error: '... third-party/glog-3.4.0 ....' or something like that
fix:
1 execute 'yarn glogconfig' command
2 run Proj again

XCode error: RCTWebSocket '...WebSocket/libfishhook.a' ...
fix: 
1 go to KinoClient proj
2 select Libraries folder
3 select RCTWebSocket.xcodeproj
4 select Build Phases tab
5 expand Link Binary With Libraries
6 remove libfishhook.a
7 add libfishhook.a again
8 run Proj again

-----------------------------------------------------------
FAST LOGIN
-----------------------------------------------------------
login to the app by mobile phone as Moderator
go to Настройки -> Написать разработчикам
type: $clearloginphones
press Send
type: $setmultilogintoken$**** where **** is multilogin password that stored in db->props
then type following commands and press send after each command:
$addloginphone$+375291906999$Женя
$addloginphone$+375445462047$Вова
$addloginphone$+375445992359$Боря
$addloginphone$+375220000001$Tайлер
to remove login use $removeloginphone$+375220000001 command
logout from app
that you will see fast login buttons on main page

-----------------------------------------------------------
ENABLE/DISABLE TESTER MODE
-----------------------------------------------------------
login to the app by mobile phone as Moderator
go to Настройки -> Написать разработчикам
type: $settester$+375445462047$true command, where phone number is tester phone number
press Send

-----------------------------------------------------------
IOS DISTRIBUTION
-----------------------------------------------------------
Run command before each IOS build:
yarn createiosbundle 
XCode -> Product -> Build
XCode -> Product -> Archive


